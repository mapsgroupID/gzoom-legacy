
function ajaxUpdate(container, url, parameters, options) {
    parameters = parameters || {};
    parameters.ajax = 'Y';
    options = options || {};
    options.parameters = parameters;
    options.evalScripts = true;
    new Ajax.Updater(container, url, options);
}
