<!-- Campi gestiti nel StandardImportUploadFileListener.js -->
<#assign resultListUploadFile = Static["com.mapsengineering.base.util.CommonUtil"].jsonResponseFromObject(parameters.resultListUploadFile?if_exists?default("")) />
<#assign resultList = Static["com.mapsengineering.base.util.CommonUtil"].jsonResponseFromObject(parameters.resultList?if_exists?default("")) />
<#assign resultETLList = Static["com.mapsengineering.base.util.CommonUtil"].jsonResponseFromObject(parameters.resultETLList?if_exists?default("")) />
<#assign sessionId = parameters.sessionId?if_exists?default("") />

<p id="resultListUploadFile">${resultListUploadFile?if_exists?default("")}</p>
<p id="resultList">${resultList?if_exists}</p>
<p id="resultETLList">${resultETLList?if_exists}</p>
<p id="sessionId">${sessionId?if_exists}</p>
