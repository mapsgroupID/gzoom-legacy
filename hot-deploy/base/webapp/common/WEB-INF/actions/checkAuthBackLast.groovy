// checkAuthBackLast

import org.ofbiz.base.util.*;

context.isAuthBackLastAjax = true;

if ("Y".equals(context._SAVED_VIEW_NAME_)) {
    if ("N".equals(context.ajaxRequest)) {
        context.isAuthBackLastAjax = false;
    }
}
