<body>
  <#assign logoutUrl = Static["org.ofbiz.base.util.UtilProperties"].getPropertyValue("security.properties", "security.saml.logoutUrl")>
   <meta http-equiv="refresh" content="0; url=${logoutUrl}"/>
</body>
