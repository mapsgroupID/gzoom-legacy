<#function fieldTypeName groupName>
	<#return freeMarkerQuery.getFieldTypeName(groupName)>
</#function>
<#assign fieldType = fieldTypeName("")>

SELECT WE.WORK_EFFORT_ID, WS.NEXT_STATUS_ID
FROM <@table "WORK_EFFORT_TYPE"/> EL
INNER JOIN <@table "WORK_EFFORT_TYPE_TYPE"/> WT ON WT.WORK_EFFORT_TYPE_ID_ROOT = EL.WORK_EFFORT_TYPE_ID
INNER JOIN <@table "WORK_EFFORT_TYPE_STATUS"/> WS ON WS.WORK_EFFORT_TYPE_ROOT_ID = WT.WORK_EFFORT_TYPE_ID_FROM
AND WS.NEXT_STATUS_ID IS NOT NULL
INNER JOIN <@table "WORK_EFFORT"/> WE ON WE.WORK_EFFORT_TYPE_ID = WS.WORK_EFFORT_TYPE_ROOT_ID
AND WE.CURRENT_STATUS_ID = WS.CURRENT_STATUS_ID
AND WE.WORK_EFFORT_SNAPSHOT_ID IS NULL
<#if fieldType == "mysql">
  AND CURRENT_DATE >= ADDDATE(WE.LAST_STATUS_UPDATE, INTERVAL WS.NEXT_STATUS_DAYS DAY)
<#elseif fieldType == "mssql">
  AND CAST(GETDATE() AS DATE) >= DATEADD(DAY, WS.NEXT_STATUS_DAYS, WE.LAST_STATUS_UPDATE)
<#elseif fieldType == "postgres">
  AND CURRENT_DATE >= WE.LAST_STATUS_UPDATE + WS.NEXT_STATUS_DAYS * INTERVAL '1 DAY'
<#else>
  AND TO_DATE(CURRENT_DATE) >= WE.LAST_STATUS_UPDATE + WS.NEXT_STATUS_DAYS
</#if>
WHERE EL.WORK_EFFORT_TYPE_ID = <@param workEffortTypeId />
