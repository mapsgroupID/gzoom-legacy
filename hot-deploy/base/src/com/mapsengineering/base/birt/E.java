package com.mapsengineering.base.birt;

/**
 * Constant for standard import
 *
 */
public enum E {	
	reportContentId, outputFormat, contentType, userLogin, locale, localDispatcherName, reportId, createBirtReport, name, contentId, langLocale,
	//
	ContentDataResourceView,
	//
	drObjectInfo, drDataResourceName, nameFile, pathFile
	
}
