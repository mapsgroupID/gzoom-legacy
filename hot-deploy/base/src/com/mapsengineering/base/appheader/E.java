package com.mapsengineering.base.appheader;

/**
 * Enumeration for appheader.*
 *
 */
public enum E {
	PartyNoteView, targetPartyId, noteName, noteInfo,
	//
	PartyContent, partyId, contentId, partyContentTypeId, thruDate,
	//
	DataResource, dataResourceId, objectInfo,
	//
	PartyAndContentDataResource,
	//
	DataResourceContentView,
	//
	DEF_LOGO, DEF_LOGO_LOGIN, DEF_ICON, DEF_ICON_NAV
}
