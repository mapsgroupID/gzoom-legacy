package com.mapsengineering.base.translatorcatalogs.common;

/**
 * Constant for translator catalog
 *
 */
public enum E {
    
    sl, tl, override,
    //
    value, property, key, entityName, fieldName
}
