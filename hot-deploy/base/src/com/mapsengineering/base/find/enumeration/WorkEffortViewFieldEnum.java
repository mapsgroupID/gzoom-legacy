package com.mapsengineering.base.find.enumeration; 

/**
 * Base WorkEffortView EntityName fields
 */
public enum WorkEffortViewFieldEnum {
    parentTypeId, workEffortRevisionId, isTemplate, isRoot, Y, N
}