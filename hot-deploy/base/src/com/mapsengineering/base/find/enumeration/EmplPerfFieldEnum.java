package com.mapsengineering.base.find.enumeration;

public enum EmplPerfFieldEnum {
	orgUnitId, orgUnitRoleTypeId, emplPositionTypeId, templateTypeId, templateId, parentTypeId, organizationId
}
