package com.mapsengineering.base.standardimport.common;

/**
 * WorkEffort interface constant
 *
 */
public interface WeInterfaceConstants {
    public static final String WORK_EFFORT_PREFIX = "E";
    public static final String WORK_EFFORT_MEASURE_PREFIX = "I";
    public static final String WORK_EFFORT_NOTE_PREFIX = "N";

    public static final String OPERATION_TYPE_A = "A";
	public static final String OPERATION_TYPE_U = "U";
	public static final String OPERATION_TYPE_I = "I";
    public static final String CREATE_SNAPSHOT_Y = "Y";
    public static final String CREATE_SNAPSHOT_N = "N";
    public static final String IS_ROOT_Y = "Y";
    public static final String IS_ROOT_N = "N";
    public static final String FROM_CARD_S = "S";
    
    public static final String PURPOSE_TYPE_ENUM_ID_PT_WORKEFFORT = "PT_WORKEFFORT";
    
    public static final String CTX_BS = "CTX_BS";
    public static final String WE_STATUS_PERFORMANC = "WE_STATUS_PERFORMANC";
    public static final String CTX_OR = "CTX_OR";
    public static final String WE_STATUS_ORGANIZAT = "WE_STATUS_ORGANIZAT";
    public static final String CTX_EP = "CTX_EP";
    public static final String WE_STATUS_EVALUATION = "WE_STATUS_EVALUATION";
    public static final String CTX_CO = "CTX_CO";
    public static final String CTX_RE = "CTX_RE";
    public static final String CTX_PR = "CTX_PR";
    public static final String CTX_TR = "CTX_TR";
    public static final String CTX_CG = "CTX_CG";
    
    public static final String RES = "RES";
    public static final String IND = "IND";

    public static final String WEMOMG_NONE = "WEMOMG_NONE";
    public static final String WEMOMG_ORG = "WEMOMG_ORG";
    
}
