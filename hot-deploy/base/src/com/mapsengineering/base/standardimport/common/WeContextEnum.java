package com.mapsengineering.base.standardimport.common;

/**
 * Enum context
 *
 */
public enum WeContextEnum {
	STR, ORG, IND, COR, REND, TRAS, CDG, PROC, GDPR, PART, DIR
}