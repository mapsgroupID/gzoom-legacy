package com.mapsengineering.base.standardimport.common;

/**
 * Constant for standard import
 *
 */
public enum E {
    crudServiceDefaultOrchestration_PartyRelationship, crudServiceDefaultOrchestration_PartyRole, crudServiceDefaultOrchestration_PartyParentRole, crudServiceDefaultOrchestration, crudServiceDefaultOrchestration_WorkEffort,
    //
    crudServiceDefaultOrchestration_WorkEffortRoot, crudServiceDefaultOrchestration_WorkEffortAssoc, crudServiceDefaultOrchestration_WorkEffortPartyAssignment,
    //
    PartyRelationship, PartyParentRole, PartyRole, PartyRelationshipRole, Party, Person, PartyGroup, GlAccountOrganization, WorkEffortPurposeAccount, Enumeration, GlAccountType, WorkEffortPurposeType, GlAccountResource, GlResourceType,
    //
    WorkEffortType, RoleType, StatusItem, WorkEffortWorkEffortAssocAndType, WorkEffortView, StandardImportFieldConfig, 
    //
    GROUP_ROLLUP, ORG_ALLOCATION, ORG_RESPONSIBLE, ORG_EMPLOYMENT, 
    //
    WEM_EVAL_IN_CHARGE, WEM_EVAL_MANAGER, WEM_EVAL_APPROVER, WEF_EVALUATED_BY, WEF_APPROVED_BY, evaluatorFromDate,
    //
    EMPLOYEE, WE_ASSIGNMENT, QUALIF, GOAL05, qualifFromDate, allocationOrgFromDate, allocationOrgThruDate,
    //
    partyId, partyIdFrom, partyIdTo, partyRelationshipTypeId, roleTypeId, roleTypeIdFrom, roleTypeIdTo, relationshipName, relationshipValue, valueUomId, orgCode, personCode, employmentAmount, emplPositionTypeId, PERSON_INTERFACE,
    //
    firstName, lastName, groupName, groupNameLang, longDescription, longDescriptionLang, refDate, personRoleTypeId, parentRoleCode, parentRoleTypeId, responsibleCode, parentCode,
    //
    statusId, fromDate, thruDate, endDate, comments, description, descriptionLang, userLogin, userLoginId, contactMail, qualifCode, note,
    //
    enabled, groupId, allocationOrgCode, contactMechId, contactMobile, employmentOrgCode, evaluatorCode, parentOrgCode, approverCode, allocationFromDate,
    //
    roleTypeValidFrom, roleTypeValidTo, UserLogin, PARTY_ENABLED, PARTY_DISABLED, ORGANIZATION_UNIT, ORGANIZATION_ROOT, PARENT_ROOT,
    //
    updateUserLoginSecurity, addUserLoginToSecurityGroup, updateUserLoginToSecurityGroup, setPartyRelationshipTo,
    //
    fromDateCompetence, toDateCompetence, uorgCode, glAccountCode, glAccountId, partyCode, dataSource,
    //
    accountCode, GlAccount, glFiscalTypeId, glFiscalTypeIdOutput, workEffortId, customTimePeriodId, weTransAccountId, acctgTransId, amount, accountName, accountNameLang, accountTypeId, accountReferenceId, debitCreditDefault, GlAccountInterface,
    //
    employmentRoleTypeId, allocationRoleTypeId, orgRoleTypeId, organizationPartyId,
    //
    accountTypeEnumId, enumId, enumTypeId, glAccountTypeId, glResourceTypeId, workEffortPurposeTypeId, purposeTypeEnumId, source, sourceLang, dataSourceId, parentTypeId,
    //
    WorkEffort, sourceReferenceId, workEffortSnapshotId, entryWorkEffortSnapshotId, operationType, workEffortTypeId, sourceReferenceRootId, weContext, createSnapshot, snapshotDescription, workEffortName, workEffortNameLang, orgDesc, sourceId, typeId,
    //
    statusItemDesc, UserLoginValidPartyRole, organizationId, isRoot, isTemplate, workEffortParentId, currentStatusId, statusDesc, fromCard, orgUnitId, orgUnitRoleTypeId, etch, workEffortCode, defaultStatusPrefix,
    //
    estimatedStartDate, estimatedCompletionDate, actualStartDate, actualCompletionDate, scheduledStartDate, scheduledCompletionDate,
    //
    revisionNumber, weightKpi, weightAssocWorkEffort, weightSons, totalEnumIdSons, totalEnumIdKpi, totalEnumIdAssoc, workEffortTypeHierarchyId, wePurposeTypeIdWe, workEffortAssocTypeId, statusTypeId,
    //
    WorkEffortTypeStatus, workEffortTypeRootId, orgTypeCode, roleTypeDesc, orgTypeDesc, workEffortPurposeTypeDesc, elabResult, workEffortTypeIdFrom, workEffortTypeIdTo,
    //
    sourceReferenceIdFrom, workEffortNameFrom, WorkEffortAssocType, workEffortAssocTypeCode, workEffortIdTo, workEffortIdFrom, WorkEffortAssoc, hasTable, wefromWetoEnumId, workEffortTypeIdRef,
    //
    sequenceNum, assocWeight, WorkEffortTypeAssoc, workEffortNameTo, sourceReferenceIdTo, workEffortIdRoot, weHierarchyTypeId, workEffortRootId, exceptionMessage, 
    //
    workEffortMeasureCode, WorkEffortMeasure, workEffortMeasureId, wePurposeTypeIdInd, wePurposeTypeIdRes,
    //
    weScoreRangeEnumId, weScoreConvEnumId, weAlertRuleEnumId, periodTypeId, weWithoutPerf, defaultUomId, weMeasureTypeDesc, crudServiceDefaultOrchestration_WorkEffortMeasure, uomDescr, uomDescrLang, weMeasureTypeEnumId,
    //
    kpiScoreWeight, sequenceId, weOtherGoalEnumId, workEffortTypeIdOut, isEvalManager, WorkEffortTypeRole, partyName, WorkEffortPartyAssignment, roleTypeIdOut, roleTypeWeight, sourceReferenceRootIdTo, sourceReferenceRootIdFrom,
    //
    inputEnumId, productId, productMainCode, voucherRef, Uom, uomId, uomTypeId, uomRatingValue, amountCode, referencedAccountId,
    //
    enumCode, WorkEffortAssocAndParentView, wrToParentId, wrFromParentId, WorkEffortTypeStatusView, templateId, returnMessages, emplPositionTypeDate,
    //
    PeriodType, periodTypeDesc, defaultUomCode, abbreviation, uomRangeId, WorkEffortTypeContent, contentId, specialTerms, INTERNAL_ORGANIZATIO, Company, entryGlFiscalTypeId, weTransTypeValueId, EmplPositionType, templateTypeId, 
    //
    workEffortRevisionId, N, Y, _NA_, AcctgTrans, deletePrevious, entityListToImport, filterConditions, filterMapList, detectOrgUnitIdFlag, uorgRoleTypeId, CustomTimePeriod, PartyRoleView, GOALDIV01, ORGROOT001, 
    //
    WorkEffortNote, WorkEffortNoteAndData, noteName, noteDateTime, noteInfo, noteInfoLang, attrName, WorkEffortTypeAttr, crudServiceDefaultOrchestration_WorkEffortNoteAndData, internalNote, isMain, isHtml, noteId, NoteData, 
    //
    entryPartyId, entryGlAccountId, entryGlAccountFinId, transactionDate, respCenterId, entryVoucherRef, entryProductId, uomRangeDesc, UomRange, skipStore, 
    //
    contactMechPurposeTypeId, PRIMARY_EMAIL, crudServiceDefaultOrchestration_PartyContactMechPurpose, PartyContactWithPurpose, purposeFromDate, updatePartyContactMechExt, contactMechTypeId, emailAddress, EMAIL_ADDRESS, 
    //
    TELECOM_NUMBER, PRIMARY_PHONE, contactNumber, PartyNameContactMechView, updatePartyTelecomNumber, updatePartyEmailAddressExt,
    //
    executeToClone, relationTitle, to, workEffortAssignmentCode, workEffortDate, employmentOrgComments, allocationOrgComments, employmentOrgDescription, allocationOrgDescription, WorkEffortPartyAssignSnapshotView,
    //
    GLACCINPUT, CustomMethod, customMethodId, calcCustomMethodId, prioCalc, infoString, isDefault, isNote, partyRoleCode, 
    //
    ouGroupName, ouExternalId, externalId, PartyRelationshipOrganizations, checkFromETL, checkEndYearElab, endYearElab, checkOnlyUpload, checkOnlyMoveSmbFile, onlyUpload, onlyMoveSmbFile, responseMessage, PersonInterface,
    //
    standardImport, workEffortPeriodId, workEffortTypeIdRoot, WEPE_CAL_YEAR, IMP_EXCEL, ORGUNIT001, oldEstimatedStartDate, oldEstimatedCompletionDate, GlAccountAndRoleView, grFromDate, grThruDate, INDICATOR, WETATO, fiscalCode, showCode,
    //
    directoryPathInXls, directoryPathOutXls, entityName, filename, filePath, entityCode, resultListUploadFile, COM_IN_PROGRESS, AUTO_EMAIL_COMM, communicationEventTypeId, contentMimeTypeId, subject, content, toString, PartyContactMechDetail, contactMechIdFrom,
    //
    PartyNoteView, targetPartyId, userPrefTypeId, UserPreference, AUTOMATIC_LOGIN_PASSWORD, defaultOrganizationPartyId, PartyAcctgPreferenceView, GlAccountClass, glAccountClassId, accountClassCode, glAccountClassCode, referencedAccountCode, 
    //
    kpiOtherWeight, detailEnumId, isPosted,
    //
    defaultValue, standardInterface, internalFieldName, WEPE_START_YEAR, WorkEffortAndTypeView, hasPersonFilter, interfaceSeq, WeInterface, WeAssocInterface, WeMeasureInterface, WeNoteInterface, WePartyInterface, OrganizationInterface, AcctgTransInterface, AllocationInterface, 
    //
    ORGANIZATION_INTERFACE, GL_ACCOUNT_INTERFACE, ACCTG_TRANS_INTERFACE, WE_ROOT_INTERFACE, WE_INTERFACE, WE_NOTE_INTERFACE, WE_MEASURE_INTERFACE, WE_ASSOC_INTERFACE, WE_PARTY_INTERFACE, ALLOCATION_INTERFACE, ALLOCATION_INTERFACE_EXT, 
    //
    WeRootInterface, externalFieldName, fileExtension, rowNum, count, record, errorMsg, ORG_RESP_INTERFACE, PERS_RESP_INTERFACE, OrgRespInterface, PersRespInterface, DataSource, id, productCode, parentFromDate,
    //
    employmentOrgFromDate, employmentOrgThruDate, changeNameDate, partyNameLang, WorkEffortTypeAttrAndNoteData, noteNameLang, attrNameLang,
    //
    errorCode, errorMessage, QueryConfig, queryType, queryActive, A, queryCtx, queryCode, CTX_PY, CTX_AC, queryExecutorService, queryId, cond0Info, histJobLogId,
    //
    RoleTypeValidFromOrgAllocationView, maxRoleTypeValidFrom, seq, throwError, syncMode, columnName
}
