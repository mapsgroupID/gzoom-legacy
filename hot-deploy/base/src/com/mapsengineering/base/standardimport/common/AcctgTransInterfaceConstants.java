package com.mapsengineering.base.standardimport.common;

/**
 * AcctgTransInterface Constants
 *
 */
public interface AcctgTransInterfaceConstants {

    public static final String ACCTG_TRANS_PREFIX = "T";
    public static final String PARENT_ROLE_TYPE_ID_DEFAULT = "ORGANIZATION_UNIT";
    public static final String INPUT_ENUM_ID_OBIETTIVO = "ACCINP_OBJ";
    public static final String INPUT_ENUM_ID_PRODOTTO = "ACCINP_PRD";
    public static final String ACCTG_TRANS_TYPE_ID_GLA_PRD = "GLA_PRD";
    public static final String ACCTG_TRANS_TYPE_ID_GLA_ORU = "GLA_ORU";
    public static final String DETAIL_ENUM_ID_SUM = "ACCDET_SUM";
    public static final String DETAIL_ENUM_ID_NOSUM = "ACCDET_NOSUM";
    public static final String GL_FISCAL_TYPE_ID_ACTUAL = "ACTUAL";
    public static final String GL_ACCOUNT_TYPE_ENUM_ID_FIN = "FINANCIAL";

    public static final String DATA_SOURCE_RAW = "RAW";
    public static final String DATA_SOURCE_ABB = "ABB";

    public static final String STR_IS_NOT_VALID = " is not valid";
    public static final String PERIOD_NOT_FOUND_FOR = "Period not found for ";

    public static final String UOM_TYPE_DATE_MEASURE = "DATE_MEASURE";
    public static final String UOM_TYPE_RATING_SCALE = "RATING_SCALE";
    public static final String CDC_SPESA = "CDC_SPESA";
    public static final String CDC_ENTRATA = "CDC_ENTRATA";
}
