package com.mapsengineering.base.standardimport.helper;

/**
 * Constant
 *
 */
public enum TemplateEnum {
    PartyHistoryView, partyId, fromDate, thruDate, templateId

}