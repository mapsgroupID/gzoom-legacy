package com.mapsengineering.base.standardimport.enumeration;

/**
 * Enumeration with OrganizationInterface fields
 *
 */
public enum OrganizationInterfaceFieldEnum {
	refDate, orgCode, description, longDescription, parentOrgCode, responsibleCode, thruDate, 
	orgRoleTypeId, parentRoleTypeId, id, dataSource, responsibleFromDate, responsibleThruDate,
	responsibleComments, responsibleRoleTypeId
}
