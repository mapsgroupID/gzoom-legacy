package com.mapsengineering.base.standardimport.enumeration;

/**
 * Enumeration with OrgRespInterface fields
 *
 */
public enum OrgRespInterfaceFieldEnum {
	refDate, orgCode, description, longDescription, parentOrgCode, responsibleCode, thruDate, 
	orgRoleTypeId, parentRoleTypeId, id, dataSource, comments, roleTypeId
}
