package com.mapsengineering.base.standardimport.enumeration;

/**
 * Enumeration with AllocationInterface fields
 *
 */
public enum AllocationInterfaceFieldEnum {
	refDate, personCode, allocationOrgCode, allocationFromDate, personRoleTypeId, allocationThruDate, allocationValue, dataSource, allocationRoleTypeId, allocationOrgComments, allocationOrgDescription, employmentAmount 
}
