package com.mapsengineering.base.standardimport.enumeration;

/**
 * Enumeration with PersRespInterface fields
 *
 */
public enum PersRespInterfaceFieldEnum {
    dataSource, refDate, personCode, evaluatorFromDate, fromDate, thruDate,  
	evaluatorCode, approverCode
}
