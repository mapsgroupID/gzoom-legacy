package com.mapsengineering.base.batchstatus;

/**
 * Enumeration for query
 */
public enum E {
    WorkEffortType, Y, batchStatusActive, workEffortTypeId, workEffortId, statusId, WORK_EFFORT_ID, NEXT_STATUS_ID, BaseUiLabels, 
    crudServiceDefaultOrchestration_WorkEffortRootStatus, WorkEffortStatus, statusDatetime
}
