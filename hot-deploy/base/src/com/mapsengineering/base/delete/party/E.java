package com.mapsengineering.base.delete.party;

/**
 * Constant for standard import
 *
 */
public enum E {	

    userLoginId, newUserLoginId, createdByUserLogin, lastModifiedByUserLogin, locale, jobLogger,
	//
	Party, PartyType, Person, PartyGroup, PartyRelationship, PartyParentRole, PartyRole, PartyStatus, PartyHistory, PartyNameHistory, PartyNote, NoteData, PartyContent, Content, 
	PartyAttribute, TelecomNumber, ContactMechAttribute, PostalAddress,
	//
	PartyContactMech, PartyContactMechPurpose, ContactMech, WorkEffortPartyAssignment,
	//
	groupId, workEffortTypeId, currentStatusId, partyId, partyIdTo, partyIdFrom, partyTypeId, contactMechId, noteId, contentId, workEffortId,
	//
	partyPhysicalDelete, responseMessage, userLoginPhysicalDelete, workEffortsRootPhysicalDelete
	
}
