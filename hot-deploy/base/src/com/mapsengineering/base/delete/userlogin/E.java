package com.mapsengineering.base.delete.userlogin;

/**
 * Constant for standard import
 *
 */
public enum E {	

    userLoginId, newUserLoginId, createdByUserLogin, lastModifiedByUserLogin, locale, jobLogger,
	//
    UserLoginValidPartyRole, UserLoginSecurityGroup, UserLoginHistory, UserLogin, WorkEffort, WorkEffortMeasure, WorkEffortAssoc, WorkEffortPartyAssignment, AcctgTrans, 
    AcctgTransEntry, Party, PartyParentRole, PartyRole, PartyRelationship, PartyStatus, Person, PartyGroup, PartyNameHistory, NoteData, WorkEffortStatus,
	//
	groupId, workEffortTypeId, currentStatusId, partyId, partyIdTo, partyIdFrom, roleTypeId, noteParty, setByUserLogin
	
}
