package com.mapsengineering.base.jdbc;

public class JdbcQueryResult extends JdbcQueryGeneric {

    private static final long serialVersionUID = 1L;

    protected JdbcQueryResult(JdbcQuery query) {
        super(query);
    }
}
