package com.mapsengineering.base.services.async;

public interface AsyncJobCallback {

    public void onAsyncJobProgress();
}
