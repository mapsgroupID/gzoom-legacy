package com.mapsengineering.base.services.async;

import java.util.Comparator;

public class AsyncJobListViewComparator implements Comparator<AsyncJob> {

    @Override
    public int compare(AsyncJob o1, AsyncJob o2) {
        return -o1.compareTo(o2);
    }
}
