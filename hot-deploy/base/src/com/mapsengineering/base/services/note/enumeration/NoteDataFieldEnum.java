package com.mapsengineering.base.services.note.enumeration;

/**
 * entityName and entityName fields
 */
public enum NoteDataFieldEnum {
    NoteData,
    //
    noteId, noteName, noteDateTime, noteInfo, noteParty, partyId, isPublic, N, SYSTEMNOTE
}