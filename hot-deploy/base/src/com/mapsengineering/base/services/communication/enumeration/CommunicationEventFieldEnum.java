package com.mapsengineering.base.services.communication.enumeration;

/**
 * entityName and entityName fields
 */
public enum CommunicationEventFieldEnum {
    CommunicationEvent,
    //
    createCommunicationEvent, partyIdEmailAddress, resourceLabel,
    //
    subject, content, toString, contactMechIdFrom, contactMechIdTo, partyIdFrom, statusId, COM_IN_PROGRESS, AUTO_EMAIL_COMM, communicationEventTypeId, contentMimeTypeId
}