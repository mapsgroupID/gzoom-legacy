package com.mapsengineering.base.util.importexport;


public enum E {
    idMisura, descrizioneMisura, codiceIndicatore, titoloIndicatore, codiceObiettivo, descrizioneObiettivo, dataRiferimento, valoreBudget,
    valoreConsuntivo, valoreConsuntivoAp, valoreRisultato, commento, codiceUnitaOrg, descrizioneUnitaOrg,
    finalita, descrizioneFinalita, codiceFonte, descrizioneFonte, risultatoImportazione,
    workbook, locale
}
