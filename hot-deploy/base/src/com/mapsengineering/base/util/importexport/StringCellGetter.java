package com.mapsengineering.base.util.importexport;

import org.apache.poi.hssf.usermodel.HSSFCell;

public class StringCellGetter implements CellValueGetter {

    @Override
    public Object getCellValue(HSSFCell cell) {
        return cell.getRichStringCellValue().toString();
    }

}
