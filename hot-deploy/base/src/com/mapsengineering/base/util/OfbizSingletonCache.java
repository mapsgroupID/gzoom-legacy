package com.mapsengineering.base.util;

import org.ofbiz.base.util.cache.UtilCache;

public class OfbizSingletonCache {

    public static final UtilCache<String, Object> INSTANCE = UtilCache.createUtilCache("object.singleton");
}
