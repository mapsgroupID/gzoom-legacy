package com.mapsengineering.base.util;

/**
 * Task to run in a transaction.
 * @author sivi
 *
 */
public interface TransactionItem {

    /**
     * Method called by a TransactionRunner
     * @throws Exception
     */
    public void run() throws Exception;
}
