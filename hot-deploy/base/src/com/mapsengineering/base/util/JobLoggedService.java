package com.mapsengineering.base.util;

/**
 * Return JobLogger 
 *
 */
public interface JobLoggedService {

    /**
     * Return JobLogger
     * @return
     */
    public JobLogger getJobLogger();
}
