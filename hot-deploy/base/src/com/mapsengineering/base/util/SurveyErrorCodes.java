package com.mapsengineering.base.util;

/**
 * Enum utilizzata per gestire i messaggi d'errore nei questionari. E' attivata dalla properties survey.errorCodes
 * 
 * @author mirlap
 *
 */

public enum SurveyErrorCodes {
	USER_NOT_FOUND, SURVEY_CLOSED, ADDRESS_NOT_RELIABLE
}
