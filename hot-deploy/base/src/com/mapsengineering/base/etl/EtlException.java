package com.mapsengineering.base.etl;

/**
 * @author rime
 *
 */
public class EtlException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1261295143460077777L;

	/**
	 * 
	 */
	public EtlException() { super(); }
	
	/**
	 * @param message
	 */
	public EtlException(String message) { super(message); }
	
	/**
	 * @param message
	 * @param cause
	 */
	public EtlException(String message, Throwable cause) { super(message, cause); }
	
	/**
	 * @param cause
	 */
	public EtlException(Throwable cause) { super(cause); }

}
