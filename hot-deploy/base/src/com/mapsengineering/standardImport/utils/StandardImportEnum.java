package com.mapsengineering.standardImport.utils;

public interface StandardImportEnum {
	boolean isMandatory();
	String getName();
}
