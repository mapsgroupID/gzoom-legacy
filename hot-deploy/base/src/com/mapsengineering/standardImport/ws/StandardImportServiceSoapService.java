/**
 * StandardImportServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mapsengineering.standardImport.ws;

public interface StandardImportServiceSoapService extends javax.xml.rpc.Service {
    public java.lang.String getStandardImportServiceAddress();

    public com.mapsengineering.standardImport.ws.StandardImportServiceSoap getStandardImportService() throws javax.xml.rpc.ServiceException;

    public com.mapsengineering.standardImport.ws.StandardImportServiceSoap getStandardImportService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
