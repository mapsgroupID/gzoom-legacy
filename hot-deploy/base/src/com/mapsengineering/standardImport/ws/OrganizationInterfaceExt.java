/**
 * OrganizationInterfaceExt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mapsengineering.standardImport.ws;

public class OrganizationInterfaceExt  implements java.io.Serializable {
    private java.util.Date refDate;

    private java.lang.String dataSource;

    private java.lang.String orgCode;

    private java.lang.String description;

    private java.lang.String parentOrgCode;

    private java.lang.String responsibleCode;

    private java.util.Date thruDate;

    private java.lang.String orgRoleTypeId;

    private java.lang.String parentRoleTypeId;

    private java.lang.String descriptionLang;

    private java.lang.String longDescription;

    private java.lang.String longDescriptionLang;

    private java.lang.String vatCode;

    private java.util.Date parentFromDate;

    private java.util.Date responsibleFromDate;

    private java.util.Date responsibleThruDate;

    private java.lang.String responsibleComments;

    private java.lang.String responsibleRoleTypeId;

    public OrganizationInterfaceExt() {
    }

    public OrganizationInterfaceExt(
           java.util.Date refDate,
           java.lang.String dataSource,
           java.lang.String orgCode,
           java.lang.String description,
           java.lang.String parentOrgCode,
           java.lang.String responsibleCode,
           java.util.Date thruDate,
           java.lang.String orgRoleTypeId,
           java.lang.String parentRoleTypeId,
           java.lang.String descriptionLang,
           java.lang.String longDescription,
           java.lang.String longDescriptionLang,
           java.lang.String vatCode,
           java.util.Date parentFromDate,
           java.util.Date responsibleFromDate,
           java.util.Date responsibleThruDate,
           java.lang.String responsibleComments,
           java.lang.String responsibleRoleTypeId) {
           this.refDate = refDate;
           this.dataSource = dataSource;
           this.orgCode = orgCode;
           this.description = description;
           this.parentOrgCode = parentOrgCode;
           this.responsibleCode = responsibleCode;
           this.thruDate = thruDate;
           this.orgRoleTypeId = orgRoleTypeId;
           this.parentRoleTypeId = parentRoleTypeId;
           this.descriptionLang = descriptionLang;
           this.longDescription = longDescription;
           this.longDescriptionLang = longDescriptionLang;
           this.vatCode = vatCode;
           this.parentFromDate = parentFromDate;
           this.responsibleFromDate = responsibleFromDate;
           this.responsibleThruDate = responsibleThruDate;
           this.responsibleComments = responsibleComments;
           this.responsibleRoleTypeId = responsibleRoleTypeId;
    }


    /**
     * Gets the refDate value for this OrganizationInterfaceExt.
     * 
     * @return refDate
     */
    public java.util.Date getRefDate() {
        return refDate;
    }


    /**
     * Sets the refDate value for this OrganizationInterfaceExt.
     * 
     * @param refDate
     */
    public void setRefDate(java.util.Date refDate) {
        this.refDate = refDate;
    }


    /**
     * Gets the dataSource value for this OrganizationInterfaceExt.
     * 
     * @return dataSource
     */
    public java.lang.String getDataSource() {
        return dataSource;
    }


    /**
     * Sets the dataSource value for this OrganizationInterfaceExt.
     * 
     * @param dataSource
     */
    public void setDataSource(java.lang.String dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * Gets the orgCode value for this OrganizationInterfaceExt.
     * 
     * @return orgCode
     */
    public java.lang.String getOrgCode() {
        return orgCode;
    }


    /**
     * Sets the orgCode value for this OrganizationInterfaceExt.
     * 
     * @param orgCode
     */
    public void setOrgCode(java.lang.String orgCode) {
        this.orgCode = orgCode;
    }


    /**
     * Gets the description value for this OrganizationInterfaceExt.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this OrganizationInterfaceExt.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the parentOrgCode value for this OrganizationInterfaceExt.
     * 
     * @return parentOrgCode
     */
    public java.lang.String getParentOrgCode() {
        return parentOrgCode;
    }


    /**
     * Sets the parentOrgCode value for this OrganizationInterfaceExt.
     * 
     * @param parentOrgCode
     */
    public void setParentOrgCode(java.lang.String parentOrgCode) {
        this.parentOrgCode = parentOrgCode;
    }


    /**
     * Gets the responsibleCode value for this OrganizationInterfaceExt.
     * 
     * @return responsibleCode
     */
    public java.lang.String getResponsibleCode() {
        return responsibleCode;
    }


    /**
     * Sets the responsibleCode value for this OrganizationInterfaceExt.
     * 
     * @param responsibleCode
     */
    public void setResponsibleCode(java.lang.String responsibleCode) {
        this.responsibleCode = responsibleCode;
    }


    /**
     * Gets the thruDate value for this OrganizationInterfaceExt.
     * 
     * @return thruDate
     */
    public java.util.Date getThruDate() {
        return thruDate;
    }


    /**
     * Sets the thruDate value for this OrganizationInterfaceExt.
     * 
     * @param thruDate
     */
    public void setThruDate(java.util.Date thruDate) {
        this.thruDate = thruDate;
    }


    /**
     * Gets the orgRoleTypeId value for this OrganizationInterfaceExt.
     * 
     * @return orgRoleTypeId
     */
    public java.lang.String getOrgRoleTypeId() {
        return orgRoleTypeId;
    }


    /**
     * Sets the orgRoleTypeId value for this OrganizationInterfaceExt.
     * 
     * @param orgRoleTypeId
     */
    public void setOrgRoleTypeId(java.lang.String orgRoleTypeId) {
        this.orgRoleTypeId = orgRoleTypeId;
    }


    /**
     * Gets the parentRoleTypeId value for this OrganizationInterfaceExt.
     * 
     * @return parentRoleTypeId
     */
    public java.lang.String getParentRoleTypeId() {
        return parentRoleTypeId;
    }


    /**
     * Sets the parentRoleTypeId value for this OrganizationInterfaceExt.
     * 
     * @param parentRoleTypeId
     */
    public void setParentRoleTypeId(java.lang.String parentRoleTypeId) {
        this.parentRoleTypeId = parentRoleTypeId;
    }


    /**
     * Gets the descriptionLang value for this OrganizationInterfaceExt.
     * 
     * @return descriptionLang
     */
    public java.lang.String getDescriptionLang() {
        return descriptionLang;
    }


    /**
     * Sets the descriptionLang value for this OrganizationInterfaceExt.
     * 
     * @param descriptionLang
     */
    public void setDescriptionLang(java.lang.String descriptionLang) {
        this.descriptionLang = descriptionLang;
    }


    /**
     * Gets the longDescription value for this OrganizationInterfaceExt.
     * 
     * @return longDescription
     */
    public java.lang.String getLongDescription() {
        return longDescription;
    }


    /**
     * Sets the longDescription value for this OrganizationInterfaceExt.
     * 
     * @param longDescription
     */
    public void setLongDescription(java.lang.String longDescription) {
        this.longDescription = longDescription;
    }


    /**
     * Gets the longDescriptionLang value for this OrganizationInterfaceExt.
     * 
     * @return longDescriptionLang
     */
    public java.lang.String getLongDescriptionLang() {
        return longDescriptionLang;
    }


    /**
     * Sets the longDescriptionLang value for this OrganizationInterfaceExt.
     * 
     * @param longDescriptionLang
     */
    public void setLongDescriptionLang(java.lang.String longDescriptionLang) {
        this.longDescriptionLang = longDescriptionLang;
    }


    /**
     * Gets the vatCode value for this OrganizationInterfaceExt.
     * 
     * @return vatCode
     */
    public java.lang.String getVatCode() {
        return vatCode;
    }


    /**
     * Sets the vatCode value for this OrganizationInterfaceExt.
     * 
     * @param vatCode
     */
    public void setVatCode(java.lang.String vatCode) {
        this.vatCode = vatCode;
    }


    /**
     * Gets the parentFromDate value for this OrganizationInterfaceExt.
     * 
     * @return parentFromDate
     */
    public java.util.Date getParentFromDate() {
        return parentFromDate;
    }


    /**
     * Sets the parentFromDate value for this OrganizationInterfaceExt.
     * 
     * @param parentFromDate
     */
    public void setParentFromDate(java.util.Date parentFromDate) {
        this.parentFromDate = parentFromDate;
    }


    /**
     * Gets the responsibleFromDate value for this OrganizationInterfaceExt.
     * 
     * @return responsibleFromDate
     */
    public java.util.Date getResponsibleFromDate() {
        return responsibleFromDate;
    }


    /**
     * Sets the responsibleFromDate value for this OrganizationInterfaceExt.
     * 
     * @param responsibleFromDate
     */
    public void setResponsibleFromDate(java.util.Date responsibleFromDate) {
        this.responsibleFromDate = responsibleFromDate;
    }


    /**
     * Gets the responsibleThruDate value for this OrganizationInterfaceExt.
     * 
     * @return responsibleThruDate
     */
    public java.util.Date getResponsibleThruDate() {
        return responsibleThruDate;
    }


    /**
     * Sets the responsibleThruDate value for this OrganizationInterfaceExt.
     * 
     * @param responsibleThruDate
     */
    public void setResponsibleThruDate(java.util.Date responsibleThruDate) {
        this.responsibleThruDate = responsibleThruDate;
    }


    /**
     * Gets the responsibleComments value for this OrganizationInterfaceExt.
     * 
     * @return responsibleComments
     */
    public java.lang.String getResponsibleComments() {
        return responsibleComments;
    }


    /**
     * Sets the responsibleComments value for this OrganizationInterfaceExt.
     * 
     * @param responsibleComments
     */
    public void setResponsibleComments(java.lang.String responsibleComments) {
        this.responsibleComments = responsibleComments;
    }


    /**
     * Gets the responsibleRoleTypeId value for this OrganizationInterfaceExt.
     * 
     * @return responsibleRoleTypeId
     */
    public java.lang.String getResponsibleRoleTypeId() {
        return responsibleRoleTypeId;
    }


    /**
     * Sets the responsibleRoleTypeId value for this OrganizationInterfaceExt.
     * 
     * @param responsibleRoleTypeId
     */
    public void setResponsibleRoleTypeId(java.lang.String responsibleRoleTypeId) {
        this.responsibleRoleTypeId = responsibleRoleTypeId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrganizationInterfaceExt)) return false;
        OrganizationInterfaceExt other = (OrganizationInterfaceExt) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.refDate==null && other.getRefDate()==null) || 
             (this.refDate!=null &&
              this.refDate.equals(other.getRefDate()))) &&
            ((this.dataSource==null && other.getDataSource()==null) || 
             (this.dataSource!=null &&
              this.dataSource.equals(other.getDataSource()))) &&
            ((this.orgCode==null && other.getOrgCode()==null) || 
             (this.orgCode!=null &&
              this.orgCode.equals(other.getOrgCode()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.parentOrgCode==null && other.getParentOrgCode()==null) || 
             (this.parentOrgCode!=null &&
              this.parentOrgCode.equals(other.getParentOrgCode()))) &&
            ((this.responsibleCode==null && other.getResponsibleCode()==null) || 
             (this.responsibleCode!=null &&
              this.responsibleCode.equals(other.getResponsibleCode()))) &&
            ((this.thruDate==null && other.getThruDate()==null) || 
             (this.thruDate!=null &&
              this.thruDate.equals(other.getThruDate()))) &&
            ((this.orgRoleTypeId==null && other.getOrgRoleTypeId()==null) || 
             (this.orgRoleTypeId!=null &&
              this.orgRoleTypeId.equals(other.getOrgRoleTypeId()))) &&
            ((this.parentRoleTypeId==null && other.getParentRoleTypeId()==null) || 
             (this.parentRoleTypeId!=null &&
              this.parentRoleTypeId.equals(other.getParentRoleTypeId()))) &&
            ((this.descriptionLang==null && other.getDescriptionLang()==null) || 
             (this.descriptionLang!=null &&
              this.descriptionLang.equals(other.getDescriptionLang()))) &&
            ((this.longDescription==null && other.getLongDescription()==null) || 
             (this.longDescription!=null &&
              this.longDescription.equals(other.getLongDescription()))) &&
            ((this.longDescriptionLang==null && other.getLongDescriptionLang()==null) || 
             (this.longDescriptionLang!=null &&
              this.longDescriptionLang.equals(other.getLongDescriptionLang()))) &&
            ((this.vatCode==null && other.getVatCode()==null) || 
             (this.vatCode!=null &&
              this.vatCode.equals(other.getVatCode()))) &&
            ((this.parentFromDate==null && other.getParentFromDate()==null) || 
             (this.parentFromDate!=null &&
              this.parentFromDate.equals(other.getParentFromDate()))) &&
            ((this.responsibleFromDate==null && other.getResponsibleFromDate()==null) || 
             (this.responsibleFromDate!=null &&
              this.responsibleFromDate.equals(other.getResponsibleFromDate()))) &&
            ((this.responsibleThruDate==null && other.getResponsibleThruDate()==null) || 
             (this.responsibleThruDate!=null &&
              this.responsibleThruDate.equals(other.getResponsibleThruDate()))) &&
            ((this.responsibleComments==null && other.getResponsibleComments()==null) || 
             (this.responsibleComments!=null &&
              this.responsibleComments.equals(other.getResponsibleComments()))) &&
            ((this.responsibleRoleTypeId==null && other.getResponsibleRoleTypeId()==null) || 
             (this.responsibleRoleTypeId!=null &&
              this.responsibleRoleTypeId.equals(other.getResponsibleRoleTypeId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRefDate() != null) {
            _hashCode += getRefDate().hashCode();
        }
        if (getDataSource() != null) {
            _hashCode += getDataSource().hashCode();
        }
        if (getOrgCode() != null) {
            _hashCode += getOrgCode().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getParentOrgCode() != null) {
            _hashCode += getParentOrgCode().hashCode();
        }
        if (getResponsibleCode() != null) {
            _hashCode += getResponsibleCode().hashCode();
        }
        if (getThruDate() != null) {
            _hashCode += getThruDate().hashCode();
        }
        if (getOrgRoleTypeId() != null) {
            _hashCode += getOrgRoleTypeId().hashCode();
        }
        if (getParentRoleTypeId() != null) {
            _hashCode += getParentRoleTypeId().hashCode();
        }
        if (getDescriptionLang() != null) {
            _hashCode += getDescriptionLang().hashCode();
        }
        if (getLongDescription() != null) {
            _hashCode += getLongDescription().hashCode();
        }
        if (getLongDescriptionLang() != null) {
            _hashCode += getLongDescriptionLang().hashCode();
        }
        if (getVatCode() != null) {
            _hashCode += getVatCode().hashCode();
        }
        if (getParentFromDate() != null) {
            _hashCode += getParentFromDate().hashCode();
        }
        if (getResponsibleFromDate() != null) {
            _hashCode += getResponsibleFromDate().hashCode();
        }
        if (getResponsibleThruDate() != null) {
            _hashCode += getResponsibleThruDate().hashCode();
        }
        if (getResponsibleComments() != null) {
            _hashCode += getResponsibleComments().hashCode();
        }
        if (getResponsibleRoleTypeId() != null) {
            _hashCode += getResponsibleRoleTypeId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrganizationInterfaceExt.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.standardImport.mapsengineering.com/", "organizationInterfaceExt"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataSource");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orgCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentOrgCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parentOrgCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsibleCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "responsibleCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("thruDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "thruDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgRoleTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orgRoleTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentRoleTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parentRoleTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descriptionLang");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descriptionLang"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "longDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longDescriptionLang");
        elemField.setXmlName(new javax.xml.namespace.QName("", "longDescriptionLang"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vatCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vatCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentFromDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parentFromDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsibleFromDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "responsibleFromDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsibleThruDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "responsibleThruDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsibleComments");
        elemField.setXmlName(new javax.xml.namespace.QName("", "responsibleComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsibleRoleTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "responsibleRoleTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
