/**
 * StandardImportServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mapsengineering.standardImport.ws;

public interface StandardImportServiceSoap extends java.rmi.Remote {
    public com.mapsengineering.standardImport.ws.StandardImportWSResponse setOrganizationInterfaceExt(com.mapsengineering.standardImport.ws.OrganizationInterfaceExt[] parameters) throws java.rmi.RemoteException;
    public com.mapsengineering.standardImport.ws.StandardImportWSResponse setPersonInterfaceExt(com.mapsengineering.standardImport.ws.PersonInterfaceExt[] parameters) throws java.rmi.RemoteException;
    public com.mapsengineering.standardImport.ws.StandardImportWSResponse setPersonInterfaceExt_2(com.mapsengineering.standardImport.ws.PersonInterfaceExt_2[] parameters) throws java.rmi.RemoteException;
    public com.mapsengineering.standardImport.ws.StandardImportWSResponse setAllocationInterfaceExt(com.mapsengineering.standardImport.ws.AllocationInterfaceExt[] parameters) throws java.rmi.RemoteException;
}
