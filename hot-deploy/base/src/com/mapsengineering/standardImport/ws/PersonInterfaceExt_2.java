/**
 * PersonInterfaceExt_2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mapsengineering.standardImport.ws;

public class PersonInterfaceExt_2  implements java.io.Serializable {
    private java.lang.String dataSource;

    private java.util.Date refDate;

    private java.lang.String personCode;

    private java.lang.String firstName;

    private java.lang.String lastName;

    private java.lang.Integer employmentAmount;

    private java.lang.String emplPositionTypeId;

    private java.lang.String qualifCode;

    private java.util.Date qualifFromDate;

    private java.lang.String employmentOrgCode;

    private java.lang.String personRoleTypeId;

    private java.lang.String comments;

    private java.lang.String description;

    private java.util.Date fromDate;

    private java.util.Date thruDate;

    private java.lang.String employmentRoleTypeId;

    private java.lang.String employmentOrgComments;

    private java.lang.String allocationOrgCode;

    private java.lang.String allocationRoleTypeId;

    private java.lang.String allocationOrgComments;

    private java.lang.String evaluatorCode;

    private java.util.Date evaluatorFromDate;

    private java.lang.String contactMail;

    private java.lang.String contactMobile;

    private java.lang.String userLoginId;

    private java.lang.String groupId;

    private java.lang.String isEvalManager;

    private java.lang.String approverCode;

    private java.lang.String workEffortAssignmentCode;

    private java.util.Date workEffortDate;

    private java.lang.String employmentOrgDescription;

    private java.lang.String allocationOrgDescription;

    private java.util.Date allocationOrgFromDate;

    private java.util.Date allocationOrgThruDate;

    private java.lang.String fiscalCode;

    private java.util.Date employmentOrgFromDate;

    private java.util.Date employmentOrgThruDate;

    private java.util.Date emplPositionTypeDate;

    public PersonInterfaceExt_2() {
    }

    public PersonInterfaceExt_2(
           java.lang.String dataSource,
           java.util.Date refDate,
           java.lang.String personCode,
           java.lang.String firstName,
           java.lang.String lastName,
           java.lang.Integer employmentAmount,
           java.lang.String emplPositionTypeId,
           java.lang.String qualifCode,
           java.util.Date qualifFromDate,
           java.lang.String employmentOrgCode,
           java.lang.String personRoleTypeId,
           java.lang.String comments,
           java.lang.String description,
           java.util.Date fromDate,
           java.util.Date thruDate,
           java.lang.String employmentRoleTypeId,
           java.lang.String employmentOrgComments,
           java.lang.String allocationOrgCode,
           java.lang.String allocationRoleTypeId,
           java.lang.String allocationOrgComments,
           java.lang.String evaluatorCode,
           java.util.Date evaluatorFromDate,
           java.lang.String contactMail,
           java.lang.String contactMobile,
           java.lang.String userLoginId,
           java.lang.String groupId,
           java.lang.String isEvalManager,
           java.lang.String approverCode,
           java.lang.String workEffortAssignmentCode,
           java.util.Date workEffortDate,
           java.lang.String employmentOrgDescription,
           java.lang.String allocationOrgDescription,
           java.util.Date allocationOrgFromDate,
           java.util.Date allocationOrgThruDate,
           java.lang.String fiscalCode,
           java.util.Date employmentOrgFromDate,
           java.util.Date employmentOrgThruDate,
           java.util.Date emplPositionTypeDate) {
           this.dataSource = dataSource;
           this.refDate = refDate;
           this.personCode = personCode;
           this.firstName = firstName;
           this.lastName = lastName;
           this.employmentAmount = employmentAmount;
           this.emplPositionTypeId = emplPositionTypeId;
           this.qualifCode = qualifCode;
           this.qualifFromDate = qualifFromDate;
           this.employmentOrgCode = employmentOrgCode;
           this.personRoleTypeId = personRoleTypeId;
           this.comments = comments;
           this.description = description;
           this.fromDate = fromDate;
           this.thruDate = thruDate;
           this.employmentRoleTypeId = employmentRoleTypeId;
           this.employmentOrgComments = employmentOrgComments;
           this.allocationOrgCode = allocationOrgCode;
           this.allocationRoleTypeId = allocationRoleTypeId;
           this.allocationOrgComments = allocationOrgComments;
           this.evaluatorCode = evaluatorCode;
           this.evaluatorFromDate = evaluatorFromDate;
           this.contactMail = contactMail;
           this.contactMobile = contactMobile;
           this.userLoginId = userLoginId;
           this.groupId = groupId;
           this.isEvalManager = isEvalManager;
           this.approverCode = approverCode;
           this.workEffortAssignmentCode = workEffortAssignmentCode;
           this.workEffortDate = workEffortDate;
           this.employmentOrgDescription = employmentOrgDescription;
           this.allocationOrgDescription = allocationOrgDescription;
           this.allocationOrgFromDate = allocationOrgFromDate;
           this.allocationOrgThruDate = allocationOrgThruDate;
           this.fiscalCode = fiscalCode;
           this.employmentOrgFromDate = employmentOrgFromDate;
           this.employmentOrgThruDate = employmentOrgThruDate;
           this.emplPositionTypeDate = emplPositionTypeDate;
    }


    /**
     * Gets the dataSource value for this PersonInterfaceExt_2.
     * 
     * @return dataSource
     */
    public java.lang.String getDataSource() {
        return dataSource;
    }


    /**
     * Sets the dataSource value for this PersonInterfaceExt_2.
     * 
     * @param dataSource
     */
    public void setDataSource(java.lang.String dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * Gets the refDate value for this PersonInterfaceExt_2.
     * 
     * @return refDate
     */
    public java.util.Date getRefDate() {
        return refDate;
    }


    /**
     * Sets the refDate value for this PersonInterfaceExt_2.
     * 
     * @param refDate
     */
    public void setRefDate(java.util.Date refDate) {
        this.refDate = refDate;
    }


    /**
     * Gets the personCode value for this PersonInterfaceExt_2.
     * 
     * @return personCode
     */
    public java.lang.String getPersonCode() {
        return personCode;
    }


    /**
     * Sets the personCode value for this PersonInterfaceExt_2.
     * 
     * @param personCode
     */
    public void setPersonCode(java.lang.String personCode) {
        this.personCode = personCode;
    }


    /**
     * Gets the firstName value for this PersonInterfaceExt_2.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this PersonInterfaceExt_2.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this PersonInterfaceExt_2.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this PersonInterfaceExt_2.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the employmentAmount value for this PersonInterfaceExt_2.
     * 
     * @return employmentAmount
     */
    public java.lang.Integer getEmploymentAmount() {
        return employmentAmount;
    }


    /**
     * Sets the employmentAmount value for this PersonInterfaceExt_2.
     * 
     * @param employmentAmount
     */
    public void setEmploymentAmount(java.lang.Integer employmentAmount) {
        this.employmentAmount = employmentAmount;
    }


    /**
     * Gets the emplPositionTypeId value for this PersonInterfaceExt_2.
     * 
     * @return emplPositionTypeId
     */
    public java.lang.String getEmplPositionTypeId() {
        return emplPositionTypeId;
    }


    /**
     * Sets the emplPositionTypeId value for this PersonInterfaceExt_2.
     * 
     * @param emplPositionTypeId
     */
    public void setEmplPositionTypeId(java.lang.String emplPositionTypeId) {
        this.emplPositionTypeId = emplPositionTypeId;
    }


    /**
     * Gets the qualifCode value for this PersonInterfaceExt_2.
     * 
     * @return qualifCode
     */
    public java.lang.String getQualifCode() {
        return qualifCode;
    }


    /**
     * Sets the qualifCode value for this PersonInterfaceExt_2.
     * 
     * @param qualifCode
     */
    public void setQualifCode(java.lang.String qualifCode) {
        this.qualifCode = qualifCode;
    }


    /**
     * Gets the qualifFromDate value for this PersonInterfaceExt_2.
     * 
     * @return qualifFromDate
     */
    public java.util.Date getQualifFromDate() {
        return qualifFromDate;
    }


    /**
     * Sets the qualifFromDate value for this PersonInterfaceExt_2.
     * 
     * @param qualifFromDate
     */
    public void setQualifFromDate(java.util.Date qualifFromDate) {
        this.qualifFromDate = qualifFromDate;
    }


    /**
     * Gets the employmentOrgCode value for this PersonInterfaceExt_2.
     * 
     * @return employmentOrgCode
     */
    public java.lang.String getEmploymentOrgCode() {
        return employmentOrgCode;
    }


    /**
     * Sets the employmentOrgCode value for this PersonInterfaceExt_2.
     * 
     * @param employmentOrgCode
     */
    public void setEmploymentOrgCode(java.lang.String employmentOrgCode) {
        this.employmentOrgCode = employmentOrgCode;
    }


    /**
     * Gets the personRoleTypeId value for this PersonInterfaceExt_2.
     * 
     * @return personRoleTypeId
     */
    public java.lang.String getPersonRoleTypeId() {
        return personRoleTypeId;
    }


    /**
     * Sets the personRoleTypeId value for this PersonInterfaceExt_2.
     * 
     * @param personRoleTypeId
     */
    public void setPersonRoleTypeId(java.lang.String personRoleTypeId) {
        this.personRoleTypeId = personRoleTypeId;
    }


    /**
     * Gets the comments value for this PersonInterfaceExt_2.
     * 
     * @return comments
     */
    public java.lang.String getComments() {
        return comments;
    }


    /**
     * Sets the comments value for this PersonInterfaceExt_2.
     * 
     * @param comments
     */
    public void setComments(java.lang.String comments) {
        this.comments = comments;
    }


    /**
     * Gets the description value for this PersonInterfaceExt_2.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this PersonInterfaceExt_2.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the fromDate value for this PersonInterfaceExt_2.
     * 
     * @return fromDate
     */
    public java.util.Date getFromDate() {
        return fromDate;
    }


    /**
     * Sets the fromDate value for this PersonInterfaceExt_2.
     * 
     * @param fromDate
     */
    public void setFromDate(java.util.Date fromDate) {
        this.fromDate = fromDate;
    }


    /**
     * Gets the thruDate value for this PersonInterfaceExt_2.
     * 
     * @return thruDate
     */
    public java.util.Date getThruDate() {
        return thruDate;
    }


    /**
     * Sets the thruDate value for this PersonInterfaceExt_2.
     * 
     * @param thruDate
     */
    public void setThruDate(java.util.Date thruDate) {
        this.thruDate = thruDate;
    }


    /**
     * Gets the employmentRoleTypeId value for this PersonInterfaceExt_2.
     * 
     * @return employmentRoleTypeId
     */
    public java.lang.String getEmploymentRoleTypeId() {
        return employmentRoleTypeId;
    }


    /**
     * Sets the employmentRoleTypeId value for this PersonInterfaceExt_2.
     * 
     * @param employmentRoleTypeId
     */
    public void setEmploymentRoleTypeId(java.lang.String employmentRoleTypeId) {
        this.employmentRoleTypeId = employmentRoleTypeId;
    }


    /**
     * Gets the employmentOrgComments value for this PersonInterfaceExt_2.
     * 
     * @return employmentOrgComments
     */
    public java.lang.String getEmploymentOrgComments() {
        return employmentOrgComments;
    }


    /**
     * Sets the employmentOrgComments value for this PersonInterfaceExt_2.
     * 
     * @param employmentOrgComments
     */
    public void setEmploymentOrgComments(java.lang.String employmentOrgComments) {
        this.employmentOrgComments = employmentOrgComments;
    }


    /**
     * Gets the allocationOrgCode value for this PersonInterfaceExt_2.
     * 
     * @return allocationOrgCode
     */
    public java.lang.String getAllocationOrgCode() {
        return allocationOrgCode;
    }


    /**
     * Sets the allocationOrgCode value for this PersonInterfaceExt_2.
     * 
     * @param allocationOrgCode
     */
    public void setAllocationOrgCode(java.lang.String allocationOrgCode) {
        this.allocationOrgCode = allocationOrgCode;
    }


    /**
     * Gets the allocationRoleTypeId value for this PersonInterfaceExt_2.
     * 
     * @return allocationRoleTypeId
     */
    public java.lang.String getAllocationRoleTypeId() {
        return allocationRoleTypeId;
    }


    /**
     * Sets the allocationRoleTypeId value for this PersonInterfaceExt_2.
     * 
     * @param allocationRoleTypeId
     */
    public void setAllocationRoleTypeId(java.lang.String allocationRoleTypeId) {
        this.allocationRoleTypeId = allocationRoleTypeId;
    }


    /**
     * Gets the allocationOrgComments value for this PersonInterfaceExt_2.
     * 
     * @return allocationOrgComments
     */
    public java.lang.String getAllocationOrgComments() {
        return allocationOrgComments;
    }


    /**
     * Sets the allocationOrgComments value for this PersonInterfaceExt_2.
     * 
     * @param allocationOrgComments
     */
    public void setAllocationOrgComments(java.lang.String allocationOrgComments) {
        this.allocationOrgComments = allocationOrgComments;
    }


    /**
     * Gets the evaluatorCode value for this PersonInterfaceExt_2.
     * 
     * @return evaluatorCode
     */
    public java.lang.String getEvaluatorCode() {
        return evaluatorCode;
    }


    /**
     * Sets the evaluatorCode value for this PersonInterfaceExt_2.
     * 
     * @param evaluatorCode
     */
    public void setEvaluatorCode(java.lang.String evaluatorCode) {
        this.evaluatorCode = evaluatorCode;
    }


    /**
     * Gets the evaluatorFromDate value for this PersonInterfaceExt_2.
     * 
     * @return evaluatorFromDate
     */
    public java.util.Date getEvaluatorFromDate() {
        return evaluatorFromDate;
    }


    /**
     * Sets the evaluatorFromDate value for this PersonInterfaceExt_2.
     * 
     * @param evaluatorFromDate
     */
    public void setEvaluatorFromDate(java.util.Date evaluatorFromDate) {
        this.evaluatorFromDate = evaluatorFromDate;
    }


    /**
     * Gets the contactMail value for this PersonInterfaceExt_2.
     * 
     * @return contactMail
     */
    public java.lang.String getContactMail() {
        return contactMail;
    }


    /**
     * Sets the contactMail value for this PersonInterfaceExt_2.
     * 
     * @param contactMail
     */
    public void setContactMail(java.lang.String contactMail) {
        this.contactMail = contactMail;
    }


    /**
     * Gets the contactMobile value for this PersonInterfaceExt_2.
     * 
     * @return contactMobile
     */
    public java.lang.String getContactMobile() {
        return contactMobile;
    }


    /**
     * Sets the contactMobile value for this PersonInterfaceExt_2.
     * 
     * @param contactMobile
     */
    public void setContactMobile(java.lang.String contactMobile) {
        this.contactMobile = contactMobile;
    }


    /**
     * Gets the userLoginId value for this PersonInterfaceExt_2.
     * 
     * @return userLoginId
     */
    public java.lang.String getUserLoginId() {
        return userLoginId;
    }


    /**
     * Sets the userLoginId value for this PersonInterfaceExt_2.
     * 
     * @param userLoginId
     */
    public void setUserLoginId(java.lang.String userLoginId) {
        this.userLoginId = userLoginId;
    }


    /**
     * Gets the groupId value for this PersonInterfaceExt_2.
     * 
     * @return groupId
     */
    public java.lang.String getGroupId() {
        return groupId;
    }


    /**
     * Sets the groupId value for this PersonInterfaceExt_2.
     * 
     * @param groupId
     */
    public void setGroupId(java.lang.String groupId) {
        this.groupId = groupId;
    }


    /**
     * Gets the isEvalManager value for this PersonInterfaceExt_2.
     * 
     * @return isEvalManager
     */
    public java.lang.String getIsEvalManager() {
        return isEvalManager;
    }


    /**
     * Sets the isEvalManager value for this PersonInterfaceExt_2.
     * 
     * @param isEvalManager
     */
    public void setIsEvalManager(java.lang.String isEvalManager) {
        this.isEvalManager = isEvalManager;
    }


    /**
     * Gets the approverCode value for this PersonInterfaceExt_2.
     * 
     * @return approverCode
     */
    public java.lang.String getApproverCode() {
        return approverCode;
    }


    /**
     * Sets the approverCode value for this PersonInterfaceExt_2.
     * 
     * @param approverCode
     */
    public void setApproverCode(java.lang.String approverCode) {
        this.approverCode = approverCode;
    }


    /**
     * Gets the workEffortAssignmentCode value for this PersonInterfaceExt_2.
     * 
     * @return workEffortAssignmentCode
     */
    public java.lang.String getWorkEffortAssignmentCode() {
        return workEffortAssignmentCode;
    }


    /**
     * Sets the workEffortAssignmentCode value for this PersonInterfaceExt_2.
     * 
     * @param workEffortAssignmentCode
     */
    public void setWorkEffortAssignmentCode(java.lang.String workEffortAssignmentCode) {
        this.workEffortAssignmentCode = workEffortAssignmentCode;
    }


    /**
     * Gets the workEffortDate value for this PersonInterfaceExt_2.
     * 
     * @return workEffortDate
     */
    public java.util.Date getWorkEffortDate() {
        return workEffortDate;
    }


    /**
     * Sets the workEffortDate value for this PersonInterfaceExt_2.
     * 
     * @param workEffortDate
     */
    public void setWorkEffortDate(java.util.Date workEffortDate) {
        this.workEffortDate = workEffortDate;
    }


    /**
     * Gets the employmentOrgDescription value for this PersonInterfaceExt_2.
     * 
     * @return employmentOrgDescription
     */
    public java.lang.String getEmploymentOrgDescription() {
        return employmentOrgDescription;
    }


    /**
     * Sets the employmentOrgDescription value for this PersonInterfaceExt_2.
     * 
     * @param employmentOrgDescription
     */
    public void setEmploymentOrgDescription(java.lang.String employmentOrgDescription) {
        this.employmentOrgDescription = employmentOrgDescription;
    }


    /**
     * Gets the allocationOrgDescription value for this PersonInterfaceExt_2.
     * 
     * @return allocationOrgDescription
     */
    public java.lang.String getAllocationOrgDescription() {
        return allocationOrgDescription;
    }


    /**
     * Sets the allocationOrgDescription value for this PersonInterfaceExt_2.
     * 
     * @param allocationOrgDescription
     */
    public void setAllocationOrgDescription(java.lang.String allocationOrgDescription) {
        this.allocationOrgDescription = allocationOrgDescription;
    }


    /**
     * Gets the allocationOrgFromDate value for this PersonInterfaceExt_2.
     * 
     * @return allocationOrgFromDate
     */
    public java.util.Date getAllocationOrgFromDate() {
        return allocationOrgFromDate;
    }


    /**
     * Sets the allocationOrgFromDate value for this PersonInterfaceExt_2.
     * 
     * @param allocationOrgFromDate
     */
    public void setAllocationOrgFromDate(java.util.Date allocationOrgFromDate) {
        this.allocationOrgFromDate = allocationOrgFromDate;
    }


    /**
     * Gets the allocationOrgThruDate value for this PersonInterfaceExt_2.
     * 
     * @return allocationOrgThruDate
     */
    public java.util.Date getAllocationOrgThruDate() {
        return allocationOrgThruDate;
    }


    /**
     * Sets the allocationOrgThruDate value for this PersonInterfaceExt_2.
     * 
     * @param allocationOrgThruDate
     */
    public void setAllocationOrgThruDate(java.util.Date allocationOrgThruDate) {
        this.allocationOrgThruDate = allocationOrgThruDate;
    }


    /**
     * Gets the fiscalCode value for this PersonInterfaceExt_2.
     * 
     * @return fiscalCode
     */
    public java.lang.String getFiscalCode() {
        return fiscalCode;
    }


    /**
     * Sets the fiscalCode value for this PersonInterfaceExt_2.
     * 
     * @param fiscalCode
     */
    public void setFiscalCode(java.lang.String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }


    /**
     * Gets the employmentOrgFromDate value for this PersonInterfaceExt_2.
     * 
     * @return employmentOrgFromDate
     */
    public java.util.Date getEmploymentOrgFromDate() {
        return employmentOrgFromDate;
    }


    /**
     * Sets the employmentOrgFromDate value for this PersonInterfaceExt_2.
     * 
     * @param employmentOrgFromDate
     */
    public void setEmploymentOrgFromDate(java.util.Date employmentOrgFromDate) {
        this.employmentOrgFromDate = employmentOrgFromDate;
    }


    /**
     * Gets the employmentOrgThruDate value for this PersonInterfaceExt_2.
     * 
     * @return employmentOrgThruDate
     */
    public java.util.Date getEmploymentOrgThruDate() {
        return employmentOrgThruDate;
    }


    /**
     * Sets the employmentOrgThruDate value for this PersonInterfaceExt_2.
     * 
     * @param employmentOrgThruDate
     */
    public void setEmploymentOrgThruDate(java.util.Date employmentOrgThruDate) {
        this.employmentOrgThruDate = employmentOrgThruDate;
    }


    /**
     * Gets the emplPositionTypeDate value for this PersonInterfaceExt_2.
     * 
     * @return emplPositionTypeDate
     */
    public java.util.Date getEmplPositionTypeDate() {
        return emplPositionTypeDate;
    }


    /**
     * Sets the emplPositionTypeDate value for this PersonInterfaceExt_2.
     * 
     * @param emplPositionTypeDate
     */
    public void setEmplPositionTypeDate(java.util.Date emplPositionTypeDate) {
        this.emplPositionTypeDate = emplPositionTypeDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PersonInterfaceExt_2)) return false;
        PersonInterfaceExt_2 other = (PersonInterfaceExt_2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dataSource==null && other.getDataSource()==null) || 
             (this.dataSource!=null &&
              this.dataSource.equals(other.getDataSource()))) &&
            ((this.refDate==null && other.getRefDate()==null) || 
             (this.refDate!=null &&
              this.refDate.equals(other.getRefDate()))) &&
            ((this.personCode==null && other.getPersonCode()==null) || 
             (this.personCode!=null &&
              this.personCode.equals(other.getPersonCode()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.employmentAmount==null && other.getEmploymentAmount()==null) || 
             (this.employmentAmount!=null &&
              this.employmentAmount.equals(other.getEmploymentAmount()))) &&
            ((this.emplPositionTypeId==null && other.getEmplPositionTypeId()==null) || 
             (this.emplPositionTypeId!=null &&
              this.emplPositionTypeId.equals(other.getEmplPositionTypeId()))) &&
            ((this.qualifCode==null && other.getQualifCode()==null) || 
             (this.qualifCode!=null &&
              this.qualifCode.equals(other.getQualifCode()))) &&
            ((this.qualifFromDate==null && other.getQualifFromDate()==null) || 
             (this.qualifFromDate!=null &&
              this.qualifFromDate.equals(other.getQualifFromDate()))) &&
            ((this.employmentOrgCode==null && other.getEmploymentOrgCode()==null) || 
             (this.employmentOrgCode!=null &&
              this.employmentOrgCode.equals(other.getEmploymentOrgCode()))) &&
            ((this.personRoleTypeId==null && other.getPersonRoleTypeId()==null) || 
             (this.personRoleTypeId!=null &&
              this.personRoleTypeId.equals(other.getPersonRoleTypeId()))) &&
            ((this.comments==null && other.getComments()==null) || 
             (this.comments!=null &&
              this.comments.equals(other.getComments()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.fromDate==null && other.getFromDate()==null) || 
             (this.fromDate!=null &&
              this.fromDate.equals(other.getFromDate()))) &&
            ((this.thruDate==null && other.getThruDate()==null) || 
             (this.thruDate!=null &&
              this.thruDate.equals(other.getThruDate()))) &&
            ((this.employmentRoleTypeId==null && other.getEmploymentRoleTypeId()==null) || 
             (this.employmentRoleTypeId!=null &&
              this.employmentRoleTypeId.equals(other.getEmploymentRoleTypeId()))) &&
            ((this.employmentOrgComments==null && other.getEmploymentOrgComments()==null) || 
             (this.employmentOrgComments!=null &&
              this.employmentOrgComments.equals(other.getEmploymentOrgComments()))) &&
            ((this.allocationOrgCode==null && other.getAllocationOrgCode()==null) || 
             (this.allocationOrgCode!=null &&
              this.allocationOrgCode.equals(other.getAllocationOrgCode()))) &&
            ((this.allocationRoleTypeId==null && other.getAllocationRoleTypeId()==null) || 
             (this.allocationRoleTypeId!=null &&
              this.allocationRoleTypeId.equals(other.getAllocationRoleTypeId()))) &&
            ((this.allocationOrgComments==null && other.getAllocationOrgComments()==null) || 
             (this.allocationOrgComments!=null &&
              this.allocationOrgComments.equals(other.getAllocationOrgComments()))) &&
            ((this.evaluatorCode==null && other.getEvaluatorCode()==null) || 
             (this.evaluatorCode!=null &&
              this.evaluatorCode.equals(other.getEvaluatorCode()))) &&
            ((this.evaluatorFromDate==null && other.getEvaluatorFromDate()==null) || 
             (this.evaluatorFromDate!=null &&
              this.evaluatorFromDate.equals(other.getEvaluatorFromDate()))) &&
            ((this.contactMail==null && other.getContactMail()==null) || 
             (this.contactMail!=null &&
              this.contactMail.equals(other.getContactMail()))) &&
            ((this.contactMobile==null && other.getContactMobile()==null) || 
             (this.contactMobile!=null &&
              this.contactMobile.equals(other.getContactMobile()))) &&
            ((this.userLoginId==null && other.getUserLoginId()==null) || 
             (this.userLoginId!=null &&
              this.userLoginId.equals(other.getUserLoginId()))) &&
            ((this.groupId==null && other.getGroupId()==null) || 
             (this.groupId!=null &&
              this.groupId.equals(other.getGroupId()))) &&
            ((this.isEvalManager==null && other.getIsEvalManager()==null) || 
             (this.isEvalManager!=null &&
              this.isEvalManager.equals(other.getIsEvalManager()))) &&
            ((this.approverCode==null && other.getApproverCode()==null) || 
             (this.approverCode!=null &&
              this.approverCode.equals(other.getApproverCode()))) &&
            ((this.workEffortAssignmentCode==null && other.getWorkEffortAssignmentCode()==null) || 
             (this.workEffortAssignmentCode!=null &&
              this.workEffortAssignmentCode.equals(other.getWorkEffortAssignmentCode()))) &&
            ((this.workEffortDate==null && other.getWorkEffortDate()==null) || 
             (this.workEffortDate!=null &&
              this.workEffortDate.equals(other.getWorkEffortDate()))) &&
            ((this.employmentOrgDescription==null && other.getEmploymentOrgDescription()==null) || 
             (this.employmentOrgDescription!=null &&
              this.employmentOrgDescription.equals(other.getEmploymentOrgDescription()))) &&
            ((this.allocationOrgDescription==null && other.getAllocationOrgDescription()==null) || 
             (this.allocationOrgDescription!=null &&
              this.allocationOrgDescription.equals(other.getAllocationOrgDescription()))) &&
            ((this.allocationOrgFromDate==null && other.getAllocationOrgFromDate()==null) || 
             (this.allocationOrgFromDate!=null &&
              this.allocationOrgFromDate.equals(other.getAllocationOrgFromDate()))) &&
            ((this.allocationOrgThruDate==null && other.getAllocationOrgThruDate()==null) || 
             (this.allocationOrgThruDate!=null &&
              this.allocationOrgThruDate.equals(other.getAllocationOrgThruDate()))) &&
            ((this.fiscalCode==null && other.getFiscalCode()==null) || 
             (this.fiscalCode!=null &&
              this.fiscalCode.equals(other.getFiscalCode()))) &&
            ((this.employmentOrgFromDate==null && other.getEmploymentOrgFromDate()==null) || 
             (this.employmentOrgFromDate!=null &&
              this.employmentOrgFromDate.equals(other.getEmploymentOrgFromDate()))) &&
            ((this.employmentOrgThruDate==null && other.getEmploymentOrgThruDate()==null) || 
             (this.employmentOrgThruDate!=null &&
              this.employmentOrgThruDate.equals(other.getEmploymentOrgThruDate()))) &&
            ((this.emplPositionTypeDate==null && other.getEmplPositionTypeDate()==null) || 
             (this.emplPositionTypeDate!=null &&
              this.emplPositionTypeDate.equals(other.getEmplPositionTypeDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDataSource() != null) {
            _hashCode += getDataSource().hashCode();
        }
        if (getRefDate() != null) {
            _hashCode += getRefDate().hashCode();
        }
        if (getPersonCode() != null) {
            _hashCode += getPersonCode().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getEmploymentAmount() != null) {
            _hashCode += getEmploymentAmount().hashCode();
        }
        if (getEmplPositionTypeId() != null) {
            _hashCode += getEmplPositionTypeId().hashCode();
        }
        if (getQualifCode() != null) {
            _hashCode += getQualifCode().hashCode();
        }
        if (getQualifFromDate() != null) {
            _hashCode += getQualifFromDate().hashCode();
        }
        if (getEmploymentOrgCode() != null) {
            _hashCode += getEmploymentOrgCode().hashCode();
        }
        if (getPersonRoleTypeId() != null) {
            _hashCode += getPersonRoleTypeId().hashCode();
        }
        if (getComments() != null) {
            _hashCode += getComments().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getFromDate() != null) {
            _hashCode += getFromDate().hashCode();
        }
        if (getThruDate() != null) {
            _hashCode += getThruDate().hashCode();
        }
        if (getEmploymentRoleTypeId() != null) {
            _hashCode += getEmploymentRoleTypeId().hashCode();
        }
        if (getEmploymentOrgComments() != null) {
            _hashCode += getEmploymentOrgComments().hashCode();
        }
        if (getAllocationOrgCode() != null) {
            _hashCode += getAllocationOrgCode().hashCode();
        }
        if (getAllocationRoleTypeId() != null) {
            _hashCode += getAllocationRoleTypeId().hashCode();
        }
        if (getAllocationOrgComments() != null) {
            _hashCode += getAllocationOrgComments().hashCode();
        }
        if (getEvaluatorCode() != null) {
            _hashCode += getEvaluatorCode().hashCode();
        }
        if (getEvaluatorFromDate() != null) {
            _hashCode += getEvaluatorFromDate().hashCode();
        }
        if (getContactMail() != null) {
            _hashCode += getContactMail().hashCode();
        }
        if (getContactMobile() != null) {
            _hashCode += getContactMobile().hashCode();
        }
        if (getUserLoginId() != null) {
            _hashCode += getUserLoginId().hashCode();
        }
        if (getGroupId() != null) {
            _hashCode += getGroupId().hashCode();
        }
        if (getIsEvalManager() != null) {
            _hashCode += getIsEvalManager().hashCode();
        }
        if (getApproverCode() != null) {
            _hashCode += getApproverCode().hashCode();
        }
        if (getWorkEffortAssignmentCode() != null) {
            _hashCode += getWorkEffortAssignmentCode().hashCode();
        }
        if (getWorkEffortDate() != null) {
            _hashCode += getWorkEffortDate().hashCode();
        }
        if (getEmploymentOrgDescription() != null) {
            _hashCode += getEmploymentOrgDescription().hashCode();
        }
        if (getAllocationOrgDescription() != null) {
            _hashCode += getAllocationOrgDescription().hashCode();
        }
        if (getAllocationOrgFromDate() != null) {
            _hashCode += getAllocationOrgFromDate().hashCode();
        }
        if (getAllocationOrgThruDate() != null) {
            _hashCode += getAllocationOrgThruDate().hashCode();
        }
        if (getFiscalCode() != null) {
            _hashCode += getFiscalCode().hashCode();
        }
        if (getEmploymentOrgFromDate() != null) {
            _hashCode += getEmploymentOrgFromDate().hashCode();
        }
        if (getEmploymentOrgThruDate() != null) {
            _hashCode += getEmploymentOrgThruDate().hashCode();
        }
        if (getEmplPositionTypeDate() != null) {
            _hashCode += getEmplPositionTypeDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PersonInterfaceExt_2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.standardImport.mapsengineering.com/", "personInterfaceExt_2"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataSource");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "personCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "firstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employmentAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employmentAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emplPositionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emplPositionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qualifCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qualifCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qualifFromDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qualifFromDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employmentOrgCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employmentOrgCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personRoleTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "personRoleTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comments");
        elemField.setXmlName(new javax.xml.namespace.QName("", "comments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fromDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("thruDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "thruDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employmentRoleTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employmentRoleTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employmentOrgComments");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employmentOrgComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocationOrgCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allocationOrgCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocationRoleTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allocationRoleTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocationOrgComments");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allocationOrgComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("evaluatorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "evaluatorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("evaluatorFromDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "evaluatorFromDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactMail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "contactMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactMobile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "contactMobile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userLoginId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userLoginId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isEvalManager");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isEvalManager"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approverCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "approverCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workEffortAssignmentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workEffortAssignmentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workEffortDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workEffortDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employmentOrgDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employmentOrgDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocationOrgDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allocationOrgDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocationOrgFromDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allocationOrgFromDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocationOrgThruDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allocationOrgThruDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fiscalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fiscalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employmentOrgFromDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employmentOrgFromDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employmentOrgThruDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employmentOrgThruDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emplPositionTypeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emplPositionTypeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
