import org.ofbiz.base.util.*;

def result = "success";

def enableNewReport = UtilProperties.getPropertyValue("BaseConfig", "report.enableNewReport", "Y");
if ("Y".equals(enableNewReport)) {    
    result = "successNewReport";
}

return result;