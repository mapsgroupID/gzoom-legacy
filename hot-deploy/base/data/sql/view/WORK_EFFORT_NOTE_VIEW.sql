CREATE VIEW WORK_EFFORT_NOTE_VIEW (
    WORK_EFFORT_ID,
    NOTE_ID,
    INTERNAL_NOTE,
    IS_MAIN,
    IS_HTML,
    SEQUENCE_ID,
    IS_POSTED,
    NOTE_NAME,
    NOTE_DATE_TIME,
    NOTE_INFO
) AS
SELECT
    WEN.WORK_EFFORT_ID,
    WEN.NOTE_ID,
    WEN.INTERNAL_NOTE,
    WEN.IS_MAIN,
    WEN.IS_HTML,
    WEN.SEQUENCE_ID,
    WEN.IS_POSTED,
    ND.NOTE_NAME,
    ND.NOTE_DATE_TIME,
    ND.NOTE_INFO
FROM
    WORK_EFFORT_NOTE WEN
    INNER JOIN NOTE_DATA ND ON ND.NOTE_ID = WEN.NOTE_ID