DROP VIEW GL_ACCOUNT_AND_MEASURE_VIEW;
DROP VIEW MEASURE_AND_PRODUCT_VIEW;
DROP VIEW PARTY_ROLE_VIEW;
DROP VIEW WORK_EFFORT_ASSOC_VIEW;
DROP VIEW WORK_EFFORT_TRANS_ALL_VIEW;
DROP VIEW WORK_EFFORT_TRANS_NOOBJ_VIEW;
DROP VIEW WORK_EFFORT_TRANS_OBJ_VIEW;
DROP VIEW WORK_EFFORT_TRANS_INDIC_VIEW;
DROP VIEW WORK_EFFORT_TRANS_UO_VIEW;
DROP VIEW ACCTG_TRANS_VIEW;
DROP VIEW WORK_EFFORT_NOTE_VIEW;
DROP VIEW WORK_EFFORT_VIEW;
DROP VIEW WORK_EFFORT_VIEW_ASS;
DROP VIEW PARTY_HISTORY_VIEW;
DROP VIEW WORK_EFFORT_VIEW_REF;
DROP VIEW GL_ACCOUNT_WITH_WORK_EFFORT_PURPOSE_TYPE_VIEW;
DROP VIEW OBO_RESP_POL;
DROP VIEW SCORE_VIEW;
DROP VIEW REND_PIAO_VIEW;
DROP VIEW PERF_INDIC_VIEW;