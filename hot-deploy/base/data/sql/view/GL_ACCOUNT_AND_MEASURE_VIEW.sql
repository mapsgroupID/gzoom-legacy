CREATE VIEW GL_ACCOUNT_AND_MEASURE_VIEW (
    IND_GL_ACCOUNT_ID,
    IND_PRODUCT_ID,
    IND_PRODUCT_MAIN_CODE,
    IND_INTERNAL_NAME,
    IND_UOM_DESCR,
    IND_DETAIL_ENUM_ID,
    WORK_EFFORT_TYPE_ID_IND,
    CONTENT_ID_IND,
    WE_PURPOSE_TYPE_ID_IND,
    PURPOSE_TYPE_ENUM_ID,
    UOM_DESCR,
    UOM_DESCR_LANG,
    UOM_TYPE_ID,
    ABBREVIATION,
    ABBREVIATION_LANG,
    GL_ACCOUNT_TYPE_DESCR,
    GL_ACCOUNT_TYPE_DESCR_LANG,
    UOM_RANGE_ID,
    UOM_RANGE_DESCRIPTION,
    GL_ACCOUNT_ID,
    ACCOUNT_CODE,
    ACCOUNT_NAME,
    ACCOUNT_NAME_LANG,
    DESCRIPTION,
    DESCRIPTION_LANG,
    PRODUCT_ID,
    CURRENT_STATUS_ID,
    INPUT_ENUM_ID,
    DETAIL_ENUM_ID,
    WE_MEASURE_TYPE_ENUM_ID,
    WE_SCORE_RANGE_ENUM_ID,
    WE_SCORE_CONV_ENUM_ID,
    WE_ALERT_RULE_ENUM_ID,
    WE_WITHOUT_PERF,
    PERIOD_TYPE_ID,
    PERIOD_TYPE_DESCRIPTION,
    RESP_CENTER_ID,
    RESP_CENTER_NAME,
    RESP_CENTER_NAME_LANG,
    MP_ORG_UNIT_ROLE_TYPE_ID,
    MP_ORG_UNIT_ID,
    MP_ORG_UNIT_NAME,
    MP_ORG_UNIT_NAME_LANG,
    MP_UOM_DESCR,
    MP_UOM_DESCR_LANG,
    ORGANIZATION_PARTY_ID
) AS
SELECT
    NULL AS IND_GL_ACCOUNT_ID,
    NULL AS IND_PRODUCT_ID,
    NULL AS IND_PRODUCT_MAIN_CODE,
    NULL AS IND_INTERNAL_NAME,
    NULL AS IND_UOM_DESCR,
    NULL AS IND_DETAIL_ENUM_ID,
    WTIC.WORK_EFFORT_TYPE_ID AS WORK_EFFORT_TYPE_ID_IND,
    WTIC.CONTENT_ID AS CONTENT_ID_IND,
    WTIC.WORK_EFFORT_PURPOSE_TYPE_ID AS WE_PURPOSE_TYPE_ID_IND,
    WPT.PURPOSE_TYPE_ENUM_ID AS PURPOSE_TYPE_ENUM_ID,
    U.DESCRIPTION AS UOM_DESCR,
    U.DESCRIPTION_LANG AS UOM_DESCR_LANG,
    U.UOM_TYPE_ID AS UOM_TYPE_ID,
	U.ABBREVIATION AS ABBREVIATION,
	U.ABBREVIATION_LANG AS ABBREVIATION_LANG,
    GT.DESCRIPTION AS GL_ACCOUNT_TYPE_DESCR,
    GT.DESCRIPTION_LANG AS GL_ACCOUNT_TYPE_DESCR_LANG,
    G.UOM_RANGE_ID AS UOM_RANGE_ID,
    UR.DESCRIPTION AS UOM_RANGE_DESCRIPTION,
    G.GL_ACCOUNT_ID AS GL_ACCOUNT_ID,
    G.ACCOUNT_CODE AS ACCOUNT_CODE,
    G.ACCOUNT_NAME AS ACCOUNT_NAME,
    G.ACCOUNT_NAME_LANG AS ACCOUNT_NAME_LANG,
    G.DESCRIPTION AS DESCRIPTION,
    G.DESCRIPTION_LANG AS DESCRIPTION_LANG,
    G.PRODUCT_ID AS PRODUCT_ID,
    G.CURRENT_STATUS_ID AS CURRENT_STATUS_ID,
    G.INPUT_ENUM_ID AS INPUT_ENUM_ID,
    G.DETAIL_ENUM_ID AS DETAIL_ENUM_ID,
    G.WE_MEASURE_TYPE_ENUM_ID AS WE_MEASURE_TYPE_ENUM_ID,
    G.WE_SCORE_RANGE_ENUM_ID AS WE_SCORE_RANGE_ENUM_ID,
    G.WE_SCORE_CONV_ENUM_ID AS WE_SCORE_CONV_ENUM_ID,
    G.WE_ALERT_RULE_ENUM_ID AS WE_ALERT_RULE_ENUM_ID,
    G.WE_WITHOUT_PERF,
    G.PERIOD_TYPE_ID AS PERIOD_TYPE_ID,
    PT.DESCRIPTION AS PERIOD_TYPE_DESCRIPTION,
    G.RESP_CENTER_ID AS RESP_CENTER_ID,
    PG.GROUP_NAME AS RESP_CENTER_NAME,
    PG.GROUP_NAME_LANG AS RESP_CENTER_NAME_LANG,
    MP.ORG_UNIT_ID AS MP_ORG_UNIT_ID,
    MP.ORG_UNIT_ROLE_TYPE_ID AS MP_ORG_UNIT_ROLE_TYPE_ID,
    MP.ORG_UNIT_NAME AS MP_ORG_UNIT_NAME,
    MP.ORG_UNIT_NAME_LANG AS MP_ORG_UNIT_NAME_LANG,
    MP.WM_UOM_DESCR AS MP_UOM_DESCR,
    MP.WM_UOM_DESCR_LANG AS MP_UOM_DESCR_LANG,
    GO.ORGANIZATION_PARTY_ID
FROM
    GL_ACCOUNT G
    LEFT OUTER JOIN PARTY_GROUP PG ON G.RESP_CENTER_ID = PG.PARTY_ID
    INNER JOIN WORK_EFFORT_PURPOSE_ACCOUNT WA ON G.GL_ACCOUNT_ID = WA.GL_ACCOUNT_ID
    INNER JOIN WORK_EFFORT_PURPOSE_TYPE WPT ON WA.WORK_EFFORT_PURPOSE_TYPE_ID = WPT.WORK_EFFORT_PURPOSE_TYPE_ID
    LEFT OUTER JOIN WORK_EFFORT_TYPE WTR ON WA.WORK_EFFORT_PURPOSE_TYPE_ID = WTR.WE_PURPOSE_TYPE_ID_RES
    LEFT OUTER JOIN WORK_EFFORT_TYPE_CONTENT WTIC ON WA.WORK_EFFORT_PURPOSE_TYPE_ID = WTIC.WORK_EFFORT_PURPOSE_TYPE_ID
    LEFT OUTER JOIN WORK_EFFORT_TYPE WTI ON WTIC.WORK_EFFORT_TYPE_ID = WTI.WORK_EFFORT_TYPE_ID
    LEFT OUTER JOIN UOM U ON G.DEFAULT_UOM_ID = U.UOM_ID
    LEFT OUTER JOIN UOM_RANGE UR ON G.UOM_RANGE_ID = UR.UOM_RANGE_ID
    INNER JOIN GL_ACCOUNT_TYPE GT ON G.GL_ACCOUNT_TYPE_ID = GT.GL_ACCOUNT_TYPE_ID
    LEFT OUTER JOIN PERIOD_TYPE PT ON G.PERIOD_TYPE_ID = PT.PERIOD_TYPE_ID
    LEFT OUTER JOIN MEASURE_AND_PRODUCT_VIEW MP ON G.GL_ACCOUNT_ID = MP.WM_GL_ACCOUNT_ID
    LEFT OUTER JOIN GL_ACCOUNT_ORGANIZATION GO ON GO.GL_ACCOUNT_ID = G.GL_ACCOUNT_ID
    WHERE G.INPUT_ENUM_ID = 'ACCINP_UO'

UNION ALL
SELECT 
    NULL AS IND_GL_ACCOUNT_ID,
    NULL AS IND_PRODUCT_ID,
    NULL AS IND_PRODUCT_MAIN_CODE,
    NULL AS IND_INTERNAL_NAME,
    NULL AS IND_UOM_DESCR,
    NULL AS IND_DETAIL_ENUM_ID,
    WTIC.WORK_EFFORT_TYPE_ID AS WORK_EFFORT_TYPE_ID_IND,
    WTIC.CONTENT_ID AS CONTENT_ID_IND,
    WTIC.WORK_EFFORT_PURPOSE_TYPE_ID AS WE_PURPOSE_TYPE_ID_IND,
    WPT.PURPOSE_TYPE_ENUM_ID AS PURPOSE_TYPE_ENUM_ID,
    U.DESCRIPTION AS UOM_DESCR,
    U.DESCRIPTION_LANG AS UOM_DESCR_LANG,
    U.UOM_TYPE_ID AS UOM_TYPE_ID,
    U.ABBREVIATION AS ABBREVIATION,
    U.ABBREVIATION_LANG AS ABBREVIATION_LANG,
    GT.DESCRIPTION AS GL_ACCOUNT_TYPE_DESCR,
    GT.DESCRIPTION_LANG AS GL_ACCOUNT_TYPE_DESCR_LANG,
    G.UOM_RANGE_ID AS UOM_RANGE_ID,
    UR.DESCRIPTION AS UOM_RANGE_DESCRIPTION,
    G.GL_ACCOUNT_ID AS GL_ACCOUNT_ID,
    G.ACCOUNT_CODE AS ACCOUNT_CODE,
    G.ACCOUNT_NAME AS ACCOUNT_NAME,
    G.ACCOUNT_NAME_LANG AS ACCOUNT_NAME_LANG,
    G.DESCRIPTION AS DESCRIPTION,
    G.DESCRIPTION_LANG AS DESCRIPTION_LANG,
    G.PRODUCT_ID AS PRODUCT_ID,
    G.CURRENT_STATUS_ID AS CURRENT_STATUS_ID,
    G.INPUT_ENUM_ID AS INPUT_ENUM_ID,
    G.DETAIL_ENUM_ID AS DETAIL_ENUM_ID,
    G.WE_MEASURE_TYPE_ENUM_ID AS WE_MEASURE_TYPE_ENUM_ID,
    G.WE_SCORE_RANGE_ENUM_ID AS WE_SCORE_RANGE_ENUM_ID,
    G.WE_SCORE_CONV_ENUM_ID AS WE_SCORE_CONV_ENUM_ID,
    G.WE_ALERT_RULE_ENUM_ID AS WE_ALERT_RULE_ENUM_ID,
    G.WE_WITHOUT_PERF,
    G.PERIOD_TYPE_ID AS PERIOD_TYPE_ID,
    PT.DESCRIPTION AS PERIOD_TYPE_DESCRIPTION,
    G.RESP_CENTER_ID AS RESP_CENTER_ID,
    PG.GROUP_NAME AS RESP_CENTER_NAME,
    PG.GROUP_NAME_LANG AS RESP_CENTER_NAME_LANG,
    MP.ORG_UNIT_ID AS MP_ORG_UNIT_ID,
    MP.ORG_UNIT_ROLE_TYPE_ID AS MP_ORG_UNIT_ROLE_TYPE_ID,
    MP.ORG_UNIT_NAME AS MP_ORG_UNIT_NAME,
    MP.ORG_UNIT_NAME_LANG AS MP_ORG_UNIT_NAME_LANG,
    MP.WM_UOM_DESCR AS MP_UOM_DESCR,
    MP.WM_UOM_DESCR_LANG AS MP_UOM_DESCR_LANG,
    GO.ORGANIZATION_PARTY_ID
FROM
    GL_ACCOUNT G
    INNER JOIN WORK_EFFORT_PURPOSE_ACCOUNT WA ON G.GL_ACCOUNT_ID = WA.GL_ACCOUNT_ID
    INNER JOIN WORK_EFFORT_PURPOSE_TYPE WPT ON WA.WORK_EFFORT_PURPOSE_TYPE_ID = WPT.WORK_EFFORT_PURPOSE_TYPE_ID
    LEFT OUTER JOIN PARTY_GROUP PG ON G.RESP_CENTER_ID = PG.PARTY_ID
    LEFT OUTER JOIN WORK_EFFORT_TYPE WTR ON WA.WORK_EFFORT_PURPOSE_TYPE_ID = WTR.WE_PURPOSE_TYPE_ID_RES
    LEFT OUTER JOIN WORK_EFFORT_TYPE_CONTENT WTIC ON WA.WORK_EFFORT_PURPOSE_TYPE_ID = WTIC.WORK_EFFORT_PURPOSE_TYPE_ID
    LEFT OUTER JOIN WORK_EFFORT_TYPE WTI ON WTIC.WORK_EFFORT_TYPE_ID = WTI.WORK_EFFORT_TYPE_ID
    LEFT OUTER JOIN UOM U ON G.DEFAULT_UOM_ID = U.UOM_ID
    LEFT OUTER JOIN UOM_RANGE UR ON G.UOM_RANGE_ID = UR.UOM_RANGE_ID
    INNER JOIN GL_ACCOUNT_TYPE GT ON G.GL_ACCOUNT_TYPE_ID = GT.GL_ACCOUNT_TYPE_ID
    LEFT OUTER JOIN PERIOD_TYPE PT ON G.PERIOD_TYPE_ID = PT.PERIOD_TYPE_ID
    LEFT OUTER JOIN MEASURE_AND_PRODUCT_VIEW MP ON G.GL_ACCOUNT_ID = MP.WM_GL_ACCOUNT_ID
    LEFT OUTER JOIN GL_ACCOUNT_ORGANIZATION GO ON GO.GL_ACCOUNT_ID = G.GL_ACCOUNT_ID
    WHERE G.INPUT_ENUM_ID = 'ACCINP_OBJ'

UNION ALL
SELECT
    WM.GL_ACCOUNT_ID AS IND_GL_ACCOUNT_ID,
    PR.PRODUCT_ID AS IND_PRODUCT_ID,
    PR.PRODUCT_MAIN_CODE AS IND_PRODUCT_MAIN_CODE,
    PR.INTERNAL_NAME AS IND_INTERNAL_NAME,
    WM.UOM_DESCR AS IND_UOM_DESCR,
    WM.DETAIL_ENUM_ID AS IND_DETAIL_ENUM_ID,
    WTIC.WORK_EFFORT_TYPE_ID AS WORK_EFFORT_TYPE_ID_IND,
    WTIC.CONTENT_ID AS CONTENT_ID_IND,
    WTIC.WORK_EFFORT_PURPOSE_TYPE_ID AS WE_PURPOSE_TYPE_ID_IND,
    WPT.PURPOSE_TYPE_ENUM_ID AS PURPOSE_TYPE_ENUM_ID,
    U.DESCRIPTION AS UOM_DESCR,
    U.DESCRIPTION_LANG AS UOM_DESCR_LANG,
    U.UOM_TYPE_ID AS UOM_TYPE_ID,
    U.ABBREVIATION AS ABBREVIATION,
    U.ABBREVIATION_LANG AS ABBREVIATION_LANG,
    GT.DESCRIPTION AS GL_ACCOUNT_TYPE_DESCR,
    GT.DESCRIPTION_LANG AS GL_ACCOUNT_TYPE_DESCR_LANG,
    G.UOM_RANGE_ID AS UOM_RANGE_ID,
    UR.DESCRIPTION AS UOM_RANGE_DESCRIPTION,
    G.GL_ACCOUNT_ID AS GL_ACCOUNT_ID,
    G.ACCOUNT_CODE AS ACCOUNT_CODE,
    G.ACCOUNT_NAME AS ACCOUNT_NAME,
    G.ACCOUNT_NAME_LANG AS ACCOUNT_NAME_LANG,
    G.DESCRIPTION AS DESCRIPTION,
    G.DESCRIPTION_LANG AS DESCRIPTION_LANG,
    G.PRODUCT_ID AS PRODUCI_ID,
    G.CURRENT_STATUS_ID AS CURRENT_STATUS_ID,
    G.INPUT_ENUM_ID AS INPUT_ENUM_ID,
    G.DETAIL_ENUM_ID AS DETAIL_ENUM_ID,
    G.WE_MEASURE_TYPE_ENUM_ID AS WE_MEASURE_TYPE_ENUM_ID,
    G.WE_SCORE_RANGE_ENUM_ID AS WE_SCORE_RANGE_ENUM_ID,
    G.WE_SCORE_CONV_ENUM_ID AS WE_SCORE_CONV_ENUM_ID,
    G.WE_ALERT_RULE_ENUM_ID AS WE_ALERT_RULE_ENUM_ID,
    G.WE_WITHOUT_PERF,
    G.PERIOD_TYPE_ID AS PERIOD_TYPE_ID,
    PT.DESCRIPTION AS PERIOD_TYPE_DESCRIPTION,
    G.RESP_CENTER_ID AS RESP_CENTER_ID,
    PG.GROUP_NAME AS RESP_CENTER_NAME,
    PG.GROUP_NAME_LANG AS RESP_CENTER_NAME_LANG,
    WM.ORG_UNIT_ID AS MP_ORG_UNIT_ID,
    WM.ORG_UNIT_ROLE_TYPE_ID AS MP_ORG_UNIT_ROLE_TYPE_ID,
    P.GROUP_NAME AS MP_ORG_UNIT_NAME,
    P.GROUP_NAME_LANG AS MP_ORG_UNIT_NAME_LANG,
    WM.UOM_DESCR AS MP_UOM_DESCR,
    WM.UOM_DESCR_LANG AS MP_UOM_DESCR_LANG,
    GO.ORGANIZATION_PARTY_ID
FROM
    WORK_EFFORT_PURPOSE_MEASURE WPM
    INNER JOIN WORK_EFFORT_MEASURE WM ON WPM.WORK_EFFORT_MEASURE_ID = WM.WORK_EFFORT_MEASURE_ID
    INNER JOIN PRODUCT PR ON WM.PRODUCT_ID = PR.PRODUCT_ID
    LEFT OUTER JOIN PARTY_GROUP P ON WM.ORG_UNIT_ID = P.PARTY_ID
    INNER JOIN GL_ACCOUNT G ON WM.GL_ACCOUNT_ID = G.GL_ACCOUNT_ID
        AND G.INPUT_ENUM_ID = 'ACCINP_PRD'
    LEFT OUTER JOIN PARTY_GROUP PG ON G.RESP_CENTER_ID = PG.PARTY_ID
    INNER JOIN WORK_EFFORT_PURPOSE_TYPE WPT ON WPM.WORK_EFFORT_PURPOSE_TYPE_ID = WPT.WORK_EFFORT_PURPOSE_TYPE_ID
    LEFT OUTER JOIN WORK_EFFORT_TYPE WTR ON WPM.WORK_EFFORT_PURPOSE_TYPE_ID = WTR.WE_PURPOSE_TYPE_ID_RES
    LEFT OUTER JOIN WORK_EFFORT_TYPE_CONTENT WTIC ON WPM.WORK_EFFORT_PURPOSE_TYPE_ID = WTIC.WORK_EFFORT_PURPOSE_TYPE_ID
    LEFT OUTER JOIN WORK_EFFORT_TYPE WTI ON WTIC.WORK_EFFORT_TYPE_ID = WTI.WORK_EFFORT_TYPE_ID
    LEFT OUTER JOIN UOM U ON G.DEFAULT_UOM_ID = U.UOM_ID
    LEFT OUTER JOIN UOM_RANGE UR ON G.UOM_RANGE_ID = UR.UOM_RANGE_ID
    INNER JOIN GL_ACCOUNT_TYPE GT ON G.GL_ACCOUNT_TYPE_ID = GT.GL_ACCOUNT_TYPE_ID
        LEFT OUTER JOIN
    PERIOD_TYPE PT ON G.PERIOD_TYPE_ID = PT.PERIOD_TYPE_ID
    LEFT OUTER JOIN GL_ACCOUNT_ORGANIZATION GO ON GO.GL_ACCOUNT_ID = G.GL_ACCOUNT_ID

 