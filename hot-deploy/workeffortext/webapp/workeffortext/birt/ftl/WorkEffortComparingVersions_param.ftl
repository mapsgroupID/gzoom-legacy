
		<tr>
			<td colspan="1" style="width: 15%;">
				<hr>
			</td>
			<td >
				<hr>
			</td>	
		</tr>
		
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_workEffortId.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_workEffortRevisionId.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_workEffortIdChild.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_monitoringDate.ftl" />		
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_orgUnitId.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_workEffortTypeIdRef.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_workEffortTypeId_Obb.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_roleTypeId.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_partyId.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_fromDate.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_thruDate.ftl" />
				
		<tr>
			<td colspan="1">
				<br><hr><br>
			</td>	
		</tr>
		<tr>
			<td colspan="2">
				<b><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${uiLabelMap.ParametriOpzionale} </i></b> <br><br>
			</td>
		</tr>
		
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeFinalcialResource.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeHumanResource.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeIndicator.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludePerformanceKPI.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeGantt.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeCollegati.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeRequest.ftl" />		
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeExtensionDescriotion.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeReference.ftl" />		
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_selectNote.ftl" />	
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_typeNotes.ftl" />	
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_exposeDetailIndicator.ftl" />	
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_exposeReleaseDate.ftl" />	
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_exposePaginator.ftl" />	
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeValidity.ftl" />	
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeIntroduction.ftl" />
		<#include  "/workeffortext/webapp/workeffortext/birt/ftl/param/managementPrintBirtForm_excludeScore.ftl" />	
		
