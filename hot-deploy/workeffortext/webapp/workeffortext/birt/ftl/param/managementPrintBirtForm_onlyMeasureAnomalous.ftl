<tr>
    <td class="label">${uiLabelMap.OnlyMeasureAnomalous}</td>
    <td class="widget-area-style">
    <select name="onlyMeasureAnomalous" id="${printBirtFormId?default("ManagementPrintBirtForm")}_onlyMeasureAnomalous" size="1" class="">
    	<option value="Y">${uiLabelMap.CommonYes}</option>
    	<option selected="selected" value="N">${uiLabelMap.CommonNo} </option>
    </select>
    </td>
</tr>