<tr>
    <td class="label">${uiLabelMap.ExcludeProcesses}</td>
    <td class="widget-area-style"><select name="excludeProcesses" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeProcesses" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>