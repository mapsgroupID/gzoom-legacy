<tr>
    <td class="label">${uiLabelMap.PrintAllProcess}</td>
    <td class="widget-area-style"><select name="printAllProcess" id="${printBirtFormId?default("ManagementPrintBirtForm")}_printAllProcess" size="1" class=""><option selected="selected" value="Y">${uiLabelMap.CommonYes}</option><option value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>