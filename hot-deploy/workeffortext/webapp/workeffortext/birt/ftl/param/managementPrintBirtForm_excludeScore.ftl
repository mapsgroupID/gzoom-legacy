<tr>
    <td class="label">${uiLabelMap.ExcludeScore}</td>
    <td class="widget-area-style"><select name="excludeScore" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeScore" size="1" class=""><option selected="selected" value="Y">${uiLabelMap.CommonYes}</option><option value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>