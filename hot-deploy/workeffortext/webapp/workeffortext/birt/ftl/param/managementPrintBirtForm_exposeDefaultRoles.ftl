<tr>
    <td class="label">${uiLabelMap.ExposeDefaultRoles}</td>
    <td class="widget-area-style">
    <select name="exposeDefaultRoles" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeDefaultRoles" size="1" class="">
    	<option value="N">${uiLabelMap.CommonNo}</option>
    	<option selected="selected" value="Y">${uiLabelMap.CommonYes} </option>
    </select>
    </td>
</tr>