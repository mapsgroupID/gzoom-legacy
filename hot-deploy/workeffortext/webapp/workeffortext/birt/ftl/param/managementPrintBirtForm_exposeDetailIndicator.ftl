<tr>
    <td class="label">${uiLabelMap.ExposeDetailIndicator}</td>
    <td class="widget-area-style"><select name="exposeDetailIndicator" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeDetailIndicator" size="1" class=""><option selected="selected" value="Y">${uiLabelMap.CommonYes}</option><option value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>