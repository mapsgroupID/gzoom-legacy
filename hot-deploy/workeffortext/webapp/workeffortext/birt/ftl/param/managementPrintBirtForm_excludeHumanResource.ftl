<tr>
    <td class="label">${uiLabelMap.ExcludeHumanResource}</td>
    <td class="widget-area-style"><select name="excludeHumanResource" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeHumanResource" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>