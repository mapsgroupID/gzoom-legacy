<tr>
    <td class="label">${uiLabelMap.ExcludeRequest}</td>
    <td class="widget-area-style"><select name="excludeRequest" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeRequest" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>