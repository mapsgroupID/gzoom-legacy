<tr>
   <td class="label">${uiLabelMap.PoliticalResponsibility}</td>
   <td class="widget-area-style"><div  class="droplist_field" id="${printBirtFormId?default("ManagementPrintBirtForm")}_partyId">
   <input  class="autocompleter_option" type="hidden" name="target" value="<@ofbizUrl>ajaxAutocompleteOptions</@ofbizUrl>"/>
   <input  class="lookup_parameter" type="hidden" name="saveView" value="N"/>
   <input  class="autocompleter_parameter" type="hidden" name="distincts" value="[N]"/>
   <input  class="autocompleter_parameter" type="hidden" name="entityName" value="[PartyRoleView]"/>
   <input  class="autocompleter_parameter" type="hidden" name="selectFields" value="[[partyId, roleTypeId, partyName]]"/>
   <input  class="autocompleter_parameter" type="hidden" name="sortByFields" value="[[partyName]]"/>
   <input  class="autocompleter_parameter" type="hidden" name="displayFields" value="[[partyName]]"/>
   <input  class="autocompleter_parameter" type="hidden" name="constraintFields" value="[[[roleTypeId| between| WEM_P1,WEM_P99]]]"/>   
   <input  class="autocompleter_parameter" type="hidden" name="entityKeyField" value="partyId"/>
   <div class="droplist_container">
   <input type="text" size="100" maxlength="255" value="" class="droplist_edit_field"  name="partyId_edit_value" id="${printBirtFormId?default("ManagementPrintBirtForm")}_partyId_edit_value"  />   
   <input type="hidden" class="droplist_code_field" name="partyId"/>
   <span class="droplist-anchor"><a style="cursor: pointer;" class="droplist_submit_field fa fa-2x" href="#"></a></span></div></div></td>
</tr>