<tr>
    <td class="label">${uiLabelMap.ExposeReleaseDate}</td>
    <td class="widget-area-style"><select name="exposeReleaseDate" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeReleaseDate" size="1" class=""><option selected="selected" value="Y">${uiLabelMap.CommonYes}</option><option value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>