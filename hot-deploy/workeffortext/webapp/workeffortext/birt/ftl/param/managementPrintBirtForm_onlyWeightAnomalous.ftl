<tr>
    <td class="label">${uiLabelMap.OnlyWeightAnomalous}</td>
    <td class="widget-area-style">
    <select name="onlyWeightAnomalous" id="${printBirtFormId?default("ManagementPrintBirtForm")}_onlyWeightAnomalous" size="1" class="">
    	<option elected="selected" value="Y">${uiLabelMap.CommonYes}</option>
    	<option value="N">${uiLabelMap.CommonNo}</option>
    </select>
    </td>
</tr>