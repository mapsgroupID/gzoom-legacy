<tr>
    <td class="label">${uiLabelMap.SelectNote}</td>
    <td class="widget-area-style"><select name="selectNote" id="${printBirtFormId?default("ManagementPrintBirtForm")}_selectNote" size="1" class=""><option value="ALL">${uiLabelMap.CommonAll}</option><option  value="NONE">${uiLabelMap.CommonNone}</option><option selected="selected" value="DATE">${uiLabelMap.CommonTheDate}</option></select></td>
</tr>