<tr>
    <td class="label">${uiLabelMap.ExposeCommentIndicator}</td>
    <td class="widget-area-style">
    	<select name="exposeCommentIndicator" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeCommentIndicator" size="1" class="">
    		<option value="Y">${uiLabelMap.CommonYes}</option>
    		<option selected="selected" value="N">${uiLabelMap.CommonNo}</option>
    	</select>
    </td>
</tr>