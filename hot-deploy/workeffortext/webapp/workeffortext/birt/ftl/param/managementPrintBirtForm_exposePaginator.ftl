<tr>
    <td class="label">${uiLabelMap.ExposePaginator}</td>
    <td class="widget-area-style"><select name="exposePaginator" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposePaginator" size="1" class=""><option selected="selected" value="Y">${uiLabelMap.CommonYes}</option><option value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>