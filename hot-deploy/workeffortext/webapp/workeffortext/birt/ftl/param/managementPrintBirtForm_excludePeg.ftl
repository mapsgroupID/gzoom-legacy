<tr>
    <td class="label">${uiLabelMap.ExcludePeg}</td>
    <td class="widget-area-style"><select name="excludePeg" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludePeg" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>