<tr>
    <td class="label">${uiLabelMap.ExcludeFinalcialResource}</td>
    <td class="widget-area-style"><select name="excludeFinalcialResource" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeFinalcialResource" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>