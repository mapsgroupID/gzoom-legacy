<tr>
    <td class="label">${uiLabelMap.ExcludeIndicator}</td>
    <td class="widget-area-style"><select name="excludeIndicator" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeIndicator" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>