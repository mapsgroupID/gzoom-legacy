<tr>
    <td class="label">${uiLabelMap.FilterRisk}</td>
    <td class="widget-area-style">
    	<select name="filterRisk" id="${printBirtFormId?default("ManagementPrintBirtForm")}_filterRisk" size="1" class="">
    		<option selected="selected" value="NONE">${uiLabelMap.CommonNone}</option>
    		<option value="Y">${uiLabelMap.OnlyAtRisk}</option>
    		<option value="N">${uiLabelMap.OnlyNotAtRisk}</option>
    	</select>
    </td>
</tr>