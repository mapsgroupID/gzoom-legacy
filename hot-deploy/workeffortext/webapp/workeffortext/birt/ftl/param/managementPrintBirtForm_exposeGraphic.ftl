<tr>
    <td class="label">${uiLabelMap.ExposeGraphic}</td>
    <td class="widget-area-style">
    	<select name="exposeGraphic" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeGraphic" size="1" class="">
    		<option selected="selected" value="Y">${uiLabelMap.CommonYes}</option>
    		<option value="N">${uiLabelMap.CommonNo}</option>
    	</select>
    </td>
</tr>