<tr>
    <td class="label">${uiLabelMap.ExcludeValidity}</td>
    <td class="widget-area-style"><select name="excludeValidity" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeValidity" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>