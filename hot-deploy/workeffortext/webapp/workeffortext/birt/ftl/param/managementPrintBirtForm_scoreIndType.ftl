<#-- livello punteggio -->
<tr>
   <td class="label">${uiLabelMap.FormFieldTitle_elementoValutazione}</td>
   <td class="widget-area-style"><div  class="droplist_field" id="${printBirtFormId?default("ManagementPrintBirtForm")}_scoreIndType">
   <input  class="autocompleter_option" type="hidden" name="target" value="<@ofbizUrl>ajaxAutocompleteOptions</@ofbizUrl>"/>
   <input  class="autocompleter_parameter" type="hidden" name="entityName" value="[ScoreIndividuale]"/>
   <input  class="autocompleter_parameter" type="hidden" name="selectFields" value="[[etch]]"/>
   <input  class="autocompleter_parameter" type="hidden" name="sortByFields" value="[[etch]]"/>
   <input  class="autocompleter_parameter" type="hidden" name="displayFields" value="[[etch]]"/>
   <input  class="autocompleter_parameter" type="hidden" name="sortByFields" value="[[etch]]"/>
   <input  class="autocompleter_parameter" type="hidden" name="saveView" value="N"/>
   <input  class="autocompleter_parameter" type="hidden" name="entityKeyField" value="etch"/>
   <input  class="autocompleter_parameter" type="hidden" name="entityDescriptionField" value="etch"/><div class="droplist_container">
   <input type="text" size="100" maxlength="255" autocomplete="off" value="" class="droplist_edit_field" name="etch_edit_value" id="${printBirtFormId?default("ManagementPrintBirtForm")}_etch_edit_value"/>
   <input type="hidden" class="droplist_code_field" name="scoreIndType"/>
   <span class="droplist-anchor"><a style="cursor: pointer;" class="droplist_submit_field fa fa-2x" href="#"></a></span></div></div></td>
</tr>