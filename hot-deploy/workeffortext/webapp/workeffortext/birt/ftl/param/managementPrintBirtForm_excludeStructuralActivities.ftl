<tr>
    <td class="label">${uiLabelMap.ExcludeStructuralActivities}</td>
    <td class="widget-area-style"><select name="excludeStructuralActivities" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeStructuralActivities" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>