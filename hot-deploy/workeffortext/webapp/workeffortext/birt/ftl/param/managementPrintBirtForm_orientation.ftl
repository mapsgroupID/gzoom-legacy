<tr>
    <td class="label">${uiLabelMap.Orientation}</td>
    <td class="widget-area-style">
    <select name="orientation" id="${printBirtFormId?default("ManagementPrintBirtForm")}_orientation" size="1" class="">
    	<option value="LANDSCAPE">${uiLabelMap.HorizontalFrmat}</option>
    	<option selected="selected" value="PORTRAIT">${uiLabelMap.VerticalFormat}</option>
    </select></td>
</tr>