<tr>
    <td class="label">${uiLabelMap.ParametriOrdinamento}</td>
    <td class="widget-area-style">
    	<select name="ordinamento" id="${printBirtFormId?default("ManagementPrintBirtForm")}_ordinamento" size="1" class="">
    		<option selected="selected" value="N">${uiLabelMap.CognomeNomeValutato}</option>
    		<option  value="RESPONSABILE">${uiLabelMap.UnitaResponsabile}</option>
    		<option  value="RESP_CATEG">${uiLabelMap.UnitaResponsabileCategoria}</option>
    		<option  value="CATEGORIA">${uiLabelMap.Categoria}</option>
    		<option  value="CATEG_RESP">${uiLabelMap.CategoriaUnitaResponsabile}</option>
    		<option  value="REFERENTE">${uiLabelMap.HeaderReferentName}</option>
    		<option  value="PUNTEGGIO">${uiLabelMap.WorkEffortAchieveViewScore}</option>
    	</select>
    </td>
</tr>