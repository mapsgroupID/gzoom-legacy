<tr>
    <td class="label">${uiLabelMap.ExcludeReference}</td>
    <td class="widget-area-style"><select name="excludeReference" id="${printBirtFormId?default("ManagementPrintBirtForm")}_excludeReference" size="1" class=""><option value="Y">${uiLabelMap.CommonYes}</option><option selected="selected" value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>