<tr>
    <td class="label">${uiLabelMap.ExposeOtherData}</td>
    <td class="widget-area-style"><select name="exposeOtherData" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeOtherData" size="1" class=""><option selected="selected" value="Y">${uiLabelMap.CommonYes}</option><option value="N">${uiLabelMap.CommonNo}</option></select></td>
</tr>