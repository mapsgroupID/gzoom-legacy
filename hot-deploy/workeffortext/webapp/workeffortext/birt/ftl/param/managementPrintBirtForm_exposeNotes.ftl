<tr>
    <td class="label">${uiLabelMap.ExposeNotes}</td>
    <td class="widget-area-style">
    	<select name="exposeNotes" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeNotes" size="1" class="">
    		<option selected="selected" value="Y">${uiLabelMap.CommonYes}</option>
    		<option value="N">${uiLabelMap.CommonNo}</option>
    	</select>
    </td>
</tr>