<tr>
    <td class="label">${uiLabelMap.ExposeCommentsIndicator}</td>
    <td class="widget-area-style">
    	<select name="exposeCommentsIndicator" id="${printBirtFormId?default("ManagementPrintBirtForm")}_exposeCommentsIndicator" size="1" class="">
    		<option selected="selected" value="Y">${uiLabelMap.CommonYes}</option>
    		<option value="N">${uiLabelMap.CommonNo}</option>
    	</select>
    </td>
</tr>