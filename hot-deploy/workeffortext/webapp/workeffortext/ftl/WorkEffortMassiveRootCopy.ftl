<div id="WorkEffortMassiveRootCopySubmit" style="position: relative;">
	${screens.render(submitFormScreenLocation, submitFormScreenName, context)}
	<div id="party-list-div">
	    <input type="hidden" id="party-list" name="party-list"/>
	    <span class="label">${uiLabelMap.MassiveRootCopyParties}</span>
	    <div id="party-list-cnt" style="padding-left: 0.7em">	
	    </div>
	</div>
</div>
