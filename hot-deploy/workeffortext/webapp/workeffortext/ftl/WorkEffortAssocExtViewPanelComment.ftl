<div id="child-management-container-WorkEffortAssocExtViewPanelComment_${parameters.contentId?if_exists}">
    <div>
        <div class="management child-management" id="child-management-screenlet-container-WorkEffortAssocExtViewPanelComment_${parameters.contentId?if_exists}">
            <div id="child-management-screenlet-body-WorkEffortAssocExtViewPanelComment_${parameters.contentId?if_exists}" class="screenlet">
                <div class="screenlet-title-bar">
                   <ul>
                        <li class="h3">${uiLabelMap.BaseDetail}</li>
                        <li title="${uiLabelMap.CommonSave}" class="save search-save portlet-menu-item fa"><a href="#"></a></li>
                    </ul>
                    <br class="clear">
                </div>
                <div class="screenlet-body">
                    <div id="child-management-container-body-WorkEffortAssocEvalPortlet_${parameters.contentId?if_exists}">
					${screens.render(managementFormScreenLocation, managementFormScreenName, Static["org.ofbiz.base.util.UtilMisc"].toMap("managementFormName", managementFormName, "managementFormLocation", managementFormLocation))}
 					</div>
                </div>
            </div>
        </div>
    </div>
</div>
