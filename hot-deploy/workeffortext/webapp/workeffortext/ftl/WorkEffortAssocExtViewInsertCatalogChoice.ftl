<div style="display: none;" id="popup-catalog-choice" class="popup-catalog-choice">
    <table style="width: 100%">
        <tr>
            <td><a href="#" id="catalog-choice" class="smallSubmit catalog-choice" onclick="WorkEffortAssocExtViewList.insertFromCatalog(); Modalbox.hide();">${uiLabelMap.WeAssocInsertCatalogChoice}</a></td>
            <td align="right"><a href="#" id="insert-choice" class="smallSubmit insert-choice" onclick="WorkEffortAssocExtViewList.insertDirect(); Modalbox.hide();">${uiLabelMap.WeAssocInsertCatalogDirectChoice}</a></td>
        </tr>
    </table>
</div>