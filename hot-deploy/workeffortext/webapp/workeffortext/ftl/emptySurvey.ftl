<center>
  <div class="screenlet login-screenlet">
    <div class="screenlet-title-bar">
    
    </div>
    <div class="screenlet-body">
    <div class="fieldgroup" style="margin: 9px 0 10px 0; font-size: 120%;">
    <div class="fieldgroup-body">
        ${parameters._ERROR_MESSAGE_?if_exists}
        ${parameters._EVENT_MESSAGE_?if_exists}
    </div>
</div>
    </div>
  </div>
</center>