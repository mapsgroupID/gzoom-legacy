<form name="WorkEffortManagementForm" class="basic-form" id="WE_WEM001_WorkEffort" action="" method="post">
<input type="hidden" value="workEffortId" name="entityPkFields" />
<input type="hidden" value="WorkEffortView" name="operationalEntityName" />
<table cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td>
 				<table>
  					<tbody>
  						<tr>
 							<td>
  								<table>
  									<tbody>
  										<tr>
  											<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_workEffortId}</td>
   											<td>
   												<input type="text" size="25" value="${workEffortId}" name="workEffortId" readonly="readonly"/>
											</td>
  										</tr>
  										<tr>
   											<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_workEffortName}</td>
   											<td>
  												<input type="text" size="50" value="${workEffortName?if_exists}" name="workEffortName" readonly="readonly"/>
											</td>
  										</tr>
  										<tr>
   											<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_assocWeight}</td>
   											<td>
   												<input type="text" size="25" value="${assocWeight?if_exists}" name="assocWeight" readonly="readonly"/>
											</td>
  										</tr>
  										<tr>
  											<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_weIntOrgDescr}</td>
											<td>
												<input type="text" size="50" value="${weIntOrgDescr?if_exists}" name="weIntOrgDescr" readonly="readonly"/>
											</td>
  										</tr>
  									</tbody>
  								</table>
  							</td>
  							<td>
  								<table>
  									<tbody>
  										<tr>
  											<td>
  												<textarea readonly="readonly" cols="60" rows="3" name="description" style="margin-left: 0.7em">${description?if_exists}</textarea>
  											</td>
  										</tr>
  										<tr>
  											<td>
  												<table>
  													<tbody>
  														<tr>
  															<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_responsible}</td>
   															<td>
   																<input type="text" size="48" value="${responsible?if_exists}" name="responsible" readonly="readonly"/>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
  										</tr>
  									</tbody>
  								</table>
  							</td>
  						</tr>
  					</tbody>
  				</table>
			</td>
		</tr>
 	</tbody>
 </table>
</form>