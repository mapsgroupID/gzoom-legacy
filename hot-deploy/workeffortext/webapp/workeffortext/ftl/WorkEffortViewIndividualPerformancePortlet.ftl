<form name="WorkEffortManagementForm" class="basic-form" id="WE_WEM001_WorkEffort" action="" method="post">
<input type="hidden" value="workEffortId" name="entityPkFields" />
<input type="hidden" value="WorkEffortView" name="operationalEntityName" />
<table cellspacing="0" cellpadding="0" class="single-editable">
	<tbody>
		<tr>
			<td>
 				<table>
 					<tbody>
  						<tr>
  							<td>
  								<table>
  									<tbody>
										<tr>
											<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_workEffortId}</td>
											<td>
												<input type="text" size="25" value="${workEffortId}" name="workEffortId" readonly="readonly"/>
											</td>
	  									</tr>
	  									<tr>
	   										<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_workEffortName}</td>
	   										<td>
	   											<input type="text" size="50" value="${workEffortName?if_exists}" name="workEffortName" readonly="readonly"/>
											</td>
	  									</tr>
	  									<tr>
	   										<td class="label">${uiLabelMap.WorkEffortViewIdentificationData_assocWeight}</td>
	   										<td>
	   											<input type="text" size="25" value="${assocWeight?if_exists}" name="assocWeight" readonly="readonly"/>
											</td>
	  									</tr>
	  								</tbody>
	  							</table>
	  						</td>
	  						<td>
	  							<textarea readonly="readonly" rows="3" cols="60" name="description">${description?if_exists}</textarea>
	  						</td>
	  					</tr>
	  				</tbody>
	  			</table>
  			</td>
  		</tr>
	</tbody>
</table>
</form>