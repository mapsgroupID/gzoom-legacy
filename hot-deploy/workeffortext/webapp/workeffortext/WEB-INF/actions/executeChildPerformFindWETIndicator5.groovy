import org.ofbiz.base.util.*;


context.contentIdInd = 'WEFLD_IND5';
parameters.contentIdInd = 'WEFLD_IND5';
context.contentIdSecondary = 'WEFLD_AIND5';
parameters.contentIdSecondary = 'WEFLD_AIND5';

Debug.log("executeChildPerformFindWETIndicator5.groovy");

GroovyUtil.runScriptAtLocation("component:/workeffortext/webapp/workeffortext/WEB-INF/actions/executeChildPerformFindWETIndicator.groovy", context);
