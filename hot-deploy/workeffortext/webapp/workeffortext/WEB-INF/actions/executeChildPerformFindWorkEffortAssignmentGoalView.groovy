import org.ofbiz.base.util.*;


context.showDates = "N";
context.showRoleType = "Y";
context.showRoleTypeWeight = "Y";
context.showComment = "N";
context.detailEnabled = "Y";
context.showSequence = "N";

GroovyUtil.runScriptAtLocation("component://workeffortext/webapp/workeffortext/WEB-INF/actions/executeChildPerformFindWorkEffortAssignmentView.groovy", context);
