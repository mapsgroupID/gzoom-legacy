package com.mapsengineering.workeffortext.util;

public enum E {
	WorkEffortTypeContent, weTypeContentTypeId, contentId, workEffortTypeId, workEffortTypeIdRoot, params,
	//
	WorkEffort, WorkEffortType, CustomTimePeriodAndParentView, enableMultiYearFlag, parentPeriodFilter, searchDate, periodTypeId,
	//
	WorkEffortAndTypePeriodAndThruDate, workEffortId, workEffortIdRoot, parentPeriodId, parentFromDate, parentThruDate, fromDate, thruDate, glFiscalTypeEnumId,
	//
	fromTreeViewSearch, locale, timeZone, Y, GLFISCTYPE_ACTUAL,
	//
	QueryConfig, queryActive, queryType, R, queryCode, cond0Info, histJobLogId, queryExecutorService, queryId,
	//
	WorkEffortTypeStatus, workEffortTypeRootId, currentStatusId, statusId, WorkEffortTypeStatusCnt, MESSAGE, MESSAGE_LANG, statusCheck
}
