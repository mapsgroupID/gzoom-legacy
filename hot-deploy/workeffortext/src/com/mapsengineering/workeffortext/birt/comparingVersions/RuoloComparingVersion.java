package com.mapsengineering.workeffortext.birt.comparingVersions;

import java.sql.SQLException;

public class RuoloComparingVersion extends AssegnazioneComparingVersion {

    private static final long serialVersionUID = 1L;

    public String getDescription() throws SQLException {
        return getRs().getString(E.DESCRIPTION.name());
    }
}
