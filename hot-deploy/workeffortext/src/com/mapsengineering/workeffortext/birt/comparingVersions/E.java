package com.mapsengineering.workeffortext.birt.comparingVersions;

public enum E {
    Y, N, NONE,
    
    //
    ETCH, WORK_EFFORT_NAME, DESCRIPTION, PARTY_NAME, ESTIMATED_START_DATE, ESTIMATED_COMPLETION_DATE, 
    
    WORK_EFFORT_ID, WORK_EFFORT_TYPE_ID, SEQUENCE_NUM, SOURCE_REFERENCE_ID, COMMENTS, ROLE_TYPE_WEIGHT,
    
    FROM_DATE, THRU_DATE, OLD_COMMENTS, OLD_ROLE_TYPE_WEIGHT, OLD_FROM_DATE, OLD_THRU_DATE, OLD_WORK_EFFORT_ID,
    
    OLD_WORK_EFFORT_SNAPSHOT_ID, OLD_ETCH, OLD_WORK_EFFORT_NAME, OLD_DESCRIPTION, OLD_PARTY_NAME,
    
    OLD_ESTIMATED_START_DATE, OLD_ESTIMATED_COMPLETION_DATE, OLD_WORK_EFFORT_TYPE_ID, ASSOC_WEIGHT,
    
    OLD_ASSOC_WEIGHT, OLD_SEQUENCE_NUM, NOTE_DATE_TIME, NOTE_NAME, NOTE_INFO, INTERNAL_NOTE, IS_HTML,
    
    OLD_NOTE_INFO, OLD_INTERNAL_NOTE, OLD_IS_HTML, C_DESCRIPTION, WEC_DESCRITPION, DATA_RESOURCE_NAME,
    
    OLD_WEC_DESCRITPION, OLD_C_DESCRIPTION, ACCOUNT_NAME, PR_UOM_DESCR, WM_UOM_DESCR, KPI_SCORE_WEIGHT,
        
    SEQUENCE_ID, OLD_UOM_DESCR, OLD_KPI_SCORE_WEIGHT, OLD_SEQUENCE_ID, G_ACCOUNT_NAME, M_PRODUCT_DESCRIPTION,
    
    UOM_DESCR, M_PARTY_NAME, M_PERIOD_NAME, TT_FISCAL_TYPE_DESCRIPTION, R_UOM_ETCH, G_UOM_DECIMAL_SCALE, 
    
    G_UOM_TYPE_ID, TM_AMOUNT, G_UOM_CODE, TM_DESCRIPTION, TT_DESCRIPTION, OLD_TT_DESCRIPTION, OLD_TM_DESCRIPTION,
    
    OLD_G_UOM_CODE, OLD_TM_AMOUNT, M_UOM_DESCR, R_UOM_CODE, R_UOM_DESCR, OLD_R_UOM_CODE,
    
    
    //
    excludeHumanResource, excludeReference, excludeCollegati, selectNote, excludeRequest, excludeIndicator, exposeDetailIndicator,
    
    workEffortId, oldWorkEffortId, workEffortTypeId, param1, param2, param3,
    
    //
    RATING_SCALE, DATE_MEASURE
    
    
    
 }
