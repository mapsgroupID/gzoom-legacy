package com.mapsengineering.workeffortext.pub.services;

import com.mapsengineering.workeffortext.pub.services.impl.mockup.PublicInterfaceMockup;

public class PublicInterfaceFactory {

    public static PublicInterface create() {
        return new PublicInterfaceMockup();
    }
}
