package com.mapsengineering.workeffortext.pub.services;

public class PublicInterfaceException extends Exception {

    private static final long serialVersionUID = 1L;

    public PublicInterfaceException() {
        super();
    }

    public PublicInterfaceException(String message) {
        super(message);
    }

    public PublicInterfaceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PublicInterfaceException(Throwable cause) {
        super(cause);
    }
}
