package com.mapsengineering.workeffortext.pub.data;

public class PubGlAccountHistoryData {

    private String imageUrl;

    public PubGlAccountHistoryData() {
        this(null);
    }

    public PubGlAccountHistoryData(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
