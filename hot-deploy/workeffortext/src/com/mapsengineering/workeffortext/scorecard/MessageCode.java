package com.mapsengineering.workeffortext.scorecard;

/**
 * Codici gravita messaggi errore
 * 
 * @author sandro
 */
public enum MessageCode {
    /**
     * Bloccante
     */
    ERROR_BLOCKING,
    /**
     * Passa a prossima scheda
     */
    ERROR_NEXTCARD,
    /**
     * Informativo
     */
    ERROR_NOTE,
    /**
     * Generico
     */
    INFO_GENERIC,
    /**
     * Warning
     */
    WARNING;
}
