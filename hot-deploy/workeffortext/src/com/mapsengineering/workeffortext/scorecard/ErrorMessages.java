package com.mapsengineering.workeffortext.scorecard;

/**
 * Error Messages
 *
 */
public class ErrorMessages {
    public static final String TARGET_ZERO = "Target value equals 0 for workEffort %s, it is not possible divide by 0, next";
    public static final String TARGET_NOT_EXISTS = "Target value does not exist for workEffort %s, it is not possible divide by 0.";
    public static final String ACTUAL_NOT_EXISTS = "Actual value does not exist for workEffort %s, it is not possible divide by 0.";
    public static final String ACTUAL_ZERO = "Actual value equals 0 for workEffort %s, it is not possible divide by 0, next";
}
