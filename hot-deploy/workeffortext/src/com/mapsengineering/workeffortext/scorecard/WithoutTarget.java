package com.mapsengineering.workeffortext.scorecard;

/**
 * Flag assenza target
 * 
 */
public enum WithoutTarget {
    /** get score of previous period */
    WEWITHTARG_PRV_SCORE, 
    
    /** no calc */
    WEWITHTARG_NO_CALC;
}
