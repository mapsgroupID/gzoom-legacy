package com.mapsengineering.workeffortext.scorecard;

/**
 * Field for scoreCard service
 *
 */
public enum E {
    glAccountId, mAccountId, kpiScoreWeight, kpiOtherWeight, weScoreRangeEnumId, weScoreConvEnumId, weOtherGoalEnumId, weWithoutPerf, weTransTypeValueId, uomRangeId, workEffortIdFrom, workEffortAssocTypeId, fromDate,
    workEffortId, glFiscalTypeId, periodTypeId, transactionDate, assocWeight, weTransDate, amount, thruDate, hasScoreAlert, weTransMeasureId, periodicalAbsoluteEnumId, targetPeriodEnumId, weWithoutTarget, targetValue, actualValue,
    actualCount, sourceReferenceId, workEffortName, weMeasureUomId, uomRangeValuesId, movTransactionDate, rangeValuesFactor, rangeValuesFactorMin, description, thruValue, acctgTransId, acctgTransEntrySeqId,
    workEffortTypeId, score, workEffortParentId, lastCorrectScoreDate, workEffortMeasureId, debitCreditDefault, workEffortSnapshotId, workEffortRevisionId, weTransWorkEffortSnapShotId, 
    somethingToWrite, actualPyValue, actualPyCount, targetCount, limitExcellentValue, limitMaxValue, limitMaxCount, limitMinValue, limitExcellentCount, limitMinCount, limitMedValue, limitMedCount, accountCode, mAccountCode, C, D,
    weTransId, weTransEntryId, customTimePeriodId, actStEnumId,
    WorkEffort, UomRangeValues, WorkEffortMeasure, AcctgTransEntry, WorkEffortType, GlAccount, WorkEffortMeasureAndGlAccountView, fromValue, perfAmountTarget, perfAmountActual, perfAmountMin, perfAmountMax, perfAmountCalc,
    WorkEffortAndType, applyScoreRange, uomRangeScoreId, transValue, workEffortParentIdFrom, WorkEffortTransactionIndicatorView, weMeasureTypeEnumId,
    WorkEffortAndTypePeriodAndThruDate, organizationPartyId, weTransValue,
    SCORE_PERIOD_ALL, SCORE_PERIOD_OPEN, SCORE_PERIOD_PARENT, ORIG, ALT, WEMT_PERF, ACTSTATUS_REPLACED
}
