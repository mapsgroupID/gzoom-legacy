package com.mapsengineering.workeffortext.scorecard;

import java.util.ArrayList;
import java.util.List;

/**
 * Risultati elaborazione collegati
 */
public class AssociatedAchievesResult extends GenericResult {
    private double scoreCount = 0d;
    private List<String> childWithoutScore = new ArrayList<String>();

    /**
     * @return scoreCount
     */
    public double getScoreCount() {
        return scoreCount;
    }

    /**
     * @param scoreCount
     */
    public void setScoreCount(double scoreCount) {
        this.scoreCount = scoreCount;
    }

    /**
     * @return childWithoutScore
     */
    public List<String> getChildWithoutScore() {
        return childWithoutScore;
    }

    /**
     * @param childWithoutScore
     */
    public void setChildWithoutScore(List<String> childWithoutScore) {
        this.childWithoutScore = childWithoutScore;
    }

    /**
     * Increment
     * @param toAdd
     */
    public void incrementScoreCount(double toAdd) {
        this.scoreCount += toAdd;
    }

}