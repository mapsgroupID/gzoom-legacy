package com.mapsengineering.workeffortext.scorecard;

/**
 * Generic Result 
 *
 */
public class GenericResult {
    private double weightedAverage = 0d;
    private double weightSum = 0d;
    private double alertCount = 0d;

    /**
     * 
     * @return weightedAverage
     */
    public double getWeightedAverage() {
        return weightedAverage;
    }

    /**
     * 
     * @param weightedAverage
     */
    public void setWeightedAverage(double weightedAverage) {
        this.weightedAverage = weightedAverage;
    }

    /**
     * 
     * @return weightSum
     */
    public double getWeightSum() {
        return weightSum;
    }

    /**
     * 
     * @param weightSum
     */
    public void setWeightSum(double weightSum) {
        this.weightSum = weightSum;
    }

    /**
     * 
     * @return alertCount
     */
    public double getAlertCount() {
        return alertCount;
    }

    /**
     * 
     * @param alertCount
     */
    public void setAlertCount(double alertCount) {
        this.alertCount = alertCount;
    }

    /**
     * Increment alertCount
     * @param toAdd
     */
    public void incrementAlertCount(double toAdd) {
        this.alertCount += toAdd;
    }

    /**
     * Increment weightedAverage
     * @param toAdd
     */
    public void incrementWeightedAverage(double toAdd) {
        this.weightedAverage += toAdd;
    }

    /**
     * Increment weightSum
     * @param toAdd
     */
    public void incrementWeightSum(double toAdd) {
        this.weightSum += toAdd;
    }
}
