package com.mapsengineering.workeffortext.services.partyrole;

public enum E {
    PARENT_ROLE_CODE, PARTY_NAME, PARTY_NAME_LANG, PARTY_ID, ROLE_TYPE_ID, USER_LOGIN_ID, PARTY_ID_TO, PARTY_ID_FROM, 
    
    ROLE_TYPE_ID_TO, ROLE_TYPE_ID_FROM, FROM_DATE, THRU_DATE, PARTY_RELATIONSHIP_TYPE_ID, EXTERNAL_ID,
	
    parentRoleCode, partyName, partyNameLang, partyId, roleTypeId, userLoginId, statusId, organizationId, partyRelationshipTypeId,
    
    partyIdTo, partyIdFrom, fromDate, thruDate, roleTypeIdTo, roleTypeIdFrom, orgUnitId, onlyCurrentEvalRef, isOrgMgr, roleTypeIdList,
    
    fromSearch, rootInqyTree, gpMenuEnumId, currentStatusContains, snapshot, queryOrderBy, externalId
}
