package com.mapsengineering.workeffortext.services.trans;

/**
 * Enumeration for query
 */
public enum E {
    SCHEDA, SCHEDA_ID, OBIETTIVO, OBIETTIVO_ID, MISURA_OBJ, MISURA_UO, MISURA_PRD, TIPO_VALORE, PERIODO, INPUT_ENUM_ID, workEffortId, statusId, WorkEffortStatus, statusDatetime, reason, crudServiceDefaultOrchestration_WorkEffortRootStatus,
    OBIETTIVO_LANG, ACCOUNT_NAME, ACCOUNT_NAME_LANG, MISURA_OBJ_LANG
}
