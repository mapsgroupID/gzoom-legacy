package com.mapsengineering.workeffortext.services.trans;

/**
 * Enumeration for query
 */
public enum WTI {
    workEffortId, glFiscalTypeId, weTransDate, weTransCurrencyUomId, contentId, workEffortMeasureId, weTransTypeValueId, hasMandatoryBudgetEmpty,
    //
    showPeriods, showDetail, onlyWithBudget, accountFilter, mostraMovimenti, mostraSoggetti, mostraPossibiliPeriodi, organizationId,
    //
    CustomTimePeriod, fromDate, thruDate, periodTypeId, masterCustomTimePeriodId, customTimePeriodIdList, customTimePeriodList, customTimePeriodId, periodElapsed, scrollInt,
    //
    PROCESS, PROJECT, firstCustomTimePeriodId, firstCustomTimePeriodFromDate, lastCustomTimePeriodId, lastCustomTimePeriodThruDate, estimatedStartDate, estimatedCompletionDate, organizationPartyId, searchDate,
    //
    WORK_EFFORT_ID, WORK_EFFORT_MEASURE_ID, UOM_DESCR, UOM_DESCR_LANG, GL_ACCOUNT_ID, KPI_SCORE_WEIGHT, KPI_OTHER_WEIGHT, 
    //
    ACCOUNT_NAME, ACCOUNT_NAME_LANG, ACCOUNT_DESCRIPTION, ACCOUNT_DESCRIPTION_LANG, INPUT_ENUM_ID, DETECT_ORG_UNIT_ID_FLAG, B_CURRENCY_UOM_ID, DEFAULT_UOM_ID, DECIMAL_SCALE, UOM_TYPE_ID, ABBREVIATION, ABBREVIATION_LANG,
    //
    B_VOUCHER_REF, B_AMOUNT, BUDGET_AMOUNT, A_ACCTG_TRANS_ID, B_ACCTG_TRANS_ENTRY_SEQ_ID, 
    //
    A_TRANSACTION_DATE, A_GL_FISCAL_TYPE_ID, 
    //
    VAL_MOD_ID, WM_VAL_MOD_ID, STATUS_ENUM_ID, DATA_SOURCE_ID, WM_DATA_SOURCE_ID,
    //
    ORG_UNIT_ROLE_TYPE_ID, ORG_UNIT_ID, M_ORG_UNIT_ROLE_TYPE_ID, M_ORG_UNIT_ID, IS_POSTED,
    //
    PARTY_ID, ROLE_TYPE_ID, ENTRY_PARTY_ID, ENTRY_ROLE_TYPE_ID, PARTY_NAME, GR_PARTY_ID, GR_ROLE_TYPE_ID, UOM_CODE, UOM_CODE_LANG, CUSTOM_TIME_PERIOD_ID, B_DESCRIPTION, B_DESCRIPTION_LANG, A_DESCRIPTION, A_DESCRIPTION_LANG, PRODUCT_ID,
    //
    GT_DESCRIPTION, GT_GL_FISCAL_TYPE_ID, GT_GL_FISCAL_TYPE_ENUM_ID, C_DESCRIPTION, SEQUENCE_ID, WE_SCORE_CONV_ENUM_ID, WE_OTHER_GOAL_ENUM_ID, COMMENTS, GLT_DESCR, DEBIT_CREDIT_DEFAULT, DC_DESCR, 
    //
    FROM_DATE, THRU_DATE, P_FROM_DATE, P_THRU_DATE, C_DESCRIPTION_LANG, GT_DESCRIPTION_LANG, COMMENTS_LANG, WORK_EFFORT_TYPE_PERIOD_ID
}
