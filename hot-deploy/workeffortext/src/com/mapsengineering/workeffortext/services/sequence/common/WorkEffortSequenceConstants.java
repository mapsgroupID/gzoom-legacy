package com.mapsengineering.workeffortext.services.sequence.common;

public class WorkEffortSequenceConstants {
	
	public static final String CONTEXT_OPERATION = "operation";
	public static final String CONTEXT_LOCALE = "locale";
	public static final String CONTEXT_PARAMS = "parameters";
	public static final String OPERATION_CREATE = "CREATE";
	public static final String PARAM_FROMCARD = "fromCard";
	public static final String VALUE_S = "S";
	public static final String VALUE_N = "N";
	
	public static final String FRAME_PF = "PF";
	public static final String FRAME_AA = "AA";
	public static final String FRAME_A2 = "A2";
	public static final String FRAME_UO = "UO";
	public static final String FRAME_PR = "PR";
	public static final String FRAME_PG = "PG";
	public static final String FRAME_ET = "ET";
	public static final String FRAME_DOT_PG = ".${PG}";
	public static final String FRAME_PF_DOT = "${PF}.";
	public static final String FRAME_AA_DOT = "${AA}.";
	public static final String FRAME_PFAA_DOT = "${PF}${AA}.";
	public static final String FRAME_AAPF_DOT = "${AA}${PF}.";
	public static final String FRAME_UO_EL = "${UO}";
	public static final String AAPFUOPR_EL = "${AA}${PF}${UO}${PR}";
	public static final String AAPFUO_EL = "${AA}${PF}${UO}";
	public static final String AAA2PFUO_EL = "${AA}${A2}${PF}${UO}";
    public static final String FRAME_AAAA = "AAAA";
    
    public static final String MANUAL = "MANUAL";
    public static final String PFPG = "PFPG";
    public static final String PFAAUOCP = "PFAAUOCP";
}
