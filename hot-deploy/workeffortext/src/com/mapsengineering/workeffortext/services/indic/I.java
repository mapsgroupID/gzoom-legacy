package com.mapsengineering.workeffortext.services.indic;

/**
 * Enumeration for query
 */
public enum I {
    workEffortId, workEffortMeasureId, glAccountId, glFiscalTypeId, searchDate, weTransCurrencyUomId, contentId, showScorekpi, yearEnd, yearStart, uomDescr, uomDescrLang,
    //
    weScoreConvEnumId, weScoreRangeEnumId, weAlertRuleEnumId, weMeasureTypeEnumId, weWithoutPerf, uomRangeId, kpiScoreWeight, kpiOtherWeight, inputEnumId, detailEnumId, calcCustomMethodId, 
    //
    glAccountTypeDescription, fromDate, thruDate, uomTypeId, sequenceId, productId, isPosted, 
    //
    WORK_EFFORT_ID, WORK_EFFORT_MEASURE_ID, UOM_DESCR, UOM_DESCR_LANG, GL_ACCOUNT_ID, KPI_SCORE_WEIGHT, KPI_OTHER_WEIGHT,
    //
    ACCOUNT_NAME, ACCOUNT_DESCRIPTION, INPUT_ENUM_ID, DETAIL_ENUM_ID, CURRENCY_UOM_ID, DEFAULT_UOM_ID, DECIMAL_SCALE, UOM_TYPE_ID, ABBREVIATION, 
    //
    VAL_MOD_ID, WM_VAL_MOD_ID, STATUS_ENUM_ID, DATA_SOURCE_ID, WM_DATA_SOURCE_ID,
    //
    ORG_UNIT_ROLE_TYPE_ID, ORG_UNIT_ID, IS_POSTED,
    //
    PARTY_ID, ROLE_TYPE_ID, WE_SCORE_CONV_ENUM_ID, WE_ALERT_RULE_ENUM_ID, WE_SCORE_RANGE_ENUM_ID, WE_MEASURE_TYPE_ENUM_ID, WE_WITHOUT_PERF,
    //
    UOM_RANGE_ID, CALC_CUSTOM_METHOD_ID, GL_ACCOUNT_TYPE_DESC, FROM_DATE, THRU_DATE, SEQUENCE_ID, PRODUCT_ID
}
