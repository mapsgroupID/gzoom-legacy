package com.mapsengineering.workeffortext.services.workeffort;

public enum WE {
	WORK_EFFORT_ID, WORK_EFFORT_TYPE_ID, CURRENT_STATUS_ID, WORK_EFFORT_PARENT_ID, ORGANIZATION_ID, ORG_UNIT_ROLE_TYPE_ID, ORG_UNIT_ID, 
	SOURCE_REFERENCE_ID, WORK_EFFORT_NAME, ESTIMATED_START_DATE, ESTIMATED_COMPLETION_DATE, WE_ETCH, WORK_EFFORT_SNAPSHOT_ID, UV_USER_LOGIN_ID, 
	WE_CONTEXT_ID, WE_IS_ROOT, WE_IS_TEMPLATE, WE_TYPE_DESCRIPTION, ETCH, WE_HIERARCHY_TYPE_ID, WE_STATUS_DESCR, WE_ACTIVATION, WE_STATUS_SEQ_ID, 
	WE_STATUS_TYPE_ID, GL_FISCAL_TYPE_ID, NEXT_STATUS_ID, MANAGEMENT_ROLE_TYPE_ID, MANAG_WE_STATUS_ENUM_ID, PARTY_ID_TO, PARTY_ID, PARTY_NAME, 
	WORK_EFFORT_NAME_LANG, WE_TYPE_DESCRIPTION_LANG, WE_STATUS_DESCR_LANG, PARTY_NAME_LANG, ETCH_LANG, WE_TOTAL, WE_TYPE_ETCH, WE_TYPE_ETCH_LANG,
	WORK_EFFORT_ROOT_TYPE_ID, WORK_EFFORT_ID_CHILD, PARTY_ID_IN_CHARGE, PARTY_NAME_IN_CHARGE, ROLE_TYPE_ID_IN_CHARGE, MIN_SEQUENCE_ID, PARENT_ROLE_CODE,
	WORK_EFFORT_ANALYSIS_ID, PARENT_TYPE_ID, COMMENTS, DESCRIPTION, UOM_RANGE_SCORE_ID, WE_UOM_RANGE_SCORE_ID, WE_FROM_NAME, WE_FROM_NAME_LANG,
	ACTUAL_START_DATE, ACTUAL_COMPLETION_DATE, USER_LOGIN_ID, AMOUNT, HAS_SCORE_ALERT, TRANSACTION_DATE, PARENT_SOURCE_REFRENECE_ID, EXTERNAL_ID,
	workEffortId, workEffortTypeId, currentStatusId, workEffortParentId, organizationId, orgUnitRoleTypeId, orgUnitId, sourceReferenceId,
	workEffortName, estimatedStartDate, estimatedCompletionDate, weEtch, workEffortSnapshotId, uvUserLoginId, weContextId, weIsRoot,
	weIsTemplate, weTypeDescription, etch, weHierarchyTypeId, weStatusDescr, weActivation, weStatusSeqId, weStatusTypeId, glFiscalTypeId, nextStatusId,
	managementRoleTypeId, managWeStatusEnumId, partyIdTo, partyId, partyName, workEffortNameLang, weTypeDescriptionLang, weStatusDescrLang,
	partyNameLang, etchLang, isOrgMgr, isRole, isSup, isTop, searchDate, currentStatusContains, workEffortRevisionId, weTotal, queryOrderBy,
	workEffortRootTypeId, workEffortIdChild, partyIdInCharge, partyNameInCharge, roleTypeIdInCharge, sequenceId, weTypeEtch, weTypeEtchLang, parentRoleCode,
	isRootActive, gpMenuEnumId, workEffortAnalysisId, parentTypeId, comments, description, uomRangeScoreId, weUomRangeScoreId,
	actualStartDate, actualCompletionDate, userLoginId, amount, hasScoreAlert, transactionDate, parentSourceReferenceId, weFromName, weFromNameLang, withProcess, externalId
}
