package com.mapsengineering.workeffortext.test;

/** 
 * Test con utente user17 e BS
 */
public class TestExecuteChildPerformFindWorkEffortRootBSUserLogin17 extends TestExecuteChildPerformFindWorkEffortRootBS {

    public static final String MODULE = TestExecuteChildPerformFindWorkEffortRootBSUserLogin17.class.getName();

    protected String getUserLoginId() {
        return "user17";
    }

    protected String getTitle() {
        return "Test execute executeChildPerformFindWorkEffortRoot BS e user17";
    }
    
    protected int getRowListNumber() {
        return 0;
    }
}
