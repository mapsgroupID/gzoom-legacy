package com.mapsengineering.workeffortext.test;

/** 
 * Test con utente user17 e BS
 */
public class TestExecuteChildPerformFindWorkEffortRootORUserLogin17 extends TestExecuteChildPerformFindWorkEffortRootOR {

    public static final String MODULE = TestExecuteChildPerformFindWorkEffortRootORUserLogin17.class.getName();

    protected String getUserLoginId() {
        return "user17";
    }

    protected String getTitle() {
        return "Test execute executeChildPerformFindWorkEffortRoot OR e user17";
    }
}
