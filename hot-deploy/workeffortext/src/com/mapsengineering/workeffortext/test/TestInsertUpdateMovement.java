package com.mapsengineering.workeffortext.test;

/**
 * TestInsertUpdateMovement
 *
 */
public class TestInsertUpdateMovement extends BaseTestInsertUpdateMovement {

    public static final String MODULE = TestInsertUpdateMovement.class.getName();

    protected String getTitle() {
        return "TestInsertUpdateMovement";
    }
}
