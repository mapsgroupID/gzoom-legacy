package com.mapsengineering.workeffortext.test.rootcopy;

/**
 * TestMassiveWorkEffortRootSnapshot for R10003 closed
 *
 */
public class TestMassiveWorkEffortRootSnapshotIsClosed extends TestMassiveWorkEffortRootSnapshot {

    public static final String MODULE = TestMassiveWorkEffortRootSnapshotIsClosed.class.getName();

    protected String getTitle() {
        return "Snapshot R10003 closed";
    }

    protected String getWorkEffortRevisionId() {
        return "R10003";
    }
}
