package com.mapsengineering.workeffortext.test.scorecard;

/** 
 * Test ScoraCard for December
 * @author dain
 *
 */
public class TestScoreCardDec extends ScoreCardTestCase {

    @Override
    protected String getCustomTimePeriodId() {
        return "201212";
    }

    @Override
    protected double getExpectedResult() {
        return 590d;
    }

}
