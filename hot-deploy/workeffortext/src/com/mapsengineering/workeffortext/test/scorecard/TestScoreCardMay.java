package com.mapsengineering.workeffortext.test.scorecard;

/** 
 * Test ScoraCard for May
 * @author dain
 *
 */
public class TestScoreCardMay extends ScoreCardTestCase {

    @Override
    protected String getCustomTimePeriodId() {
        return "201205";
    }

    @Override
    protected double getExpectedResult() {
        return 305d;
    }

}
