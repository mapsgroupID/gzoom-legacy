package com.mapsengineering.workeffortext.test.scorecard;

/** 
 * Test ScoraCard for September
 * @author dain
 *
 */
public class TestScoreCardSep extends ScoreCardTestCase {

    @Override
    protected String getCustomTimePeriodId() {
        return "201209";
    }

    @Override
    protected double getExpectedResult() {
        return 556d;
    }

}
