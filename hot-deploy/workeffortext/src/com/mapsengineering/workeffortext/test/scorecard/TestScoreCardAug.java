package com.mapsengineering.workeffortext.test.scorecard;

/** 
 * Test ScoraCard for August
 * @author dain
 *
 */
public class TestScoreCardAug extends ScoreCardTestCase {

    @Override
    protected String getCustomTimePeriodId() {
        return "201208";
    }

    @Override
    protected double getExpectedResult() {
        return 515d;
    }

}
