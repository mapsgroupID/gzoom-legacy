package com.mapsengineering.workeffortext.test.scorecard;

/** 
 * Test ScoraCard for June
 * @author dain
 *
 */
public class TestScoreCardJun extends ScoreCardTestCase {

    @Override
    protected String getCustomTimePeriodId() {
        return "201206";
    }

    @Override
    protected double getExpectedResult() {
        return 385d;
    }

}
