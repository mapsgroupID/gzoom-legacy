package com.mapsengineering.workeffortext.test.scorecard;

import com.mapsengineering.base.test.BaseTestCase;
import com.mapsengineering.workeffortext.scorecard.WithoutPerformance;

/**
 * Test Without Perfomance
 *
 */
public class TestWithoutPerfomance extends BaseTestCase {

    /**
     * Test Without Perfomance
     */
    public void testWithoutPerformance() {
        assertNotNull(WithoutPerformance.getDefault());
    }
}
