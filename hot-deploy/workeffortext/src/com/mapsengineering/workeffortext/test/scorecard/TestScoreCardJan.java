package com.mapsengineering.workeffortext.test.scorecard;

import org.ofbiz.entity.GenericValue;

/** 
 * Test ScoraCard for January
 * @author dain
 *
 */
public class TestScoreCardJan extends ScoreCardTestCase {
    
    @Override
    protected String getCustomTimePeriodId() {
        return "201201";
    }

    @Override
    protected void assertResult(GenericValue value) {
        assertNull(value);
    }

    @Override
    protected double getExpectedResult() {
        return 0d;
    }
}
