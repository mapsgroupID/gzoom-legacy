package com.mapsengineering.workeffortext.test.scorecard;

/** 
 * Test ScoraCard for November
 * @author dain
 *
 */
public class TestScoreCardNov extends ScoreCardTestCase {

    @Override
    protected String getCustomTimePeriodId() {
        return "201211";
    }

    @Override
    protected double getExpectedResult() {
        return 589d;
    }

}
