package com.mapsengineering.workeffortext.test;

/**
 * Integration Test 
 * In Snapshot 'Valori Risorse' dei capitoli di spesa e 'Valori Indicatori'
 *
 */
public class TestWorkEffortRootSnapshotWithOnlyPartialTransaction extends TestWorkEffortRootSnapshotWithoutResourceValues {

    @Override
    protected String getTitle() {
        return "Snapshot PEG12, Revision with fromDate e toDate";
    }

    protected String getWorkEffortRevisionId() {
        return "R10000";
    }
}
