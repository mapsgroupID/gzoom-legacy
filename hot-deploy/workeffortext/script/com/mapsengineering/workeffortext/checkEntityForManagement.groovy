import org.ofbiz.base.util.*;

res = "success";

if ("WorkEffortAnalysis".equals(parameters.entityName)) {
	res = "workEffortAnalysis";
}

return res;
