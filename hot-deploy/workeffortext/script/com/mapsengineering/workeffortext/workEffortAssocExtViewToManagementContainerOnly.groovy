import org.ofbiz.base.util.*;

request.setAttribute("workEffortId", parameters.worEffortParentIdTo);
request.setAttribute("selectedId", "");

if ("Y".equals(parameters.ignoreFolderIndex)) {
    request.setAttribute("folderIndex", "");  
}

return "success";