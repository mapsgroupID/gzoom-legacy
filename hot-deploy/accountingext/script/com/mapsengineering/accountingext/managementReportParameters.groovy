import org.ofbiz.base.util.*;
import org.ofbiz.service.*;
import com.mapsengineering.base.util.*;

birtParameters = [:];

birtParameters.put("year", parameters.year);
birtParameters.put("partyId", parameters.partyId);
birtParameters.put("roleTypeId", parameters.roleTypeId);
request.setAttribute("birtParameters", birtParameters);

return "success";