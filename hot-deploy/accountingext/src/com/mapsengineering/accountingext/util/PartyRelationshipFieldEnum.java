package com.mapsengineering.accountingext.util;

/**
 * Field for PartyRelationshipCustomMethodCalculatorUtil
 *
 */
public enum PartyRelationshipFieldEnum {
   partyRelationTypeId, glAccountId, partyId, roleTypeId, refDate, PARTY_ID, ROLE_TYPE_ID, isFinalPeriod, isPartTime
}
