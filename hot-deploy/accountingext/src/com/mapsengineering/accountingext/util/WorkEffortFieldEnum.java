package com.mapsengineering.accountingext.util;

/**
 * Field for query
 *
 */
public enum WorkEffortFieldEnum {
    VOUCHER_REF, WORK_EFFORT_ID, PRODUCT_ID, startYearDate, endYearDate, isAggregAnno, workEffortId, glFiscalTypeId, glAccountId, refDate, aggregType, calcType, transDate, productId, orgUnitId, WorkEffort
}
