package com.mapsengineering.accountingext.test;

/**
 * Test Indicator Calc Service indicator output with inputEnumId = ACCINP_OBJ
 * Gestione del modello per obiettivi come indicatore di output
 *
 */
public class TestIndicatorCalcServicesAggregMaxAnno extends TestIndicatorCalcServicesAggreg {
    
    /**
     * Test ACCINP_OBJ same workEffort Create movement for 2012,
     * WORK_EFFORT_ASSOC in 2012
     */
    public void testAcctgTransInterfaceAggregMaxAnno() {
    	updateCustomMethodAggregMaxAnno();
        runTestAcctgTransInterfaceAggreg(true);
    }
    
}
