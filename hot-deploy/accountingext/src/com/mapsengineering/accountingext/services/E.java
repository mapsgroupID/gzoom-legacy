package com.mapsengineering.accountingext.services;

/**
 * Enumeration field for indicator calc service
 *
 */
public enum E {
    glFiscalTypeId, glFiscalTypeIdInput, glFiscalTypeIdOutput, glAccountId, calcCustomMethodId, customMethodName, acctgTransId, factorCalculator, decimalScale, defaultUomId, customMethodId,
    //
    inputEnumId, detailEnumId, productId,
    //
    WorkEffortMeasure, workEffortId, workEffortMeasureId, partyId, roleTypeId, fromDate, thruDate,
    //
    entryPartyId, entryRoleTypeId, entryProductId, workEfforTypeId,
    //
    entryVoucherRef, GlAccount, glAccountIdRef, weTransProductId, weTransMeasureId, transactionDate,
    //
    entryGlAccountId, entryGlFiscalTypeId, parentTypeId, weTransPartyId, weTransRoleTypeId,
    //
    workEffortSnapshotId, entryWorkEffortSnapshotId,
    //
    amount, origAmount, prioCalc, debitCreditDefault,
    //
    WorkEffort, WorkEffortTransactionIndicatorView,
    //
    workEffortParentId, weTransWeId, accountTypeEnumId, serviceName, serviceTypeId, sessionId, glAccountTypeId, weTransAccountId, glResourceTypeId, weTransTypeValueId,
    //
    GlAccountTypeGlFiscalTypeView,    
    //
    WorkEffortIndicatorView, AcctgTransAndEntries, GlAccountAndTypeAndFiscalType, AcctgTransAndEntriesView,
    //
    target, performance, cleanOnlyScoreCard, scoreValueType, WorkEffortType, workEffortTypeId, isRoot, Y, CustomMethod, detectOrgUnitIdFlag, Company, onlyElaborateIndicator, orgUnitId, orgUnitRoleTypeId, entryAmount,
    //
    WorkEffortTypeContent,
    //
    weTypeContentTypeId, contentId, dataResourceId, limitExcellent, limitMax, limitMed, limitMin, showOtherWeight, elabScoreIndic, weightType, searchDate, organizationPartyId,
    //
    WorkEfforMeasAcctgTEView, accGlAccountId, rootExecute, params
}
