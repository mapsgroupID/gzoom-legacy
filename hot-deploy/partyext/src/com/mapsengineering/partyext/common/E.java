package com.mapsengineering.partyext.common;

/**
 * Constant
 * @author dain
 *
 */
public enum E {
	WorkEffortPartyAssignment, WorkEffort, WorkEffortPartyAssignView, WorkEffortAndWorkEffortPartyAssView, WorkEffortMeasureView, WorkEffortAssocToView,
	//
	WorkEffortMeasure, WorkEffortAssoc, WorkEffortAssocFromView, jobLogger, PartyRelationship, Person, PartyHistory,
	//
	partyId, endDate, statusId, fromDate, thruDate, workEffortId, roleTypeId, estimatedStartDate, estimatedCompletionDate, workEffortRevisionId,
	//
	parentTypeId, isRoot, isTemplate, workEffortParentId, partyIdFrom, partyIdTo, roleTypeIdFrom, roleTypeIdTo,
	//
	weMeasureFromDate, weMeasureThruDate, weMeasureWeCompletionDate, weMeasureWeStartDate, workEffortMeasureId,
	//
	workEffortIdFrom, workEffortIdTo, workEffortAssocTypeId, weMeasureParentId, weMeasureId, returnMessages,
	//
	name, value, orgUniId, conditionParam, newEndDate, partyRelationshipTypeId,
	//
	WEM_EVAL_MANAGER, PARTY_ENABLED, Party, PARTY_DISABLED, WEM_EVAL_IN_CHARGE, CTX_EP, Y, N, WE_ASSIGNMENT,
	//
	locale, emplPositionTypeDate, employmentAmount, emplPositionTypeId, description, comments, lastName, firstName,
	//
	skipStore, orgUnitId
	
}
