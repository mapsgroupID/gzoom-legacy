package com.mapsengineering.emplperf.update;

/**
 * Service fields, entityName and entityName fields
 */
public enum EmplPerfServiceEnum {
    WorkEffortNoteAndDataCompareValue, WorkEffortAndAcctgTransCompareValue, WorkEffortAndTemplateAndEmplPositionTypeAndAcctgTransCompareValue,
    //
    WorkEffortAndEmplPositionTypeAndAcctgTransCompareValue, WorkEffortAndTemplateAndAcctgTransCompareValue, WorkEffortAndAssocCompareValue,
    //
    workEffortId, sourceReferenceId, workEffortTypeId, workEffortName, currentStatusId, statusId, description, 
	//
	workEffortSnapshotId, workEffortRevisionId,
	//
	estimatedStartDate, estimatedCompletionDate, sequenceId,
	//
	newEstimateStartDate, oldEstimateStartDate,
	//
	partyId, newNoteDateTime, oldNoteDateTime, newNoteId, oldNoteInfo, oldWorkEffortId, parentTypeId, isTemplate, isRoot, noteDateTime, evalEndDate, 
	//
	CTX_EP, N, Y, NoteData, noteId, noteInfo, glAccountId, workEffortPurposeTypeId, transactionDate, entryWorkEffortRevisionId, oldWorkEffortRevisionId, oldWeRevisionId, oldThruDate, 
	//
	AcctgTransInterface, WeRootInterface, WeAssocInterface,
	//
	dataSource, refDate, voucherRef, workEffortMeasureId, uomDescr, _NA_, glAccountCode, accountCode, amount, glFiscalTypeId, glAccountTypeEnumId, INDICATOR, uorgCode, orgUnitId, productCode, workEffortCode, 
	//
	partyCode, thruDate, evalPartyId, WorkEffortAssoc, WorkEffort, Person, emplPositionTypeId, templateTypeId, templateId, workEffortIdTo, workEffortAssocTypeId, 
	//
	WorkEffortType, WorkEffortPartyAssignment, WorkEffortMeasure, GlAccount, AcctgTrans, AcctgTransEntry, WorkEffortPurposeAccount, orgUnitRoleTypeId, workEffortRootId, acctgTransId, acctgTransEntrySeqId,
	//
	weContext, operationType, sourceReferenceRootId, workEffortTypeRootId, oldWeAssocTypeId, oldWeAssocFromDate, oldWeAssocThruDate, weFromSourceRefId,
	//
	workEffortAssocTypeCode, sourceReferenceRootIdFrom, sourceReferenceRootIdTo, workEffortTypeIdFrom, workEffortTypeIdTo, weFromTypeId, weFromSourceRefRootId, sourceReferenceIdFrom, sourceReferenceIdTo,
	//
	sequenceNum, assocWeight, oldWeAssocSequenceNum, oldWeAssocWeight, workEffortNameFrom, workEffortNameTo, workEffortParentId, sameWorkEffortId, 
	//
	fromDate, oldEstimatedStartDate, oldEstimatedCompletionDate, id
	
}
