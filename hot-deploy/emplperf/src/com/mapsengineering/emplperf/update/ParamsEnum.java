package com.mapsengineering.emplperf.update;

/**
 * Service fields, entityName and entityName fields
 */
public enum ParamsEnum {
    orgUnitRoleTypeId, orgUnitId, emplPositionTypeId, templateTypeId, templateId,
    // 
    updateAcctgTransAndEntry, updateWorkEffortPurposeType, updateWorkEffortNote, updateWorkEffortAssoc, updateWorkEffortAssocType,
	//
	estimatedStartDate, estimatedCompletionDate, readDate, writeDate, oldEstimatedCompletionDate
}
