package com.mapsengineering.emplperf.insert;

import javolution.util.FastList;

import java.util.List;


/**
 * Return fields
 *
 */
public class EmplPerfInsertFromTemplateOrberByFields {
    
    /**
     * Return Map with field
     * @return
     */
    public static List<String> getOrderByFields() {
    	List<String> toOrder = FastList.newInstance();
        toOrder.add(ParamsEnum.partyId.name());
        toOrder.add(ParamsEnum.templateId.name());
        toOrder.add(ParamsEnum.orgUnitId.name());
        toOrder.add(ParamsEnum.effort.name()); 
        toOrder.add(ParamsEnum.fromDate.name());

        return toOrder;
    }
}
