package com.mapsengineering.emplperf.insert;

/**
 * Service fields, entityName and entityName fields
 */
public enum EmplPerfRootViewFieldEnum {
    EmplPerfRootView, WorkEffort,
    //
    workEffortId, workEffortParentId, workEffortSnapshotId, workEffortRevisionId,
	//
	estimatedStartDate, estimatedCompletionDate, wepaPartyId, orgUnitId, valPartyId, appPartyId, templateId, parentTypeId, organizationId
	
	
}
