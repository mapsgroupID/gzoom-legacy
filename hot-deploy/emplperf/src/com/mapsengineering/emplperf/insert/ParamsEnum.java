package com.mapsengineering.emplperf.insert;


/**
 * Service fields, entityName and entityName fields
 */
public enum ParamsEnum {
    Party, WorkEffort, Person, PartyParentRole, EMPLOYEE, RelationshipTemplateView, 
    //
    emplPerfInsertFromTemplate, BaseUiLabels, 
    //
    orgUnitRoleTypeId, orgUnitId, emplPositionTypeId, templateTypeId, templateId, parentRoleCode, roleTypeId, forcedTemplateId, 
    //
	estimatedStartDate, estimatedCompletionDate, readDate, writeDate, workEffortId, workEffortName, partyId, partyName, emplPositionTypeDate, showCode,
	//
	entityView, emplPerfInsertFromTemplateParams, partyRelationshipTypeId,  
	//
    value, evaluator, approver, fromDate, thruDate, effort, defaultOrganizationPartyId, organizationId, multiplicity, parentTypeId;

}
