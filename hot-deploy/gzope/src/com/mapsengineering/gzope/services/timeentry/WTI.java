package com.mapsengineering.gzope.services.timeentry;

/**
 * Enumeration for query
 */
public enum WTI {
    showParty, workEffortId, hours, projectName, taskName, projectId, taskId, partyId, estimatedStartDate, estimatedCompletionDate, partyName, 
    //
    WORK_EFFORT_ID, HOURS, PROJECT_NAME, TASK_NAME, PROJECT_ID, TASK_ID, PARTY_ID, ESTIMATED_START_DATE, ESTIMATED_COMPLETION_DATE, PARTY_NAME
}
