
<style type="text/css">

div.lookup_field input.lookup_field_description {
	border: 0;
}

</style>


<div class="lookup_field" name="lookupField01" id="lookupField01">

	<input type="hidden" name="entityName" value="Party" class="autocompleter_parameter"/>
	<input type="hidden" name="selectFields" value="[partyId, partyTypeId, statusId]" class="autocompleter_parameter"/>
	<input type="hidden" name="sortByFields" value="[partyId]" class="autocompleter_parameter"/>
	<input type="hidden" name="displayFields" value="[partyId, partyTypeId]" class="autocompleter_parameter"/>
	
	<div style="display: inline;">
		<input type="text" value="" name="partyId" class="lookup_field_code" id="lookupField01_partyId"/>
		<a style="cursor: pointer;"	class="lookup_field_submit"><img src="/images/fieldlookup.gif" width="15" height="15" border="0" alt="Lookup"/></a>
		<input type="text"  class="lookup_field_description partyId"  readonly/>
		<input type="text"  class="lookup_field_description partyTypeId" readonly/>
		<input type="text"  class="lookup_field_description statusId"  readonly/>
	</div>
</div>

<div id="lookup_field001" name="lookup_field001" class="lookup_field">

	<div>
		<input type="text" name="partyId" id="lookup_field001_partyId" class="lookup_field_code" autocomplete="off"/>
		<div id="lookup_field001_autoCompleterOptions" class="autocomplete" style="display: none;"/>
		<a class="lookup_field_submit" style="cursor: pointer;"><img width="15" height="15" border="0" alt="Lookup" src="/images/fieldlookup.gif"/></a>
		<input type="text" readonly="" class="lookup_field_description firstName"/>
		<input type="text" readonly="" class="lookup_field_description groupName"/>
	</div>
</div>