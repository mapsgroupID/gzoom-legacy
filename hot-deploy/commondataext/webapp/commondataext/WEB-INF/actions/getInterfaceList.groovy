def interfaceList = [];
def organizationInterfaceField = [:];
organizationInterfaceField.interfaceId = "ORGANIZATION_INTERFACE";
organizationInterfaceField.description = uiLabelMap.InterfaceOrganizationName;
interfaceList.add(organizationInterfaceField);

def personInterfaceField = [:];
personInterfaceField.interfaceId = "PERSON_INTERFACE";
personInterfaceField.description = uiLabelMap.InterfacePersonName;
interfaceList.add(personInterfaceField);

def glAccountInterfaceField = [:];
glAccountInterfaceField.interfaceId = "GL_ACCOUNT_INTERFACE";
glAccountInterfaceField.description = uiLabelMap.InterfaceGlAccountName;
interfaceList.add(glAccountInterfaceField);

def acctgTransInterfaceField = [:];
acctgTransInterfaceField.interfaceId = "ACCTG_TRANS_INTERFACE";
acctgTransInterfaceField.description = uiLabelMap.InterfaceAcctgTransName;
interfaceList.add(acctgTransInterfaceField);

def weRootInterfaceField = [:];
weRootInterfaceField.interfaceId = "WE_ROOT_INTERFACE";
weRootInterfaceField.description = uiLabelMap.InterfaceWeRootName;
interfaceList.add(weRootInterfaceField);

def weInterfaceField = [:];
weInterfaceField.interfaceId = "WE_INTERFACE";
weInterfaceField.description = uiLabelMap.InterfaceWeName;
interfaceList.add(weInterfaceField);

def weNoteInterfaceField = [:];
weNoteInterfaceField.interfaceId = "WE_NOTE_INTERFACE";
weNoteInterfaceField.description = uiLabelMap.InterfaceWeNoteName;
interfaceList.add(weNoteInterfaceField);

def weMeasureInterfaceField = [:];
weMeasureInterfaceField.interfaceId = "WE_MEASURE_INTERFACE";
weMeasureInterfaceField.description = uiLabelMap.InterfaceWeMeasureName;
interfaceList.add(weMeasureInterfaceField);

def weAssocInterfaceField = [:];
weAssocInterfaceField.interfaceId = "WE_ASSOC_INTERFACE";
weAssocInterfaceField.description = uiLabelMap.InterfaceWeAssocName;
interfaceList.add(weAssocInterfaceField);

def wePartyInterfaceField = [:];
wePartyInterfaceField.interfaceId = "WE_PARTY_INTERFACE";
wePartyInterfaceField.description = uiLabelMap.InterfaceWePartyName;
interfaceList.add(wePartyInterfaceField);

context.interfaceList = interfaceList;