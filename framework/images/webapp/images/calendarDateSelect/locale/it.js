Date.weekdays = $w('L Ma Me G V S D');
Date.months = $w('Gennaio Febbraio Marzo Aprile Maggio Giugno Luglio Agosto Settembre Ottobre Novembre Dicembre');

Date.first_day_of_week = 1;

_translations = {
  "OK": "OK",
  "Now": "Ora",
  "Today": "Oggi",
  "Clear": "Pulisci"
}

var dataFormatJs = "format_italian.js"

var e = document.createElement("script");
e.src = "/images/calendarDateSelect/format/" + dataFormatJs;
e.type="text/javascript";
document.getElementsByTagName("head")[0].appendChild(e);