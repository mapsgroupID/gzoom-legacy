Date.weekdays = $w("S M T W T F S");
Date.months = $w("January February March April May June July August September October November December" );

Date.first_day_of_week = 1;

_translations = {
	"OK": "OK",
    "Now": "Now",
    "Today": "Today",
  "Clear": "Clear"
}

var dataFormatJs = "format_english.js"

var e = document.createElement("script");
e.src = "/images/calendarDateSelect/format/" + dataFormatJs;
e.type="text/javascript";
document.getElementsByTagName("head")[0].appendChild(e);