<?xml version="1.0" encoding="UTF-8"?>
<forms xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:noNamespaceSchemaLocation="http://ofbiz.apache.org/dtds/widget-form.xsd">

    <form name="WorkEffortRootViewSearchForm" extends="WorkEffortRootViewSearchForm" extends-resource="component://workeffortext/widget/forms/WorkEffortRootViewForms.xml">
        <actions>
            <set field="orderBy" from-field="parameters.orderBy" default-value="sourceReferenceId|workEffortName|weTypeDescription|weStatusDescr|workEffortId"/>
            <set field="weContextId" value="CTX_PM"/>
            <script location="component://workeffortext/webapp/workeffortext/WEB-INF/actions/executePerformFindPartyAndPartyParentRoleAndRoleType.groovy"/>            
        </actions>
        <field name="weContextId">
            <hidden value="CTX_PM"/>
        </field>
        <field name="weIsTemplate">
            <hidden value="N"/>
        </field>
        <field name="workEffortTypeId" title="${uiLabelMap.WorkEffortRootType}">
            <drop-down type="drop-list" maxlength="255" size="68" local-autocompleter="false" drop-list-key-field="workEffortTypeId" drop-list-display-field="${workEffortTypeDisplayField}">
                <entity-options entity-name="WorkEffortWithTypeView" key-field-name="workEffortTypeId" description="${bsh: &quot;Y&quot;.equals(context.get(&quot;localeSecondarySet&quot;)) ? descriptionLang : description}" distinct="true">
                    <select-field field-name="${workEffortTypeDisplayField}" display="true"/>
                    <select-field field-name="workEffortTypeId"/>
                    <select-field field-name="seqEsp" display="hidden"/>
                    <entity-constraint name="isTemplate" value="N"/>
                    <entity-constraint name="isRoot" value="Y"/>
                    <entity-constraint name="parentTypeId" value="CTX_PM"/>
                    <entity-constraint name="statusId" operator="like" value="%${currentStatusId_value}%"/>
                    <entity-constraint name="gpMenuEnumId" env-name="gpMenuEnumId"/>
                    <entity-constraint name="organizationId" value="${defaultOrganizationPartyId}"/>
                    <entity-order-by field-name="seqEsp"/>
                    <entity-order-by field-name="workEffortTypeId"/>                 
                </entity-options>            
            </drop-down>
        </field>
        <field name="currentStatusId_op">
            <hidden value="contains"/>
        </field>
        <field name="currentStatusId_value">
            <hidden value="WEPRJST"/>
        </field>
        <field name="childStruct" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return (security.hasPermission(&quot;PROJECTMGR_ADMIN&quot;, userLogin))}">
            <drop-down type="drop-down" allow-empty="false" no-current-selected-key="N" current="selected">
                <option key="Y" description="${uiLabelMap.CommonYes}"/>
                <option key="N" description="${uiLabelMap.CommonNo}"/>
            </drop-down>
        </field>
        <sort-order>
            <sort-field name="organizationId"/>
            <sort-field name="group_OrgUnit_sep"/>
            <sort-field name="group_OrgUnit"/>
            <sort-field name="orgUnitRoleTypeId"/>
            <sort-field name="orgUnitId"/>
            <sort-field name="childStruct"/>
            <sort-field name="responsibleRoleTypeId"/>
            <sort-field name="responsiblePartyId"/>
            <sort-field name="group_WorkEffortRole_sep"/>
            <sort-field name="group_WorkEffortRole"/>            
            <sort-field name="weResponsibleRoleTypeId"/>
            <sort-field name="weResponsiblePartyId"/>
            <sort-field name="group_Others_sep"/>
            <sort-field name="group_Others"/>
            <sort-field name="currentStatusId"/>
            <sort-field name="workEffortTypeId"/>
            <sort-field name="sourceReferenceId"/>
            <sort-field name="weEtch"/>
            <sort-field name="workEffortName"/>
            <sort-field name="workEffortNameLang"/>
            <sort-field name="weActivation"/>
            <sort-field name="referenceDate"/>
        </sort-order>
    </form>
    
    <form name="WorkEffortRootViewAdvancedSearchForm" extends="WorkEffortRootViewAdvancedSearchForm" extends-resource="component://workeffortext/widget/forms/WorkEffortRootViewForms.xml">
    </form>
    
    <form name="WorkEffortRootViewSearchResultListForm" id="WETSRL001OR_WorkEffortType" extends="WorkEffortRootViewSearchResultListForm"  extends-resource="component://workeffortext/widget/forms/WorkEffortRootViewForms.xml">
        <field name="weContextId">
            <hidden/>
        </field>
        <field name="weIsTemplate">
            <hidden value="N"/>
        </field>
        <field name="statusIdParameters">
            <hidden value="WEPRJST"/>
        </field>
    </form>
    
    <form name="WorkEffortRootViewSearchResultContextForm" extends="WorkEffortRootViewSearchResultContextForm" extends-resource="component://workeffortext/widget/forms/WorkEffortRootViewForms.xml">
        <actions>
            <set field="insertButtonAuxParameters" value="&amp;weIsTemplate=N&amp;rootTree=N&amp;rootInqyTree=N&amp;loadTreeView=N&amp;gpMenuEnumId=${parameters.gpMenuEnumId}"/>
            <set field="entityName" value="WorkEffortView"/>
            <set field="extraParams" value="selectedId=${parameters.selectedId}&amp;rootTree=N&amp;rootInqyTree=N&amp;loadTreeView=Y&amp;specialized=Y"/>
        </actions>
    </form>
    
    <form name="WorkEffortRootViewManagementForm" id="WorkEffortRootViewManagementForm" extends="WorkEffortRootViewManagementForm" extends-resource="component://workeffortext/widget/forms/WorkEffortRootViewForms.xml">
        <!-- Bug 3994 -->
        <!-- isEtchReadOnly é valutato in WorkEffortRootViewManagementForm dentro WorkEffortRootViewForms.xml -->
        <field name="sourceReferenceId" use-when="${bsh: !&quot;Y&quot;.equals(context.get(&quot;insertMode&quot;)) &amp;&amp; &quot;Y&quot;.equals(context.get(&quot;showCode&quot;))}">
            <text size="25" maxlength="60" read-only="${isEtchReadOnly}"/>
        </field>        
        <field name="etch" use-when="${bsh: !&quot;Y&quot;.equals(context.get(&quot;insertMode&quot;)) &amp;&amp; &quot;Y&quot;.equals(context.get(&quot;showEtchField&quot;))}">
            <text size="25" maxlength="20" read-only="${isEtchReadOnly}"/>
        </field>
        <!-- End 3994 -->       
        <field name="workEffortTypeId" required-field="true" widget-area-style="singleSmall">
            <drop-down show-tooltip="true" tooltip="${parameters.workEffortId}" type="drop-list" read-only="!&quot;Y&quot;.equals(parameters.get(&quot;insertMode&quot;))" maxlength="255" size="100" local-autocompleter="false" drop-list-key-field="workEffortTypeId" drop-list-display-field="${workEffortTypeEtchDisplayField}">
                <entity-options entity-name="WorkEffortTypeParentRelView" key-field-name="workEffortTypeId" description="${bsh: &quot;Y&quot;.equals(context.get(&quot;localeSecondarySet&quot;)) ? etchLang : etch}">
                	<select-field field-name="orgUnitRoleTypeId" display="hidden"/>
                	<select-field field-name="orgUnitRoleTypeId2" display="hidden"/>
                	<select-field field-name="orgUnitRoleTypeId3" display="hidden"/>                 	
                    <select-field field-name="orgUnitRoleTypeDesc" display="hidden"/>
                    <select-field field-name="orgUnitRoleTypeDescLang" display="hidden"/>
                    <select-field field-name="frameEnumId" display="hidden"/>
                    <select-field field-name="parentRelTypeId" display="hidden"/>                    
                    <select-field field-name="parentRelTypeIdRef" display="hidden"/>                    
                    <select-field field-name="parentRelTypeDesc" display="hidden"/>
                    <select-field field-name="parentRelTypeDescLang" display="hidden"/>
                    <select-field field-name="wePurposeTypeIdWe" display="hidden"/>
                    <select-field field-name="purposeEtch" display="hidden"/>                      
                    <select-field field-name="${workEffortTypeEtchDisplayField}" display="true"/>
                    <select-field field-name="workEffortTypeId"/>

                    <entity-constraint name="isRoot" value="Y"/>
                    <entity-constraint name="parentTypeId" value="CTX_PM"/>
                    <entity-constraint name="isRootActive" value="Y"/>
                    <entity-constraint name="gpMenuEnumId" env-name="gpMenuEnumId"/>
                    <entity-order-by field-name="seqEsp"/>
                    <entity-order-by field-name="workEffortTypeId"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="workEffortTypeId" widget-area-style="singleSmall" required-field="true" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isEmpty(parameters.get(&quot;workEffortIdFrom&quot;)) &amp;&amp; UtilValidate.isNotEmpty(parameters.get(&quot;weIsTemplate&quot;))}">
            <drop-down show-tooltip="true" tooltip="${parameters.workEffortId}" type="drop-list" read-only="!&quot;Y&quot;.equals(parameters.get(&quot;insertMode&quot;))" maxlength="255" size="100" local-autocompleter="false" drop-list-key-field="workEffortTypeId" drop-list-display-field="${workEffortTypeEtchDisplayField}">
                <entity-options entity-name="WorkEffortTypeParentRelView" key-field-name="workEffortTypeId" description="${bsh: &quot;Y&quot;.equals(context.get(&quot;localeSecondarySet&quot;)) ? etchLang : etch}">
                	<select-field field-name="orgUnitRoleTypeId" display="hidden"/>
                	<select-field field-name="orgUnitRoleTypeId2" display="hidden"/>
                	<select-field field-name="orgUnitRoleTypeId3" display="hidden"/>                 	
                    <select-field field-name="orgUnitRoleTypeDesc" display="hidden"/>
                    <select-field field-name="orgUnitRoleTypeDescLang" display="hidden"/>
                    <select-field field-name="frameEnumId" display="hidden"/>
                    <select-field field-name="parentRelTypeId" display="hidden"/>                    
                    <select-field field-name="parentRelTypeIdRef" display="hidden"/>                    
                    <select-field field-name="parentRelTypeDesc" display="hidden"/>
                    <select-field field-name="parentRelTypeDescLang" display="hidden"/>
                    <select-field field-name="wePurposeTypeIdWe" display="hidden"/>
                    <select-field field-name="purposeEtch" display="hidden"/>                      
                    <select-field field-name="${workEffortTypeEtchDisplayField}" display="true"/>
                    <select-field field-name="workEffortTypeId"/>

                    <entity-constraint name="isTemplate" env-name="parameters.weIsTemplate"/>
                    <entity-constraint name="isRoot" value="Y"/>
                    <entity-constraint name="parentTypeId" value="CTX_PM"/>
                    <entity-constraint name="isRootActive" value="Y"/>
                    <entity-constraint name="gpMenuEnumId" env-name="gpMenuEnumId"/>
                    <entity-order-by field-name="seqEsp"/>
                    <entity-order-by field-name="workEffortTypeId"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="workEffortTypeId" widget-area-style="singleSmall" required-field="true" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return !&quot;Y&quot;.equals(context.get(&quot;weIsRoot&quot;))}">
            <drop-down show-tooltip="true" tooltip="${parameters.workEffortId}" type="drop-list" maxlength="255" size="100" read-only="!&quot;Y&quot;.equals(parameters.get(&quot;insertMode&quot;))" local-autocompleter="false" drop-list-key-field="workEffortTypeId" drop-list-display-field="${workEffortTypeEtchDisplayField}">
                <entity-options entity-name="WorkEffortTypeTypeParentRelView" key-field-name="workEffortTypeId" description="${bsh: &quot;Y&quot;.equals(context.get(&quot;localeSecondarySet&quot;)) ? etchLang : etch}">
                	<select-field field-name="orgUnitRoleTypeId" display="hidden"/>
                	<select-field field-name="orgUnitRoleTypeId2" display="hidden"/>
                	<select-field field-name="orgUnitRoleTypeId3" display="hidden"/>                 	
                    <select-field field-name="orgUnitRoleTypeDesc" display="hidden"/>
                    <select-field field-name="orgUnitRoleTypeDescLang" display="hidden"/>
                    <select-field field-name="frameEnumId" display="hidden"/>
                    <select-field field-name="parentRelTypeId" display="hidden"/>                    
                    <select-field field-name="parentRelTypeIdRef" display="hidden"/>                    
                    <select-field field-name="parentRelTypeDesc" display="hidden"/>
                    <select-field field-name="parentRelTypeDescLang" display="hidden"/>
                    <select-field field-name="wePurposeTypeIdWe" display="hidden"/>
                    <select-field field-name="purposeEtch" display="hidden"/>                      
                    <select-field field-name="${workEffortTypeEtchDisplayField}" display="true"/>
                    <select-field field-name="workEffortTypeId"/>
                    <entity-constraint name="workEffortTypeIdRoot" env-name="workEffortTypeIdRoot"/>
                    <entity-constraint name="workEffortTypeIdFrom" env-name="workEffortTypeIdFrom"/>
                    <entity-constraint name="isRootActive" value="Y"/>
                    <entity-constraint name="gpMenuEnumId" env-name="gpMenuEnumId"/>     
                    <entity-order-by field-name="sequenceNum"/>
                </entity-options>
            </drop-down>
        </field>
    </form>
    
    <form name="WorkEffortRootViewCreateFromTemplateManagementForm" id="WorkEffortRootViewCreateFromTemplateManagementForm"  extends="BaseManagementForm" extends-resource="component://base/widget/BaseForms.xml">
        <read-only>
            <not>
                <or>
                    <if-has-permission permission="PROJECTMGR_ADMIN"/>
                    <if-has-permission permission="PROJECTMGR" action="_CREATE"/>
                    <if-has-permission permission="PROJECTMGR_ORG_ADMIN"/>
                </or>
            </not>
        </read-only>
        <field name="crudService">
            <hidden value="crudServiceDefaultOrchestration_WorkEffortRootView"/>
        </field>
        <field name="_AUTOMATIC_PK_">
            <hidden value="Y"/>
        </field>
        <field name="workEffortId">
            <hidden/>
        </field>
        <field name="weContextId">
            <hidden value="CTX_PM"/>
        </field>
        <!-- Sogg Valutato -->
        <!-- PartyEvalPermissionView per trovare i soggetti valutati legati al Responsabile e Valutatore che effettua il login -->
        <!-- PartyEvalOrgPermissionView per trovare i soggetti valutati legati al Responsabile che effettua il login -->
        <!-- PartyEvalRolePermissionView per trovare i soggetti valutati legati al Valutatore che effettua il login -->
        <!-- PartyRoleView per trovare i soggetti valutati all'amministratore che effettua il login -->
        <field name="evalPartyId" required-field="true">
            <lookup  target="lookupPartyEvalView" target-parameter="lookupScreenLocation=component://partyext/widget/screens/LookupScreens.xml,noConditionFind=N,saveView=N" target-form-name="lookUpPartyEvalViewForm" modal-lookup="true" edit-field-name="parentRoleCode" key-field-name="partyId" description-field-name="partyName">
                <entity-options entity-name="PartyRoleView" key-field-name="partyId" description="${partyName}">
                    <select-field field-name="partyName" display="true"/>
                    <select-field field-name="partyId" display="hidden"/>
                    <entity-constraint name="parentRoleTypeId" value="EMPLOYEE"/>
                    <entity-constraint name="roleTypeId" value="WEM_EVAL_IN_CHARGE"/>
                    <entity-order-by field-name="parentRoleCode"/>
                </entity-options>
            </lookup>
        </field>
        <!-- Unit Org del soggetto Valutato -->
        <!-- PartyEvalOrganizationView per trovare le unità organizzative dei soggetti valutati -->
        <!-- PartyEvalOrganizationOrgPermissionView per trovare le unità organizzative dei soggetti valutati legati all' utente loggato se è un Responsabile-->
        <field name="orgUnitId" required-field="true" use-when="${bsh: !security.hasPermission(&quot;PROJECTMGR_ORG_ADMIN&quot;, userLogin) }">
            <lookup target="lookupPartyEvalOrganizationView" target-parameter="lookupScreenLocation=component://partyext/widget/screens/LookupScreens.xml,noConditionFind=N,saveView=N" target-form-name="lookupPartyEvalOrganizationViewForm" modal-lookup="true" edit-field-name="parentRoleCode" key-field-name="partyId" description-field-name="partyName">
                <entity-options entity-name="PartyEvalOrganizationView" key-field-name="partyId" description="${partyName}">
                    <select-field field-name="partyName" display="true"/>
                    <select-field field-name="roleDescription" display="true"/>
                    <select-field field-name="roleTypeId" display="hidden-disabled"/>
                    <select-field field-name="partyId" display="hidden"/>
                    <entity-constraint name="parentRoleTypeId" value="ORGANIZATION_UNIT"/>
                    <entity-constraint name="evalPartyId" env-name="evalPartyId" lookup-for-field-name="evalPartyId"/>
                    <entity-order-by field-name="parentRoleCode"/>
                </entity-options>
            </lookup>
        </field>
        <field name="orgUnitId" required-field="true" use-when="${bsh: security.hasPermission(&quot;PROJECTMGR_ORG_ADMIN&quot;, userLogin) }">
            <lookup modal-lookup="true" target="lookupPartyEvalOrganizationOrgPermissionView" target-parameter="lookupScreenLocation=component://partyext/widget/screens/LookupScreens.xml,noConditionFind=N,saveView=N" size="25" target-form-name="lookupPartyEvalOrganizationOrgPermissionViewForm" edit-field-name="parentRoleCode" key-field-name="partyId" description-field-name="partyName">
                <entity-options entity-name="PartyEvalOrganizationOrgPermissionView" key-field-name="partyId" description="${partyName}">
                    <select-field field-name="partyName" display="true"/>
                    <select-field field-name="roleDescription" display="true"/>
                    <select-field field-name="roleTypeId" display="hidden-disabled"/>
                    <select-field field-name="partyId" display="hidden"/>
                    <entity-constraint name="parentRoleTypeId" value="ORGANIZATION_UNIT"/>
                    <entity-constraint name="evalPartyId" env-name="evalPartyId" lookup-for-field-name="evalPartyId"/>
                    <entity-order-by field-name="parentRoleCode"/>
                </entity-options>
            </lookup>
        </field>
        <field name="uoRoleTypeId">
            <hidden/>
        </field>
        <field name="evalManagerPartyId" required-field="true">
            <lookup modal-lookup="true" target="lookupPartyEvalManagerView" target-parameter="lookupScreenLocation=component://partyext/widget/screens/LookupScreens.xml,noConditionFind=N,saveView=N" size="25" target-form-name="lookupPartyEvalManagerViewForm" edit-field-name="parentRoleCode" key-field-name="partyId" description-field-name="partyName">
                <entity-options entity-name="PartyEvalManagerView" key-field-name="partyId" description="${partyName}">
                    <select-field field-name="partyName" display="true"/>
                    <select-field field-name="partyId" display="hidden"/>
                    <entity-constraint name="estimated" env-name="evalPartyId" lookup-for-field-name="evalPartyId"/>
                    <entity-constraint name="roleTypeId" value="WEM_EVAL_MANAGER"/>
                    <entity-order-by field-name="parentRoleCode"/>
                </entity-options>
            </lookup>
        </field>
        <field name="evalManagerPartyId" use-when="${bsh: security.hasPermission(&quot;ORGPERFROLE_ADMIN&quot;, userLogin) &amp;&amp; !security.hasPermission(&quot;PROJECTMGR_ORG_ADMIN&quot;, userLogin)}">
            <hidden value="${userLogin.partyId}"/>
        </field>
        <field name="partyName" title="${uiLabelMap.FormFieldTitle_evalManagerPartyId}" use-when="${bsh: !security.hasPermission(&quot;PROJECTMGR_ORG_ADMIN&quot;, userLogin)}" required-field="true">
            <display description="${bsh: import org.ofbiz.party.party.PartyHelper; import org.ofbiz.base.util.UtilValidate; UtilValidate.isNotEmpty(parameters.get(&quot;partyName&quot;)) ? parameters.get(&quot;partyName&quot;) : PartyHelper.getPartyName(delegator, userLogin.get(&quot;partyId&quot;), true);}"/>
        </field>
        <field name="templateId" title="${uiLabelMap.TemplateWorkEffortId}" required-field="true">
            <lookup modal-lookup="true" target="lookupTemplateView" target-parameter="lookupScreenLocation=component://emplperf/widget/screens/LookupScreens.xml,noConditionFind=N,saveView=N" size="25" target-form-name="lookupTemplateViewForm" edit-field-name="sourceReferenceId" key-field-name="workEffortId" description-field-name="workEffortName">
                <entity-options entity-name="TemplateView" key-field-name="workEffortId" description="${workEffortName}">
                    <select-field field-name="workEffortId" display="hidden"/>
                    <select-field field-name="workEffortName" display="true"/>
                    <select-field field-name="estimatedStartDate" display="hidden-disabled"/>
                    <select-field field-name="estimatedCompletionDate" display="hidden-disabled"/>
                    <entity-constraint name="partyId" env-name="orgUnitId" lookup-for-field-name="orgUnitId"/>
                    <entity-constraint name="evalPartyId" env-name="evalPartyId" lookup-for-field-name="evalPartyId"/>
                    <entity-constraint name="roleTypeId" env-name="uoRoleTypeId" lookup-for-field-name="uoRoleTypeId"/>
                </entity-options>
            </lookup>    
        </field>
        <field name="description" required-field="true">
            <text default-value="${partyName} - ${workEffortName}" size="50" maxlength="50"/>
        </field>
        <field name="comments" title="${uiLabelMap.Nota}">
            <text size="50" maxlength="255"/>
        </field>
        <field name="tempEstimatedStartDate" required-field="true">
            <date-time type="date"/>
        </field>
        <field name="tempEstimatedCompletionDate" required-field="true">
            <date-time type="date"/>
        </field>
    </form>
</forms>
