<?xml version="1.0" encoding="UTF-8"?>
<screens xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:noNamespaceSchemaLocation="http://ofbiz.apache.org/dtds/widget-screen.xsd">

    <screen name="main-decorator-ui-labels">
        <section>
            <actions>
                <property-map resource="CustomUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="TrasPerfUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="WorkeffortExtUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="WorkeffortExtEntityUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="BaseUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="WorkEffortUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="BaseContentUiLabels" map-name="uiLabelMap" global="true"/>
            </actions>
            <widgets/>
        </section>
    </screen>
    
    <screen name="main-decorator">
        <section>
            <actions>
                <property-map resource="CustomUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="TrasPerfUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="WorkeffortExtUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="WorkeffortExtEntityUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="BaseUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="WorkEffortUiLabels" map-name="uiLabelMap" global="true"/>
                
                <set field="layoutSettings.companyName" from-field="uiLabelMap.WorkeffortExtCompanyName" global="true"/>
                <set field="layoutSettings.companySubtitle" from-field="uiLabelMap.WorkeffortExtCompanySubtitle" global="true"/>

                <set field="applicationTitle" value="${uiLabelMap.WorkeffortExtApplication}" global="true"/>

                <set field="layoutSettings.javaScripts[]" value="/images/log4javascript-1.4/log4javascript.js" global="true"/>
            </actions>
            <widgets>
                <decorator-screen name="BaseCommonDecorator" location="component://base/widget/BaseScreens.xml">
                    <decorator-section name="body">
                        <section>
                            <actions>
                                <set field="layoutSettings.javaScriptBlocks[]" value="component://base/webapp/resources/ftl/validation-manager.js.ftl"/>
                            </actions>
                            <widgets>
                                <include-screen name="loadjavascript" location="component://base/widget/CommonScreens.xml"/>
                            </widgets>
                        </section>
                        <decorator-section-include name="body"/>
                    </decorator-section>
                </decorator-screen>
            </widgets>
        </section>
    </screen>

    <screen name="SearchScreen">
        <section>
            <actions>
                <set field="searchFormLocation" value="${parameters.searchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="searchFormResultLocation" value="${parameters.searchFormResultLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="advancedSearchFormLocation" value="${parameters.advancedSearchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="SearchCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="SearchContainerOnlyScreen">
        <section>
            <actions>
                <set field="searchFormLocation" value="${parameters.searchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="searchFormResultLocation" value="${parameters.searchFormResultLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="advancedSearchFormLocation" value="${parameters.advancedSearchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="SearchContainerOnlyCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>
    
    <screen name="SearchSimpleContainerOnlyScreen">
        <section>
            <actions>            
                <set field="searchFormScreenLocation" value="${parameters.searchFormScreenLocation}" default-value="component://base/widget/SearchScreens.xml"/>
                <set field="searchFormScreenName" value="${parameters.searchFormScreenName}" default-value="SearchFormScreen"/>
                <set field="searchFormLocation" value="${searchFormLocation}" default-value="${parameters.searchFormLocation}"/>
                <set field="searchFormResultLocation" value="${searchFormResultLocation}" default-value="${parameters.searchFormResultLocation}"/>
                
                <set field="searchResultContextFormLocation" value="${searchResultContextFormLocation}" default-value="${parameters.searchResultContextFormLocation}"/>
                <set field="searchResultContextFormName" value="${searchResultContextFormName}" default-value="${parameters.searchResultContextFormName}"/>
                
                <set field="advancedSearchFormLocation" value="${advancedSearchFormLocation}" default-value="${parameters.advancedSearchFormLocation}"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://stratperf/widget/menus/StratPerfMenus.xml"/>
                
                <set field="entityName" value="${entityName}" default-value="${parameters.entityName}"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="${searchFormScreenName}" location="${searchFormScreenLocation}"/>
            </widgets>
        </section>
    </screen>

    <screen name="SearchResultContainerOnlyScreen">
        <section>
            <actions>
                <set field="searchFormLocation" value="${parameters.searchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="searchFormResultLocation" value="${parameters.searchFormResultLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="advancedSearchFormLocation" value="${parameters.advancedSearchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="SearchResultContainerOnlyCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="ManagementScreen">
        <section>
            <actions>
                <set field="managementFormLocation" value="${parameters.managementFormLocation}"/>
                <set field="managementTabMenuLocation" value="${parameters.managementTabMenuLocation}" default-value="component://workeffortext/widget/WorkeffortExtMenus.xml"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <container>
                    <include-screen name="main-decorator-ui-labels"/>
                    <include-screen name="ManagementCommonScreen" location="component://base/widget/CommonScreens.xml"/>
                </container>
            </widgets>
        </section>
    </screen>

    <screen name="ManagementContainerOnlyScreen">
        <section>
            <actions>
                <set field="managementFormLocation" value="${parameters.managementFormLocation}"/>
                <set field="managementTabMenuLocation" value="${parameters.managementTabMenuLocation}" default-value="component://workeffortext/widget/WorkeffortExtMenus.xml"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="ManagementContainerOnlyCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="SubFolderManagementContainerOnlyScreen">
        <section>
            <actions>
                <set field="managementFormLocation" value="${parameters.managementFormLocation}"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="SubFolderManagementContainerOnlyCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="ChildManagementContainerOnlyScreen">
        <section>
            <actions>
                <set field="managementFormLocation" value="${parameters.managementFormLocation}"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="ChildManagementContainerOnlyCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="ChildManagementListContainerOnlyScreen">
        <section>
            <actions>
                <set field="managementFormLocation" value="${parameters.managementFormLocation}"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="ChildManagementListContainerOnlyCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>
    
    <screen name="ShowPortalPageContainerOnly">
        <section>
            <widgets>
            	<include-screen name="main-decorator-ui-labels"/>
                <include-screen name="ShowPortalPageContainerOnly" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="ShowPortletContainerOnly">
        <section>
            <widgets>
            	<include-screen name="main-decorator-ui-labels"/>
                <include-screen name="ShowPortletContainerOnly" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="LookupScreen">
        <section>
            <actions>
                <set field="searchScreenName" value="${parameters.searchScreenName}" default-value="SearchBaseScreen"/>
                <set field="searchFormLocation" value="${parameters.searchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="searchFormResultLocation" value="${parameters.searchFormResultLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="advancedSearchFormLocation" value="${parameters.advancedSearchFormLocation}" default-value="component://base/widget/BaseForms.xml"/>
                <set field="actionMenuLocation" value="${parameters.actionMenuLocation}" default-value="component://trasperf/widget/menus/TrasPerfMenus.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="LookupCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="FoScreen">
        <section>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="FoCommonScreen" location="component://base/widget/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>

    <screen name="TrasPerfPrintBirt">
        <section>
            <actions>
                <set field="parentTypeId" value="CTX_TR"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="TrasPerfPrintBirtSearch"/>
            </widgets>
        </section>
    </screen>
    
    <screen name="TrasPerfPrintBirtSearch">
        <section>
            <actions>
                <set field="actionMenuName" value="AllDisabledMenuBar"/>
                
                <set field="searchBaseScreenName" value="TrasPerfPrintBirt"/>
                <set field="searchBaseScreenLocation" value="component://trasperf/widget/screens/TrasPerfScreens.xml"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="SearchScreen"/>
            </widgets>
        </section>
    </screen>    
   
    
    <screen name="WorkEffortAnalysisPrintBirt">
        <section>
            <actions>
                <set field="parentTypeId" value="CTX_TR"/>
                <set field="parameters.customFindScriptLocation" value="component://workeffortext/webapp/workeffortext/WEB-INF/actions/executePerformFindWorkEffortRootInqyTree.groovy"/>
            </actions>
            <widgets>
                <include-screen name="main-decorator-ui-labels"/>
                <include-screen name="WorkEffortAnalysisPrintBirt" location="component://workeffortext/widget/screens/CommonScreens.xml"/>
            </widgets>
        </section>
    </screen>
    
        <!-- Screen per invio mail -->
    <screen name="CommonCommunicationEventDecorator">
        <section>
            <actions>
                 <property-map resource="BaseUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="BaseErrorLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="CommonUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="PartyUiLabels" map-name="uiLabelMap" global="true"/>
                <property-map resource="PartyExtUiLabels" map-name="uiLabelMap" global="true"/>
            </actions>
            <widgets>
                <container style="communication-container">
                    <decorator-section-include name="body"/>
                </container>
            </widgets>
        </section>
    </screen>
</screens>

