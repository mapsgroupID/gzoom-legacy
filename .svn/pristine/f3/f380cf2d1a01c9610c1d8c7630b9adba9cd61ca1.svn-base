<?xml version="1.0" encoding="UTF-8" ?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<forms xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="http://ofbiz.apache.org/dtds/widget-form.xsd">

    <form name="WorkEffortAndPartyAssignManagementForm" extends="BaseManagementForm" extends-resource="component://base/widget/BaseForms.xml">
    	<actions>
    		<set field="fieldPartyId" from-field="parameters.partyId" default-value="${userLogin.partyId}"/>
    		<entity-one entity-name="Party" value-field="localParty">
    			<field-map field-name="partyId" from-field="fieldPartyId"/>
    		</entity-one>
    		<set field="partyName" from-field="localParty.partyName"/>
    		<entity-one entity-name="Party" value-field="hostParty">
    			<field-map field-name="partyId" from-field="parameters.hostPartyId"/>
    		</entity-one>
    		<set field="hostPartyName" from-field="hostParty.partyName"/>
    	</actions>
    	
    	<read-only>
            <not>
                <or>
                    <if-has-permission permission="WORKEFFORTMGR" action="_CREATE"/>
                    <if-has-permission permission="WORKEFFORTMGR" action="_UPDATE"/>
                    <if-has-permission permission="WORKEFFORTORG" action="_ADMIN"/>
                    <if-has-permission permission="WORKEFFORTROLE" action="_ADMIN"/>
                </or>
            </not>
        </read-only>
        
        <field name="crudService" ><hidden value="crudServiceDefaultOrchestration_WorkEffortPartyAssignment"/></field>
        <field name="workEffortId" use-when="workEffort!=null"><hidden/></field>
        <field name="_AUTOMATIC_PK_"><hidden value="Y"/></field>
        <field name="fromDate" use-when="${bsh: &quot;Y&quot;.equals(context.get(&quot;insertMode&quot;))}">
            <hidden value="${date:nowTimestamp()}"/>
        </field>
        <field name="createDate">
            <hidden value="${date:nowTimestamp()}"/>
        </field>
        <field name="partyId" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;partyId&quot;))}">
            <hidden/>
        </field>
        <field name="partyId" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isEmpty(parameters.get(&quot;partyId&quot;))}">
            <hidden value="${parameters.userLogin.partyId}"/>
        </field>
        <field name="partyName" title="${uiLabelMap.BaseParty}" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isEmpty(parameters.get(&quot;partyId&quot;))}">
            <text read-only="true" default-value="${partyName}"/>
        </field>
        <field name="roleTypeId" use-when="workEffort==null"><hidden value="CAL_OWNER"/></field>

        <field name="hostPartyId" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;hostPartyId&quot;))}">
            <hidden/>
        </field>
        <field name="hostPartyName" title="${uiLabelMap.WorkeffortHostParty}" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;hostPartyId&quot;))}">
            <text read-only="true"/>
        </field>
        <field name="hostRoleTypeId" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;hostPartyId&quot;))}"><hidden value="CAL_ATTENDEE"/></field>
        <field name="hostStatusId" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;hostPartyId&quot;))}"><hidden value="PRTYASGN_ASSIGNED"/></field>
        <field name="workEffortAssocTypeId">
            <hidden/>
        </field>
        <field name="workEffortIdFrom">
            <hidden/>
        </field>

        <field name="statusId" use-when="workEffort==null"><hidden value="PRTYASGN_ASSIGNED"/></field>
        <field name="workEffortName" required-field="true" title="${uiLabelMap.WorkEffortEventName}"><text size="50" maxlength="100" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
        <field name="description" title="${uiLabelMap.CommonDescription}"><text size="70" maxlength="255" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
        <field name="workEffortTypeId" required-field="true">
            <drop-down read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}" current="selected">
                <entity-options entity-name="WorkEffortType" description="${description}">
                    <entity-constraint name="parentTypeId" value="${parameters.parentTypeId}"/>
                    <entity-order-by field-name="description"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="currentStatusId" use-when="workEffort!=null">
            <drop-down read-only="true" drop-list-key-field="statusId" drop-list-display-field="description" size="50" type="drop-list">
                <entity-options entity-name="StatusItem" key-field-name="statusId">
                    <select-field field-name="statusId" display="false"/>
                    <select-field field-name="description" display="true"/>

                    <entity-constraint name="statusTypeId" value="EVENT_STATUS"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="currentStatusId" use-when="workEffort==null"><hidden value="CAL_TENTATIVE"/></field>
        <field name="scopeEnumId" required-field="true">
            <drop-down current="selected" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}">
                <entity-options entity-name="Enumeration" key-field-name="enumId" description="${description}">
                    <entity-constraint name="enumTypeId" value="WORK_EFF_SCOPE"/>
                    <entity-order-by field-name="description"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="estimatedStartDate" required-field="true"><date-time type="timestamp" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
        <field name="estimatedCompletionDate" required-field="true"><date-time type="timestamp" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
    </form>

    <form name="WorkEffortAndPartyAssignCalendarManagementForm" id="WorkEffortAndPartyAssignCalendarManagementForm" extends="WorkEffortAndPartyAssignManagementForm">
        <on-event-update-area area-target="${parameters.extraAreaTarget}${bsh: return org.ofbiz.base.util.UtilValidate.isNotEmpty(parameters.get(&quot;extraTargetParameters&quot;)) ? &quot;?&quot; + parameters.get(&quot;extraTargetParameters&quot;).replace(&quot;*&quot;, &quot;&amp;&quot;) : &quot;&quot;}" event-type="submit" area-id="${parameters.extraAreaId}"/>
    </form>

    <form name="WorkEffortManagementForm" extends="BaseManagementForm" extends-resource="component://base/widget/BaseForms.xml" default-map-name="workEffort">
    	<read-only>
            <not>
                <or>
                    <if-has-permission permission="WORKEFFORTMGR" action="_CREATE"/>
                    <if-has-permission permission="WORKEFFORTMGR" action="_UPDATE"/>
                    <if-has-permission permission="WORKEFFORTORG" action="_ADMIN"/>
                    <if-has-permission permission="WORKEFFORTROLE" action="_ADMIN"/>
                </or>
            </not>
        </read-only>
        
        <field name="entityName"><hidden value="${context.entityName}"/></field>
        <field name="crudService" ><hidden value="crudServiceDefaultOrchestration_WorkEffort"/></field>
        <field name="workEffortId" use-when="workEffort!=null"><hidden/></field>
        <field name="workEffortName" title="${uiLabelMap.WorkEffortEventName}" required-field="true"><text size="50" maxlength="100" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
        <field name="description" title="${uiLabelMap.CommonDescription}" widget-style="fixed-height-textarea"><textarea cols="50" rows="40" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
        <field name="workEffortTypeId" required-field="true">
            <drop-down no-current-selected-key="MEETING" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}" current="selected">
                <entity-options entity-name="WorkEffortType" description="${description}">
                    <entity-constraint name="parentTypeId" value="${parameters.parentTypeId}"/>
                    <entity-order-by field-name="description"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="currentStatusId" use-when="workEffort!=null" required-field="true">
            <drop-down read-only="true" drop-list-key-field="statusId" drop-list-display-field="description" size="50" type="drop-list">
                <entity-options entity-name="StatusItem" key-field-name="statusId" description="${description}">
                    <select-field field-name="statusId" display="false"/>
                    <select-field field-name="description" display="true"/>

                    <entity-constraint name="statusTypeId" value="EVENT_STATUS"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="currentStatusId" use-when="workEffort==null"><hidden value="CAL_TENTATIVE"/></field>
        <field name="scopeEnumId" required-field="true">
            <drop-down current="selected" no-current-selected-key="WES_PRIVATE" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}">
                <entity-options entity-name="Enumeration" key-field-name="enumId" description="${description}">
                    <entity-constraint name="enumTypeId" value="WORK_EFF_SCOPE"/>
                    <entity-order-by field-name="description"/>
                </entity-options>
            </drop-down>
        </field>
        <field name="estimatedStartDate" required-field="true"><date-time type="timestamp" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
        <field name="estimatedCompletionDate" required-field="true"><date-time type="timestamp" read-only="${bsh: &quot;true&quot;.equals(context.get(&quot;readOnly&quot;))}"/></field>
    </form>

    <form name="WorkEffortCalendarManagementForm" id="WorkEffortManagementForm" extends="WorkEffortManagementForm">
        <actions>
        	<set field="fieldPartyId" from-field="parameters.partyId" default-value="${userLogin.partyId}"/>
    		<entity-one entity-name="Party" value-field="localParty">
    			<field-map field-name="partyId" from-field="fieldPartyId"/>
    		</entity-one>
    		<set field="partyName" from-field="localParty.partyName"/>
    		<entity-one entity-name="Party" value-field="hostParty">
    			<field-map field-name="partyId" from-field="parameters.hostPartyId"/>
    		</entity-one>
    		<set field="hostPartyName" from-field="hostParty.partyName"/>
    	</actions>
        <field name="_AUTOMATIC_PK_" use-when="${bsh: return &quot;Y&quot;.equals(parameters.get(&quot;insertMode&quot;))}"><hidden value="Y"/></field>
        <field name="partyId" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isEmpty(parameters.get(&quot;partyId&quot;))}">
            <hidden value="${parameters.userLogin.partyId}"/>
        </field>
        <field name="partyId" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;partyId&quot;))}">
            <hidden/>
        </field>
        <field name="roleTypeId" use-when="workEffort==null"><hidden value="CAL_OWNER"/></field>
        <field name="statusId" use-when="workEffort==null"><hidden value="PRTYASGN_ASSIGNED"/></field>
        <field name="partyName" title="${uiLabelMap.BaseParty}" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;partyId&quot;))}">
            <text read-only="true" default-value="${partyName}"/>
        </field>
        <field name="hostPartyName" map-name="calendarEventDetails" title="${uiLabelMap.WorkeffortHostParty}" use-when="${bsh: import org.ofbiz.base.util.UtilValidate; return UtilValidate.isNotEmpty(parameters.get(&quot;hostPartyId&quot;))}">
            <text read-only="true"/>
        </field>
        <on-event-update-area area-target="${parameters.extraAreaTarget}?${bsh: return org.ofbiz.base.util.UtilValidate.isNotEmpty(parameters.get(&quot;extraTargetParameters&quot;)) ? &quot;?&quot; + parameters.get(&quot;extraTargetParameters&quot;).replace(&quot;*&quot;, &quot;&amp;&quot;) : &quot;&quot;}" event-type="submit" area-id="${parameters.extraAreaId}"/>
        <sort-order>
            <sort-field name="partyName"/>
            <sort-field name="hostPartyName"/>
            <sort-field name="workEffortName"/>
            <sort-field name="description"/>
            <sort-field name="workEffortTypeId"/>
            <sort-field name="currentStatusId"/>
            <sort-field name="scopeEnumId"/>
            <sort-field name="estimatedStartDate"/>
            <sort-field name="estimatedCompletionDate"/>
        </sort-order>
    </form>
</forms>