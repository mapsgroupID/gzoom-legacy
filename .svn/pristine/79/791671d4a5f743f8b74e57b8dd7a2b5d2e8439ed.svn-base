<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<!-- Note:  A "Server" is not itself a "Container", so you may not
     define subcomponents such as "Valves" at this level.
     Documentation at /docs/config/server.html
 -->
<Server port="8005" shutdown="SHUTDOWN">
  <Listener className="org.apache.catalina.startup.VersionLoggerListener" />
  <!-- Security listener. Documentation at /docs/config/listeners.html
  <Listener className="org.apache.catalina.security.SecurityListener" />
  -->
  <!--APR library loader. Documentation at /docs/apr.html -->
  <Listener className="org.apache.catalina.core.AprLifecycleListener" SSLEngine="on" />
  <!-- Prevent memory leaks due to use of particular java/javax APIs-->
  <Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener" />
  <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
  <Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener" />

  <!-- Global JNDI resources
       Documentation at /docs/jndi-resources-howto.html
  -->
  <GlobalNamingResources>
    <!-- Editable user database that can also be used by
         UserDatabaseRealm to authenticate users
    -->
    <Resource name="UserDatabase" auth="Container"
              type="org.apache.catalina.UserDatabase"
              description="User database that can be updated and saved"
              factory="org.apache.catalina.users.MemoryUserDatabaseFactory"
              pathname="conf/tomcat-users.xml" />
  </GlobalNamingResources>

  <!-- A "Service" is a collection of one or more "Connectors" that share
       a single "Container" Note:  A "Service" is not itself a "Container",
       so you may not define subcomponents such as "Valves" at this level.
       Documentation at /docs/config/service.html
   -->
  <Service name="Catalina">

    <!--The connectors can use a shared executor, you can define one or more named thread pools-->
    <!--
    <Executor name="tomcatThreadPool" namePrefix="catalina-exec-"
        maxThreads="150" minSpareThreads="4"/>
    -->


    <!-- A "Connector" represents an endpoint by which requests are received
         and responses are returned. Documentation at :
         Java HTTP Connector: /docs/config/http.html
         Java AJP  Connector: /docs/config/ajp.html
         APR (HTTP/AJP) Connector: /docs/apr.html
         Define a non-SSL/TLS HTTP/1.1 Connector on port 8080
    -->
    <Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               address="0.0.0.0" redirectPort="10523" 
               relaxedPathChars="[]|" relaxedQueryChars="[]|{}^\&quot;&gt;&lt;"/>
    <!-- A "Connector" using the shared thread pool-->
    <!--
    <Connector executor="tomcatThreadPool"
               port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
    -->
    <!-- Define an SSL/TLS HTTP/1.1 Connector on port 8443
         This connector uses the NIO implementation. The default
         SSLImplementation will depend on the presence of the APR/native
         library and the useOpenSSL attribute of the
         AprLifecycleListener.
         Either JSSE or OpenSSL style configuration may be used regardless of
         the SSLImplementation selected. JSSE style configuration is used below.
    -->
    <!--
    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true">
        <SSLHostConfig>
            <Certificate certificateKeystoreFile="conf/localhost-rsa.jks"
                         type="RSA" />
        </SSLHostConfig>
    </Connector>
    -->
    <!-- Define an SSL/TLS HTTP/1.1 Connector on port 8443 with HTTP/2
         This connector uses the APR/native implementation which always uses
         OpenSSL for TLS.
         Either JSSE or OpenSSL style configuration may be used. OpenSSL style
         configuration is used below.
    -->
    <!--
    <Connector port="8443" protocol="org.apache.coyote.http11.Http11AprProtocol"
               maxThreads="150" SSLEnabled="true" >
        <UpgradeProtocol className="org.apache.coyote.http2.Http2Protocol" />
        <SSLHostConfig>
            <Certificate certificateKeyFile="conf/localhost-rsa-key.pem"
                         certificateFile="conf/localhost-rsa-cert.pem"
                         certificateChainFile="conf/localhost-rsa-chain.pem"
                         type="RSA" />
        </SSLHostConfig>
    </Connector>
    -->

    <!-- Define an AJP 1.3 Connector on port 8009 -->
   
    <Connector protocol="AJP/1.3"
               address="0.0.0.0"
               port="8009"
               redirectPort="10523" secretRequired="false"
               tomcatAuthentication="true"/>
   

    <!-- An Engine represents the entry point (within Catalina) that processes
         every request.  The Engine implementation for Tomcat stand alone
         analyzes the HTTP headers included with the request, and passes them
         on to the appropriate Host (virtual host).
         Documentation at /docs/config/engine.html -->

    <!-- You should set jvmRoute to support load-balancing via AJP ie :
    <Engine name="Catalina" defaultHost="localhost" jvmRoute="jvm1">
    -->
    <Engine name="Catalina" defaultHost="localhost">

      <!--For clustering, please take a look at documentation at:
          /docs/cluster-howto.html  (simple how to)
          /docs/config/cluster.html (reference documentation) -->
      <!--
      <Cluster className="org.apache.catalina.ha.tcp.SimpleTcpCluster"/>
      -->

      <!-- Use the LockOutRealm to prevent attempts to guess user passwords
           via a brute-force attack -->
      <Realm className="org.apache.catalina.realm.LockOutRealm">
        <!-- This Realm uses the UserDatabase configured in the global JNDI
             resources under the key "UserDatabase".  Any edits
             that are performed against this UserDatabase are immediately
             available for use by the Realm.  -->
        <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
               resourceName="UserDatabase"/>
      </Realm>

      <Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">

        <!-- SingleSignOn valve, share authentication between web applications
             Documentation at: /docs/config/valve.html -->
        <!--
        <Valve className="org.apache.catalina.authenticator.SingleSignOn" />
        -->

        <!-- Access log processes all example.
             Documentation at: /docs/config/valve.html
             Note: The pattern used is equivalent to using pattern="common" -->
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="localhost_access_log" suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
		
		<!-- BEGIN OFBiz settings -->
	<Context path="/bi" docBase="../../../framework/bi/webapp/bi"
         privileged="false">
	</Context>
	<Context path="/webtools" docBase="../../../framework/webtools/webapp/webtools"
			 privileged="false">
	</Context>
	<Context path="/images" docBase="../../../framework/images/webapp/images"
			 privileged="false">
	</Context>
	<Context path="/tempfiles" docBase="../../../framework/images/../../runtime/tempfiles"
			 privileged="false">
	</Context>
	<Context path="/bizznesstime" docBase="../../../themes/bizznesstime/webapp/bizznesstime"
			 privileged="false">
	</Context>
	<Context path="/bluelight" docBase="../../../themes/bluelight/webapp/bluelight"
			 privileged="false">
	</Context>
	<Context path="/droppingcrumbs" docBase="../../../themes/droppingcrumbs/webapp/droppingcrumbs"
			 privileged="false">
	</Context>
	<Context path="/flatgrey" docBase="../../../themes/flatgrey/webapp/flatgrey"
			 privileged="false">
	</Context>
	<Context path="/multiflex" docBase="../../../themes/multiflex/webapp/multiflex"
			 privileged="false">
	</Context>
	<Context path="/theme_gplus" docBase="../../../themes/theme_gplus/webapp/theme_gplus"
			 privileged="false">
	</Context>
	<Context path="/old_theme_gplus" docBase="../../../themes/theme_gplus/webapp/old_theme_gplus"
			 privileged="false">
	</Context>
	<Context path="/tomahawk" docBase="../../../themes/tomahawk/webapp/tomahawk"
			 privileged="false">
	</Context>
	<Context path="/partymgr" docBase="../../../applications/party/webapp/partymgr"
			 privileged="false">
	</Context>
	<Context path="/content" docBase="../../../applications/content/webapp/content"
			 privileged="false">
	</Context>
	<Context path="/workeffort" docBase="../../../applications/workeffort/webapp/workeffort"
			 privileged="false">
	</Context>
	<Context path="/ical" docBase="../../../applications/workeffort/webapp/ical"
			 privileged="false">
	</Context>
	<Context path="/catalog" docBase="../../../applications/product/webapp/catalog"
			 privileged="false">
	</Context>
	<Context path="/facility" docBase="../../../applications/product/webapp/facility"
			 privileged="false">
	</Context>
	<Context path="/manufacturing" docBase="../../../applications/manufacturing/webapp/manufacturing"
			 privileged="false">
	</Context>
	<Context path="/accounting" docBase="../../../applications/accounting/webapp/accounting"
			 privileged="false">
	</Context>
	<Context path="/ar" docBase="../../../applications/accounting/webapp/ar"
			 privileged="false">
	</Context>
	<Context path="/ap" docBase="../../../applications/accounting/webapp/ap"
			 privileged="false">
	</Context>
	<Context path="/humanres" docBase="../../../applications/humanres/webapp/humanres"
			 privileged="false">
	</Context>
	<Context path="/ordermgr" docBase="../../../applications/order/webapp/ordermgr"
			 privileged="false">
	</Context>
	<Context path="/marketing" docBase="../../../applications/marketing/webapp/marketing"
			 privileged="false">
	</Context>
	<Context path="/sfa" docBase="../../../applications/marketing/webapp/sfa"
			 privileged="false">
	</Context>
	<Context path="/ofbizsetup" docBase="../../../applications/commonext/webapp/ofbizsetup"
			 privileged="false">
	</Context>
	<Context path="/birt" docBase="../../../specialpurpose/birt/webapp/birt"
			 privileged="false">
	</Context>
	<Context path="/" docBase="../../../hot-deploy/base/webapp/baseroot"
			 privileged="false">
	</Context>
	<Context path="/gzoom" docBase="../../../hot-deploy/base/webapp/base"
			 privileged="false">
	</Context>
	<Context path="/resources" docBase="../../../hot-deploy/base/webapp/resources"
			 privileged="false">
	</Context>
	<Context path="/commondataext" docBase="../../../hot-deploy/commondataext/webapp/commondataext"
			 privileged="false">
	</Context>
	<Context path="/humanresext" docBase="../../../hot-deploy/humanresext/webapp/humanresext"
			 privileged="false">
	</Context>
	<Context path="/partyext" docBase="../../../hot-deploy/partyext/webapp/partyext"
			 privileged="false">
	</Context>
	<Context path="/accountingext" docBase="../../../hot-deploy/accountingext/webapp/accountingext"
			 privileged="false">
	</Context>
	<Context path="/workeffortext" docBase="../../../hot-deploy/workeffortext/webapp/workeffortext"
			 privileged="false">
	</Context>
	<Context path="/workeffortpub" docBase="../../../hot-deploy/workeffortext/webapp/workeffortpub"
			 privileged="false">
	</Context>
	<Context path="/emplperf" docBase="../../../hot-deploy/emplperf/webapp/emplperf"
			 privileged="false">
	</Context>
	<Context path="/orgperf" docBase="../../../hot-deploy/orgperf/webapp/orgperf"
			 privileged="false">
	</Context>
	<Context path="/stratperf" docBase="../../../hot-deploy/stratperf/webapp/stratperf"
			 privileged="false">
	</Context>
	<Context path="/managacc" docBase="../../../hot-deploy/managacc/webapp/managacc"
			 privileged="false">
	</Context>
	<Context path="/projectmgrext" docBase="../../../hot-deploy/projectmgrext/webapp/projectmgrext"
			 privileged="false">
	</Context>
	<Context path="/corperf" docBase="../../../hot-deploy/corperf/webapp/corperf"
			 privileged="false">
	</Context>
	<Context path="/procperf" docBase="../../../hot-deploy/procperf/webapp/procperf"
			 privileged="false">
	</Context>
	<Context path="/cdgperf" docBase="../../../hot-deploy/cdgperf/webapp/cdgperf"
			 privileged="false">
	</Context>
	<Context path="/trasperf" docBase="../../../hot-deploy/trasperf/webapp/trasperf"
			 privileged="false">
	</Context>
	<Context path="/rendperf" docBase="../../../hot-deploy/rendperf/webapp/rendperf"
			 privileged="false">
	</Context>
	<Context path="/gdprperf" docBase="../../../hot-deploy/gdprperf/webapp/gdprperf"
			 privileged="false">
	</Context>
	<Context path="/partperf" docBase="../../../hot-deploy/partperf/webapp/partperf"
			 privileged="false">
	</Context>
	<Context path="/dirigperf" docBase="../../../hot-deploy/dirigperf/webapp/dirigperf"
			 privileged="false">
	</Context>
	<Context path="/productext" docBase="../../../hot-deploy/productext/webapp/productext"
			 privileged="false">
	</Context>
	<Context path="/jbpm" docBase="../../../hot-deploy/jbpm/webapp/jbpm"
			 privileged="false">
	</Context>
	<Context path="/gzope" docBase="../../../hot-deploy/gzope/webapp/gzope"
			 privileged="false">
	</Context>
	<Context path="/help" docBase="../../../hot-deploy/help/webapp/help"
			 privileged="false">
	</Context>
	<Context path="/testapp" docBase="../../../hot-deploy/testapp/webapp/testapp"
			 privileged="false">
	</Context>
	<Context path="/gzbox" docBase="../../../hot-deploy/gzbox/webapp/gzbox"
			 privileged="false">
	</Context>
	<Context path="/ws" docBase="../../../hot-deploy/ws/webapp/ws"
			 privileged="false">
	</Context>
	<Context path="/custom" docBase="../../../hot-deploy/custom/webapp/custom"
			 privileged="false">
	</Context>
    <!-- END OFBiz settings -->

      </Host>
    </Engine>
  </Service>
</Server>
